# Primicerius Project

## Version History

* 1.0
    * Initial version.

## Introduction

The Primicerius Project (PRIM) is a video game / simulation application that aims to mimic the adventures of TODO

## Controls and Commands

### Command

For simplification, a "command" will be defined as a set of words entered in the command box, which is the bottom-most box in the game screen (preceded by the character `>`).

### Action Modes

Action modes are actions that are performed when the player presses the arrow keys. The action performed is performed in the appropriate direction. To change action modes, the following commands can be entered:

* `debug`: Enters the debug action mode, allowing for further commands.
* `move`: Enters the move action mode. Arrow keys will displace the player character in the game world.
* `look`: Enters the look action mode. Arrow keys will display what is in the given direction directly adjacent to the player character. Not to be confused with `look around`.
* `use`: Enters the use action mode. Arrow keys will allow the player character to "use" the object in the given direction, if it can be used. Shorthand for these are in the `use up`, `use down`, `use left`, and `use right` commands.

### Debug Commands

Note that in the debug action mode, no other commands will be accepted.

* `debug`: Enter debug action mode.
* `debug end`: Leave debug action mode and enters the move action mode.
* `player.increase $attribute $val`: Increases the given attribute `[hunger, thirst, energy]` by the amount in `$val`.
* `player.decrease $attribute $val`: Decreases the given attribute `[hunger, thirst, energy]` by the amount in `$val`.

### Single-Key Commands

These commands can be entered by pressing a single key on the keyboard. They will not be entered in the command box for confirmation.

* `1`,`0`: Moves the inventory view up/down based on the player character's inventory.
* `2` through to `9`: Uses the inventory item at the given index in the inventory view.

### Other Commands

* `cast spell`: Casts the spell denoted by `spell`.
* `discard n`: Discards the inventory item at the index `n`, either above, to the right, below, or to the left of the player character. If no space is available, the item is destroyed.
* `equip n`: Equips to the player character the inventory item at the index `n`.
* `look around`: Allows a cursor to be controlled by the arrow keys in the viewport, giving a short description of what is under the cursor until the `ESC` key is pressed.
* `spellbook`: Opens a small window showing all spells in the player's inventory.
* `unequip n`: Unequips to the player character the inventory item at the index `n`.

## Various Notes

### User Interface
The user interface of this project is implemented using [Google lanterna](https://github.com/mabe02/lanterna).

### UI Samples
These really only look good in monospace.

NOTE: Most of the following are deprecated, but kept because they had golden ideas.

#### Base Sketch
```
inbox 40x18 outbox 42x22                  i 25x10 o 27x12            i 25x10 o 27x12
╔════════════════════════════════════════╗╔═════════════════════════╗╔═════════════════════════╗
║                                        ║║BLD XXX               ACT║║1 -----------------------║
║                                        ║║PSN XXX    ███ 100%      ║║2     apple             2║
║                             w          ║║FTG XXX    ███           ║║3     bullets, .44.    99║
║                                        ║║            ░            ║║4 [R] gun, revolvr .44  1║
║                                        ║║    100% █░███░█ 100%    ║║5     gun, revolvr .44  2║
║                    X                   ║║         █ ███ █         ║║6     leather cwby hat  1║
║                                    ░░░░║║           ░ ░      H---%║║7 ~~~ leather cwby hat  2║
║                                   ░░░░░║║       95% █ █ 100% T 76%║║8 [T] leather jacket    1║
║                     w             ░░░░░║║[======]H  █ █      B100%║║9 [B] leather pants     1║
║                    @             ░░░░░░║║[===   ]T       E[ =====]║║0 ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼║
║                                  ░░░░░░║╚═════════════════════════╝╚═════════════════════════╝
║                                 ░░░░░░ ║╔════════════════════════════════════════════════════╗
║                                ░░░░░░  ║║$ [use] 2                                           ║
║                                ░░░░░   ║║Use apple                                           ║
║                                 ░░░    ║║You eat the apple. It is tasty.                     ║
║                                        ║║$ 4                                                 ║
║                                        ║║gun, revolver .44                                   ║
║                              wolf, sand║║common weapon, gun, ballistic, 60 DMG, 1-handed     ║
╠════════════════════════════════════════╣║A ballistic 6-shooter revolver reminiscent of the   ║
║           Sephir, Teneig Desert (12, 5)║║wild wild west. Bang bang.                          ║
╚════════════════════════════════════════╝╚════════════════════════════════════════════════════╝
i 40x1 o see above                        i 52x8 o 54x10
```

#### Panels

The left-most panel is called the **viewport**, and describes the world the player is currently in. There are a few elements seen here. The `@` character here represents the player, while the `w` character represents wolves. We also see the cursor, `X`, which is highlighting a wolf on sand (as described to the bottom-right of the viewport). Some `░` characters represent the water of an oasis, which would be coloured in the final version. At the bottom of the viewport is indications of where the player is right now, at least the immediately-concerning elements such as the planet and the location on the planet; systems are excluded in this view because of the relative irrelevance here (that information can always be viewed in a map screen).

On the right we have three panels. The top-left one is called the **status panel**, and describes the player's status. It has a few elements, such as limb status (represented visually by the character model), a set of status codes (all displayed here to show, common ones on the left (`BLD` for bleeding, `PSN` for poisoned, `FTG` for fatigued), variable ones on the right), the current action the player is set to do (`ACT` on the top-right), `H`ealth and `T`hirst bars on the bottom-left, and `E`nergy levels on the bottom-right. Above Energy levels are shown 3 percentages, indicating the durability, in percent, of worn head, torso, and leg armor.

The panel to the right of the status panel is the **inventory panel** and denotes a slice of 8 items in the player's inventory. Number keys allow for quick inspection of each of these elements. Except for the first and last items, which represent whether the player has more items in their inventory, all inventory item lines have the following format: `n COD item             qt`. `n` is the number key to inspect the given item. `CND` is a 3-character code to describe the condition of the item. For example, the `4` item, `gun, revolvr .44`, is current held in the `[R]`ight hand. `item` is a 16-character-limited version of the item's name, and `qt` is the quantity of the item in the player's inventory.

Here we can see that the player is holding a gun in its right hand, and is wearing a leather jacket on its `T`op and some leather pants as `B`ottoms. We can also see that the player has two `BRK` (broken) leather cowboy hats in their inventory. The row of `-` at the top indicates that there are no items above apple, and the row of `▼` at the bottom means the player has more items in their inventory below the leather pants. Items in the inventory are shown here in alphabetical order.

The bottom-right most panel is called the **interaction panel** and shows text to the user based on recent interactions. Inputs from the user are preceded by `$`. There are two kinds of inputs by the user. The first one is to simply look, and is implicit. The line `$ 4` indicates that the user pressed the 4 key, meaning they wanted to look at the item occupying the 4 space in their inventory.

`$ [use] 2` is a little more descriptive, however. `[use]` means that the player recently pressed `u` to `u`se an item. Since the window does not show more than 8 lines, the entire interaction is currently lost, but would have looked like the following:

```
╔════════════════════════════════════════════════════╗
║                                                    ║
║                                                    ║
║                                                    ║
║$ u                                                 ║
║Use ?                                               ║
║$ [use] 2                                           ║
║Use apple                                           ║
║You eat the apple. It is tasty.                     ║
╚════════════════════════════════════════════════════╝
```

#### Layering

Finally, the UI should be layered in order to display extraneous data to the user. For example, the following is the exact same screen as above, but after the user has pressed `s` in order to save the game:

```
╔════════════════════════════════════════╗╔═════════════════════════╗╔═════════════════════════╗
║                                        ║║BLD XXX               ACT║║1 -----------------------║
║                                        ║║PSN XXX    ███ 100%      ║║2     apple             2║
║                             w          ║║FTG XXX    ███           ║║3     bullets, .44.    99║
║                                        ║║            ░            ║║4 [R] gun, revolvr .44  1║
║                                        ║║    100% █░███░█ 100%    ║║5     gun, revolvr .44  2║
║                    X                   ║║         █ ███ █         ║║6     leather cwby hat  1║
║                                  ╔════════════════════════╗  H---%║║7 ~~~ leather cwby hat  2║
║                                  ║                        ║% T 76%║║8 [T] leather jacket    1║
║                     w            ║  Do you wish to save?  ║  B100%║║9 [B] leather pants     1║
║                    @             ║                        ║ =====]║║0 ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼║
║                                  ║  [YES]            NO   ║═══════╝╚═════════════════════════╝
║                                 ░║                        ║══════════════════════════════════╗
║                                ░░╚════════════════════════╝item 2: apple                     ║
║                                ░░░░░   ║║apple - common - A fruit native to Earth. Restores  ║
║                                 ░░░    ║║25 hunger and 20 thirst. Tasty.                     ║
║                                        ║║$ u                                                 ║
║                                        ║║Use ?                                               ║
║                              wolf, sand║║$ u 2                                               ║
╠════════════════════════════════════════╣║Use apple                                           ║
║           Sephir, Teneig Desert (12, 5)║║You eat the apple. It is tasty.                     ║
╚════════════════════════════════════════╝╚════════════════════════════════════════════════════╝
```

#### Ship Screen

```
╔════════════════════════════════════════╗╔═════════════════════════╗╔═════════════════════════╗
║                                        ║║       ████1  ████3      ║║1 Basic Lasers           ║
║                                     ██ ║║      ░░░░░░░░░░░        ║║2 Basic Lasers           ║
║                                     ██ ║║ ███░███████████████░███ ║║3 Photon Torpedoes     ++║
║                                        ║║ ███░███████████████░███ ║║4 Shield Enhancers       ║
║                                        ║║      ░░░░░░░░░░░        ║║5 XXXXXXXXXXXXXXXXXXXX   ║
║                                ◍       ║║       ████2  ████4      ║║6 XXXXXXXXXXXXXXXXXXXX   ║
║                                        ║╠═════════════════════════╣╠═════════════════════════╣
║                                        ║║H/E/S: 100% / 100% / 100%║║SHIELDS    10 ( 3000 PWR)║
║              ▻                         ║║POWER:        300 /  6000║║WEAPONS     2 (  300 PWR)║
║                    @                   ║║FUEL :       9780 / 10000║║THRUST      5 ( 2400 PWR)║
║                   █████                ║╚═════════════════════════╝╚═════════════════════════╝
║                  ███████               ║╔════════════════════════════════════════════════════╗
║                  ███████               ║║$ +                                                 ║
║                  ███████               ║║Moving up to system view.                           ║
║                   █████                ║║$ 4                                                 ║
║                                        ║║Insufficient power (requires 500).                  ║
║     ▫                                  ║║$ t                                                 ║
║                                     SYS║║Alter Thrust to ?                                   ║
╠════════════════════════════════════════╣║$ [thrust] 5                                        ║
║             Goetz System, Planet Sephir║║Setting thrust to 5/10, speed is 2000pt.            ║
╚════════════════════════════════════════╝╚════════════════════════════════════════════════════╝
++ = more than 99
pt = per tick, unit of speed
```

The ship screen re-uses the same panels as the previous view, but uses them differently.

### Game Elements

#### Ship Specifications

Ship specifications always have a spine showing, in order of left-to-right, the status of internal systems, the hull integrity, and the engines.

**Empty (for copy-paste purposes)**

```
╔═════════════════════════╗
║                         ║
║                         ║
║                         ║
║                         ║
║                         ║
║                         ║
╠═════════════════════════╣
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
╚═════════════════════════╝
```

**Light Transport**
A small ship with a single pilot and up to a single passenger. Limited cargo space.
```
╔═════════════════════════╗
║                         ║
║                         ║
║        █░███░██         ║
║                         ║
║                         ║
║                         ║
╠═════════════════════════╣
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
╚═════════════════════════╝
```

**Heavy Transport**
A large ship with a single pilot and up to a small number of passengers. Cargo space is larger.
```
╔═════════════════════════╗
║                         ║
║                         ║
║      ██░█████░███       ║
║      ██░█████░███       ║
║                         ║
║                         ║
╠═════════════════════════╣
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
╚═════════════════════════╝
```

**Freighter**
A large ship designed to haul a large number of cargo. Two hardpoints.
```
╔═════════════════════════╗
║           ███1          ║
║          ░░░            ║
║   ██░████████░███       ║
║   ██░████████░███       ║
║          ░░░            ║
║           ███2          ║
╠═════════════════════════╣
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
╚═════════════════════════╝
```

**Heavy Freighter**
A large ship designed to haul a large number of cargo. Two hardpoints.
```
╔═════════════════════════╗
║              ███1       ║
║         ░░░░░░░         ║
║    ██░██████████░███    ║
║    ██░██████████░███    ║
║         ░░░░░░░         ║
║              ███2       ║
╠═════════════════════════╣
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
╚═════════════════════════╝
```

**Train**
A large ship designed to haul a very large number of cargo. Four hardpoints.
```
╔═════════════════════════╗
║      ░███1  ░███3       ║
║     ░░░░░░░░░░░░        ║
║ ██░███████████████░████ ║
║ ██░███████████████░████ ║
║     ░░░░░░░░░░░░        ║
║      ░███2  ░███4       ║
╠═════════════════════════╣
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
╚═════════════════════════╝
```

**Light Fighter**
A small fighter designed to be lightweight, cheap, and fast. Has two hardpoints.
```
╔═════════════════════════╗
║              ███1       ║
║          ░░░░░░         ║
║     ██░████████░██      ║
║          ░░░░░░         ║
║              ███2       ║
║                         ║
╠═════════════════════════╣
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
╚═════════════════════════╝
```

**Heavy Fighter**
Standard fighter ship. Has four hardpoints.
```
╔═════════════════════════╗
║       ████1  ████3      ║
║      ░░░░░░░░░░░        ║
║ ████░░█████████████░███ ║
║ ████░░█████████████░███ ║
║      ░░░░░░░░░░░        ║
║       ████2  ████4      ║
╠═════════════════════════╣
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
╚═════════════════════════╝
```

**Very Heavy Fighter**
A very large fighter that moves slowly but has 6 hardpoints.
```
╔═════════════════════════╗
║  ░████1░░████3░░████5░  ║
║ ░░░░░░░░░░░░░░░░░░░░░░░ ║
║ █░███████████████████░█ ║
║ █░███████████████████░█ ║
║ ░░░░░░░░░░░░░░░░░░░░░░░ ║
║  ░████2░░████4░░████6░  ║
╠═════════════════════════╣
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
║XXXXXXXXXXXXXXXXXXXXXXXXX║
╚═════════════════════════╝
```