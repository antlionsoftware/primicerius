
enable_debug_commands = True
profile_memory_usage = False
player_vis_on_start = True
fps_counter = False

# Change the values below and see what happens, I have no fucking clue.

app_window_height = 35
app_window_width = 161

system_view_height = app_window_height - 6
system_view_width = app_window_width - 6
system_view_y = 3
system_view_x = 3

text_width = 79
text_x = 82

text_scroll_height = 20
text_scroll_width = text_width
text_scroll_y = 12
text_scroll_x = text_x

text_enter_height = 1
text_enter_width = text_width
text_enter_y = 34
text_enter_x = text_x

inv_height = 10
inv_width = 52
inv_y = 0
inv_x = 109

view_height = 30
view_width = 80
view_y = 0
view_x = 0

loc_info_height = 3
loc_info_width = 80
loc_info_y = 32
loc_info_x = 0


########## EVERYTHING ABOVE THIS LINE IS DEPRECATED ##########

# World generation settings
stage_size = 100

# 0: KEYSTROKE, 1: DEBUG, 2: INFO, 3: WARN, 4: ERROR, 5: FATAL
log_level = 3
