﻿# -*- coding: utf-8 -*-
import curses
import threading
import time
import traceback
from curses import wrapper
from statistics import mean

import settings
from poc.engine import (
    window_manager_inst,
    menu_library_inst,
    keyboard_input_manager_inst,
    menu_manager_inst,
    app_ctrl_inst,
    MenuManager,
    game_manager_inst,
    event_manager_inst)
from poc.engine.event_listener import Event
from poc.engine.event_manager import PLAYER_CLICK_EVENT
from poc.utils import get_center_coordinates
from poc.utils.log_manager import logger

pm_log_thread = None
loop_times = []


def main(screen_handle):
    from poc.ui.singletons import app_window, view_port

    curses.start_color()
    screen_handle.keypad(1)
    screen_handle.nodelay(1)
    curses.curs_set(False)
    curses.mousemask(1)
    logger.debug("Screen handler setup.")
    from poc.ui.color import color_manager_inst

    color_manager_inst.init_colors()
    window_manager_inst.register(app_window)
    app_window.lower_right("For best results, make sure your terminal has a monospace font.")
    # TODO Figure out why these are needed twice
    window_manager_inst.refresh()
    screen_handle.refresh()
    window_manager_inst.refresh()
    screen_handle.refresh()
    logger.debug("App window registered.")
    main_menu_center_y, main_menu_center_x = get_center_coordinates(
        menu_library_inst.main_menu.get_expected_draw_height(), menu_library_inst.main_menu.get_expected_draw_width()
    )
    menu_manager_inst.open_menu(menu_library_inst.main_menu, main_menu_center_y, main_menu_center_x)
    logger.debug("Main menu opened.")

    kill_keys = [ord("`")]
    while app_ctrl_inst.running:
        start_time = time.time()
        has_input = False
        try:
            key = screen_handle.getch()
            curses.flushinp()

            if key in kill_keys and game_manager_inst.get_kill_enabled():
                has_input = True
                MenuManager.show_quit_screen()

            elif key == curses.KEY_MOUSE:
                has_input = True
                _, mx, my, _, _ = curses.getmouse()
                pos = (my - 1 + view_port.delta_y, mx - 1 + view_port.delta_x)
                if game_manager_inst.active:
                    event_manager_inst.notify(Event(PLAYER_CLICK_EVENT, pos))

            elif key is not None and key != -1:
                has_input = True
                logger.keystroke(key)
                keyboard_input_manager_inst.on_press(key)

            from poc.engine.actions.player_status_actions import RefreshPlayerStatusAction

            RefreshPlayerStatusAction().act()

            window_manager_inst.refresh()
            screen_handle.refresh()
        finally:
            loop_time = time.time() - start_time
            if has_input:
                loop_times.append(loop_time)
            if loop_time > 0.2:
                logger.time("Longer game loop.", loop_time, 0.2)
                curses.flushinp()


def profile_memory_usage():
    import os
    import psutil

    process = psutil.Process(os.getpid())
    logger.debug("Memory usage: %s" % process.memory_info().rss)
    global pm_log_thread
    pm_log_thread = threading.Timer(2.0, profile_memory_usage)
    pm_log_thread.start()


if __name__ == "__main__":
    logger.info("Starting application Primicerius.")
    if settings.profile_memory_usage:
        profile_memory_usage()
    try:
        wrapper(main)
    except KeyboardInterrupt as ki:
        logger.debug("Keyboard Interrupt.", traceback.format_exc())
    except Exception:
        logger.fatal("Unknown error.", traceback.format_exc())
    finally:
        from poc.ui.singletons import flash_controller

        flash_controller.cancel()
        if settings.profile_memory_usage:
            pm_log_thread.cancel()
        game_manager_inst.disable_realtime()
        logger.info("Closing application.")
        logger.info("Stat: Average game loop length this run when there was input was %f." % mean(loop_times))
        logger.close()
