import math

from settings import app_window_height, app_window_width


def get_center_coordinates(expected_height: int, expected_width: int) -> (int, int):
    y = math.floor((app_window_height / 2) - (expected_height / 2))
    x = math.floor((app_window_width / 2) - (expected_width / 2))
    return y, x


def space_text(left_text: str, right_text: str, width: int):
    assert len(left_text) + len(right_text) + 1 < width
    spaces = width - (len(left_text) + len(right_text))
    return left_text + (" " * spaces) + right_text


def get_value(obj):
    return obj() if callable(obj) else obj

