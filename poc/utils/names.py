adjective = [
    "avaricious",
    "ethereal",
    "fallen",
    "hungry",
    "majestic",
    "secular"
]

# Agnostic of biome names.
location_suffix = [
    "approach",
    "land",
    "pass",
    "passage",
    "trail"
]
