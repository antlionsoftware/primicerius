class UnimplementedException(Exception):
    def __init__(self):
        super().__init__('Unimplemented method called.')
