from poc.entity.player import Player, PlayerInfo
from poc.entity.nation import Nation

player_info = PlayerInfo("Damian Zachary Jenata", "Male", 25)
player_nation = Nation("Aegis", 100, [], 1000000)
player_inst = Player(player_info, player_nation)
