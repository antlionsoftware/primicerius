from poc.entity.entity import Tag, EntityInstance


class BehaviourEntityDict:

    @staticmethod
    def register_behaviour_to_entity(entity_instance):
        assert entity_instance.has_tag(Tag.BEHAVIOUR)
        assert isinstance(entity_instance, EntityInstance), "Entity %s is not of the proper type." % entity_instance.get_name()
        bpid = entity_instance.get_blueprint_identifier()
        # TODO Make this better
        # if entity_instance.get_blueprint_identifier() in self._behaviour_entity_dict.keys():
        #     logger.debug("Registering behaviour to %s." % entity_instance)
        #     entity_instance.register_behaviour(self._behaviour_entity_dict[entity_instance.get_blueprint_identifier()])
        from poc.entity.entity_lib import wolf_entity
        from poc.entity.behaviour_lib import WandererBehaviour
        from poc.entity.behaviour_lib import HostileBehaviour
        from poc.entity.behaviour import PriorityCompositeBehaviour
        from poc.entity.entity_lib import goblin_entity
        from poc.entity.entity_lib import human_adult_male
        from poc.entity.entity_lib import human_adult_female
        from poc.utils.log_manager import logger
        if bpid == wolf_entity.get_identifier():
            logger.debug("Registering wolf_entity behaviour.")
            entity_instance.register_behaviour(PriorityCompositeBehaviour([HostileBehaviour(entity_instance), WandererBehaviour(1, 5)]))
        elif bpid == goblin_entity.get_identifier():
            logger.debug("Registering goblin_entity behaviour.")
            entity_instance.register_behaviour(PriorityCompositeBehaviour([HostileBehaviour(entity_instance), WandererBehaviour(1, 2)]))
        elif bpid == human_adult_male.get_identifier() or bpid == human_adult_female.get_identifier():
            logger.debug("Registering human adult behaviour.")
            entity_instance.register_behaviour(WandererBehaviour(20, 100))
