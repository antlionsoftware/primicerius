from poc.entity.entity import Entity
from poc.entity.entity_lib import wood_wall_entity


def place_box(
    stage,
    y: int,
    x: int,
    height: int,
    width: int,
    enter_y: int = -1,
    enter_x: int = -1,
    wall_entity: Entity = wood_wall_entity,
    door_entity: Entity = None,
    extras: [(Entity, int, int)] = None,
):
    # TOP
    for i1 in range(x, x + width):
        stage.place_at(wall_entity, y, i1)
    # BOTTOM
    for i2 in range(x, x + width):
        stage.place_at(wall_entity, y + height - 1, i2)
    # LEFT
    for j1 in range(y, y + height):
        stage.place_at(wall_entity, j1, x)
    # RIGHT
    for j2 in range(y, y + height):
        stage.place_at(wall_entity, j2, x + width - 1)
    # Remove hole
    if enter_y != -1 and enter_x != -1 and stage.entity_at(enter_y, enter_x) == wall_entity:
        if door_entity:
            stage.place_at(door_entity, enter_y, enter_x)
        else:
            stage.unset(enter_y, enter_x)
    # Shove any extras
    if extras:
        for extra in extras:
            (extra_entity, y, x) = extra
            stage.place_at(extra_entity, y, x)


def place(stage, y: int, x: int, mask: [], key: {}, default_bg: Entity = None):
    for j in range(0, len(mask)):
        for i in range(0, len(mask[0])):
            mask_code = mask[j][i]
            if mask_code != " ":
                entity = key[mask_code]
                if not isinstance(entity, tuple):
                    stage.place_at(entity, y + j, x + i)
                else:
                    to_place = entity[0].get_instance() if entity[1] else entity[0]
                    if to_place:
                        stage.place_at(to_place, y + j, x + i)
                    if len(entity) > 2 and entity[2]:
                        stage.place_at_bg(entity[2], y + j, x + i)
                    elif default_bg:
                        stage.place_at_bg(default_bg, y+j, x+i)
