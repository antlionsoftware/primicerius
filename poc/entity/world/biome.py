from poc.entity.entity import Entity, Tag
from poc.entity.entity_lib import ground_dirt, basic_floor_entity


class Biome:
    def __init__(self, name: str, default_ground: Entity, temp_range: (int, int), wildlife: [Entity]):
        self.name = name
        assert Tag.GROUND in default_ground.get_tags()
        self.default_ground = default_ground
        self.temp_range = temp_range
        self.wildlife = wildlife


class BiomeLib:
    GRASSLAND = Biome(
        name="Grassland",
        default_ground=ground_dirt,
        temp_range=(16, 26),
        wildlife=[]
    )
