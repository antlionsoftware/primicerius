import math

from poc.engine.keyboard_input_manager import Key
from poc.entity.entity import Entity, Tag
from poc.entity.entity_lib import basic_ground_entity, meta_marker_player_placement, random_fill_container
from poc.entity.world.area import Area
from poc.utils.log_manager import logger


class Stage:
    def __init__(self, area: Area, y_coord: int, x_coord: int, height: int, width: int, default_bg: Entity = basic_ground_entity, outside: bool = True):
        self.area = area
        self.y_coord = y_coord
        self.x_coord = x_coord
        self.height = height
        self.width = width
        self._entities = [[None for x in range(width)] for y in range(height)]
        self._entities_under = [[None for x in range(width)] for y in range(height)]
        self._static_seen = [[False for x in range(width)] for y in range(height)]
        self._bg_entities = [[default_bg for x in range(width)] for y in range(height)]
        self._optimize_entities_by_tag = {}
        self._player_marker_y = -1
        self._player_marker_x = -1
        self._outside = outside

    def is_outside(self):
        return self._outside

    def _add_entity_by_tag(self, entity: Entity):
        for tag in entity.get_tags():
            if not tag in self._optimize_entities_by_tag.keys():
                self._optimize_entities_by_tag[tag] = []
            self._optimize_entities_by_tag[tag].append(entity)

    def add_stage_portal(self, entity: Entity, stage):
        import uuid
        from poc.engine.actions.entity_actions import ChangeStageAction
        from poc.engine.menu import Menu
        from poc.engine.actions.menu_actions import OpenPopUpMenuActionCentered
        from poc.engine.actions.action import CompositeAction
        from poc.engine.actions.input_actions import UnpromoteKeyboardListenerAction
        from poc.engine.actions.menu_actions import ClosePopUpMenuAction
        from poc.engine.actions.menu_actions import ClosePromotedPopUpAction
        loc_change_menu = Menu(
            str(uuid.uuid4()),
            [
                ("Yes", CompositeAction([ChangeStageAction(stage), UnpromoteKeyboardListenerAction(), ClosePopUpMenuAction()])),
                # TODO The following composite action should be its own action
                ("No", ClosePromotedPopUpAction()),
            ],
            "Change locations?",
        )
        entity.set_on_use_action(OpenPopUpMenuActionCentered(loc_change_menu))

    def _remove_entity_by_tag(self, entity: Entity):
        for tag in entity.get_tags():
            if entity in self._optimize_entities_by_tag[tag]:
                self._optimize_entities_by_tag[tag].remove(entity)

    def place_player_at_marker(self):
        from poc.entity import player_inst
        if self._player_marker_y >= 0 and self._player_marker_x >= 0:
            self.place_at(player_inst, self._player_marker_y, self._player_marker_x)
        else:
            raise Exception("Could not find meta_marker_entity in which to place the player.")

    def entity_at(self, y: int, x: int) -> Entity:
        if not (0 <= y < self.height) or not (0 <= x < self.width):
            return None
        return self._entities[y][x]

    def entity_under(self, y: int, x: int) -> Entity:
        if not (0 <= y < self.height) or not (0 <= x < self.width):
            return None
        return self._entities_under[y][x]

    def was_static_entity_seen_by_player(self, y: int, x: int) -> bool:
        return self._entities[y][x] and self._entities[y][x].get_static() and self._static_seen[y][x]

    def set_static_seen_by_player(self, y: int, x: int):
        assert self._entities[y][x] and self._entities[y][x].get_static()
        self._static_seen[y][x] = True

    def clear_visible(self):
        self._static_seen = [[False for x in range(self.width)] for y in range(self.height)]

    def bg_entity_at(self, y: int, x: int):
        if not (0 <= y < self.height) or not (0 <= x < self.width):
            return None
        return self._bg_entities[y][x]

    def move_entity(self, entity: Entity, direction: Key) -> (bool, Entity):
        """
        Attempts to move the selected entity in the given direction. Returns the entity blocking the move, or None if
        the move was successful.
        """
        prev_j = -1
        prev_i = -1
        for y in range(0, self.height):
            for x in range(0, self.width):
                if entity == self.entity_at(y, x):
                    prev_j = y
                    prev_i = x
                    break

        new_j = prev_j
        new_i = prev_i
        # TODO: Proper direction enums
        if direction == Key.A_UP:
            new_j = new_j - 1
        elif direction == Key.A_DOWN:
            new_j = new_j + 1
        elif direction == Key.A_LEFT:
            new_i = new_i - 1
        elif direction == Key.A_RIGHT:
            new_i = new_i + 1

        if not (0 <= new_j < self.height) or not (0 <= new_i < self.width):
            return False, None

        target = self._entities[new_j][new_i]
        target_bg = self._bg_entities[new_j][new_i]
        if target and not target.get_tile():
            logger.debug(target)
            logger.debug(type(target))
        if (not target or target.has_tag(Tag.PASSABLE)) and target_bg.has_tag(Tag.PASSABLE):
            entity_under = self._entities_under[prev_j][prev_i]
            if entity_under:
                self.place_at(entity_under, prev_j, prev_i)
                self._entities_under[prev_j][prev_i] = None
            if target:
                self.unset(new_j, new_i)
                self._entities_under[new_j][new_i] = target
            if not entity_under:
                self.unset(prev_j, prev_i)
            self.place_at(entity, new_j, new_i)
            # Special case - player is the mover, and the previous tile had an open door, and player autoclosedoor is on.
            from poc.entity import player_inst
            if (
                entity == player_inst
                and player_inst.autoclosedoor
                and entity_under
                and entity_under.get_tags()
                and Tag.DOOR in entity_under.get_tags()
            ):
                success, _, _ = entity_under.on_use()
                if success:
                    from poc.engine.actions.game_window_actions import DisplayInTextScroll

                    DisplayInTextScroll.do("You close the door behind you.")
            return True, None
        else:
            return False, target if target and not target.has_tag(Tag.PASSABLE) else target_bg

    def place_at(self, entity: Entity, y: int, x: int):
        if entity.get_identifier() == meta_marker_player_placement.get_identifier():
            logger.debug("Player placement set at (%i,%i)." % (y, x))
            self._player_marker_y = y
            self._player_marker_x = x
        else:
            self._entities[y][x] = entity
            self._add_entity_by_tag(entity)

    def place_at_bg(self, bg_entity: Entity, y: int, x: int):
        self._bg_entities[y][x] = bg_entity
        self._add_entity_by_tag(bg_entity)

    def unset(self, y: int, x: int):
        entity = self._entities[y][x]
        self._entities[y][x] = None
        self._remove_entity_by_tag(entity)

    def get_pos(self, entity: Entity) -> (int, int):
        for y in range(0, self.height):
            for x in range(0, self.width):
                if entity == self.entity_at(y, x) or entity == self.bg_entity_at(y, x):
                    return y, x
        raise Exception("Entity %s is not on the stage." % entity.get_identifier())

    def get_all_entities(self):
        entities = []
        for y in range(0, self.height):
            for x in range(0, self.width):
                if self.entity_at(y, x):
                    entities.append(self.entity_at(y, x))
                if self.bg_entity_at(y, x):
                    entities.append(self.bg_entity_at(y, x))
        return entities

    def get_entities_matching_tag(self, tag: Tag) -> []:
        return self._optimize_entities_by_tag[tag] if tag in self._optimize_entities_by_tag.keys() else []

    def scan_and_fill_stage_containers(self):
        for container in self.get_entities_matching_tag(Tag.CONTAINER):
            random_fill_container(container)

    @staticmethod
    def get_dist(y1: int, x1: int, y2: int, x2: int):
        return round(math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2), 3)

    @staticmethod
    def get_mask_template():
        return {
            "1": (None, False, None),
            "2": (None, False, None),
            "3": (None, False, None),
            "4": (None, False, None),
            "5": (None, False, None),
            "6": (None, False, None),
            "7": (None, False, None),
            "8": (None, False, None),
            "9": (None, False, None),
            "0": (None, False, None),
            "a": (None, False, None),
            "b": (None, False, None),
            "c": (None, False, None),
            "d": (None, False, None),
            "e": (None, False, None),
            "f": (None, False, None),
            "g": (None, False, None),
            "h": (None, False, None),
            "i": (None, False, None),
            "j": (None, False, None),
            "k": (None, False, None),
            "l": (None, False, None),
            "m": (None, False, None),
            "n": (None, False, None),
            "o": (None, False, None),
            "p": (None, False, None),
            "q": (None, False, None),
            "r": (None, False, None),
            "s": (None, False, None),
            "t": (None, False, None),
            "u": (None, False, None),
            "v": (None, False, None),
            "w": (None, False, None),
            "x": (None, False, None),
            "y": (None, False, None),
            "z": (None, False, None),
            "A": (None, False, None),
            "B": (None, False, None),
            "C": (None, False, None),
            "D": (None, False, None),
            "E": (None, False, None),
            "F": (None, False, None),
            "G": (None, False, None),
            "H": (None, False, None),
            "I": (None, False, None),
            "J": (None, False, None),
            "K": (None, False, None),
            "L": (None, False, None),
            "M": (None, False, None),
            "N": (None, False, None),
            "O": (None, False, None),
            "P": (None, False, None),
            "Q": (None, False, None),
            "R": (None, False, None),
            "S": (None, False, None),
            "T": (None, False, None),
            "U": (None, False, None),
            "V": (None, False, None),
            "W": (None, False, None),
            "X": (None, False, None),
            "Y": (None, False, None),
            "Z": (None, False, None),
            "@": meta_marker_player_placement
        }
