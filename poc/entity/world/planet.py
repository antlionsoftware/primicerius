from enum import Enum

from poc.entity.world import System


class PlanetSize(Enum):
    TINY = 1
    SMALL = 9
    MEDIUM = 25
    LARGE = 49
    HUGE = 81


class Planet:
    def __init__(self, system: System, name: str, size: PlanetSize):
        self.system = system
        self.name = name
        self.size = size
        from poc.entity.world import Area
        self.planet_area = Area(self, name, None)
