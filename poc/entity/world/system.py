class System:
    def __init__(self, name: str):
        self.name = name
        self.planets = []

    def register_planet(self, planet):
        from poc.entity.world import Planet

        assert isinstance(planet, Planet)
        self.planets.append(planet)
