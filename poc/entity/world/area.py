from poc.entity.world.biome import Biome
from poc.entity.world.planet import Planet


class Area:
    def __init__(self, planet: Planet, name: str, biome: Biome):
        self.planet = planet
        self.name = name
        self._stages = [[None for x in range(3)] for y in range(3)]
        self.biome = biome

    def register_stage(self, stage):
        from poc.entity.world import Stage

        assert isinstance(stage, Stage)
        self._stages[stage.y_coord][stage.x_coord] = stage
