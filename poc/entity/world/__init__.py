# > System    > Planet    > Area  > Stage > Tile
# TODO        > Moon      > Stage > Tile
# TODO        > Station   > Stage > Tile
from poc.entity.entity_lib import ship_block, ship_block_window, ship_block_entrance, ship_block_engine, \
    meta_hidden, meta_marker_player_placement, ship_floor_entity, ship_block_upper, ship_block_lower, ship_block_left, \
    ship_block_window_lower, meta_black, ship_block_panel_upper, ship_locker_right, ship_block_control_panel, \
    ship_block_seat, ship_door_entity, ship_locker_left, basic_bed_entity, ship_block_control_panel_right, \
    ship_block_control_panel_left, wood_wall_entity, basic_door_entity, basic_floor_entity, window_entity, water_entity, \
    human_adult_male, human_adult_female, wolf_entity, farm_bg, hidden_door_entity, basic_chest, get_signpost_entity, \
    basic_ground_entity, counter_entity, stool_entity, goblin_entity, gate_door_entity, wall_fence_wood_entity, \
    station_block, station_block_bg, space_block, station_window, station_door, station_locker, station_block_glowing, \
    station_control_panel, table_entity, station_panel_left
from poc.entity.world.system import System
from poc.entity.world.planet import Planet, PlanetSize
from poc.entity.world.area import Area
from poc.entity.world.biome import BiomeLib
from poc.entity.world.placement_utils import place
from poc.entity.world.stage import Stage

# Demo world
demo_system = System("Alpha System")
demo_planet = Planet(demo_system, "Lurele Goyar", PlanetSize.TINY)
demo_area = Area(demo_planet, "Madiy", BiomeLib.GRASSLAND)
player_ship = Stage(demo_area, 0, 0, 30, 80, ship_floor_entity, outside=False)
demo_stage = Stage(demo_area, 0, 0, 60, 160)

small_ship_mask = [
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU█www█UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUU█████UU█▀ P ▀█UU█████UUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUU██V█████  C  █████V██UUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUU██V██B rC   Cl B██V██UUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUU██V█R  █     █  L█V██UUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUU█████  D     D  █████UUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU███████     ███████UUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU█         █UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU█         █UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU██████       ██████UUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUU█▀    D   @   D    ▀█UUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUU█     ████H████     █UUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUU█▄ppp▄█UUUUUUU█▄ppp▄█UUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUU██VVV██UUUUUUU██VVV██UUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU"
]
exit_door = ship_block_entrance.get_instance()
control_panel = ship_block_control_panel.get_instance()
small_ship_mask_key = {
    "█": ship_block,
    "▀": ship_block_upper,
    "▄": ship_block_lower,
    "w": (ship_block_window_lower, False, meta_black),
    "p": (ship_block_panel_upper, False, meta_black),
    "P": control_panel,
    "▌": ship_block_left,
    "A": ship_block_window,
    "H": (exit_door, False),
    "V": ship_block_engine,
    "U": meta_hidden,
    "L": (ship_locker_right, True, meta_black),
    "R": (ship_locker_left, True, meta_black),
    "l": (ship_block_control_panel_right, False, meta_black),
    "r": (ship_block_control_panel_left, False, meta_black),
    "D": (ship_door_entity, True, ship_floor_entity),
    "C": ship_block_seat,
    "B": basic_bed_entity,
    "@": meta_marker_player_placement
}

stage_mask = [
    "                                              44                                                                                                                ",
    " 1131113111311131113111311                    44                                                                                                                ",
    " 1Bxx1xxB1Bxx1xxB1Bxx1Bxb11111311111          44          11111                                                                                                 ",
    " 1xxx1xxx1xxx1xxx1xxx1xxx1xxxxxxxxx1         444         11xxx11                                                                                                ",
    " 1xxx1xxx1xxx1xxx1xxx1xxx1xxxxxxx6x1         44          1xxxxx1                                                                                                ",
    " 1112121111121211111212111xxxxxxxxx3         44          axxxxb1   11111      111111111111                                                                      ",
    " 1xxxxxxxxxxxxxxxxxxxxxxx1xxxxxxxxx1         44          1xxxxx1  11xxx11     1mxxxxx.xx71                                                                      ",
    " 1xxxxxxxxxxxxxxxxxxxxxxx1xxxxxxxxx1         44          11xxx11  1xxxxx1     3xxxxxx.xx73                                                                      ",
    " 11111111111111111111211111111111211                      11111   1xxxxx1    j1xxxxxxixxx1                                                                      ",
    " 1xxxxxxxxxxxxxxxxxxxxxxx1xxxxxxxxx1                              1xxxxxa  xxx2xxxxxx....1                                                                      ",
    " 1xxxxxx5xxxxxxxxxxxxxxxx2xxxxxxxxx1         44                   11xxx11  x  1xxxxxxixxg1                                                                      ",
    " 3xxxxxxxxxxxxxxxxxxxxxxx11111211111         44                    11111   x  3xxxxxx.xxx3                                                                      ",
    " 3xxxxxxxxxxxxxxxxxxxxxxx1         •         444                           x  1kxxxxx.xxB1                                                                      ",
    " 1xxxxxxxxxxxxxxxxxxxxxxx1 9999999 •          444              xxxxxxxxxxxxx  111111111111                                                                      ",
    " 1xxxxxxxxxxxxxxxxxxxxxxx1 9999999 •           444444444       x                                                                                                ",
    " 1111331113311112111331111••••••••••               4444444444  x 4444444444444444                                                                               ",
    "                x c        1111113111 111xx11 11xx111    xxxxxxx                444444444444              44444444444444444  4444444444                         ",
    "        x11111  x          1xx2xxxxx1 1xxxxx1 1xxxxx1    x  1113111                       44444444444444444444                       444444444444444444444444444",
    "        xxxxx1  x          3xx1xxxxx1 xxxxxx1 1xxxxxx    x  1xxxxB1                                                                                             ",
    "        1xxxxxxxxx         1111112111 1xxx111 111xxxx    x  1xxxxx1                                                                                             ",
    "        1xxxx1   xx              x       x       x       xxx2xxxxx1                                                                                             ",
    "        xx1xxx    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  1xxxxx1                                                                                             ",
    "                      xh      x      x                   x  1113111                                                                                             ",
    "        4         113121311 112111 11211                 x                                                                                                      ",
    "        444       1xxxxxxx1 1xxxx1 1xxx1  •••••111311    x  1113111                                                                                             ",
    "       444444     1xxexexe1 1xxxf1 1xxx1  •99991Bxxx1    x  1xxxmB1                                                                                             ",
    "      444444444   1xxddddd1 1xxxx3 1Bxm1  •99991xxxx2xxxxxxx2xxxxx1                                                                                             ",
    "        4444444   1xxxxfxx1 1xmxB1 11311  •    1xxxx1    x  1xxxxx1                                                                                             ",
    "       4444444    113111311 113111        •    2xfxx1    x  1xmxxx1                                                                                             ",
    "        44444                             •••••111311    x  1113111                                                                                             ",
    "      44444444444444                                     x                                                                                                      ",
    "     444444444444444444                         1113111  x                                                                                                      ",
    "     44444444444444444444                       1xxxfx1  x  1113111                                                                                             ",
    "    444444444444444444444                       1xxxxx2xxx  1xxxxx1                                                                                             ",
    "    444444444444444444444                       1xxxmx1  xxx2xxxxx1                                                                                             ",
    "    444444444  4444444444                       1Bxxxx1  x  1xxxxB1                                                                                             ",
    "    444444444   444444444                       1113111  x  1113111                                                                                             ",
    "     4444444444444444444                                 x                                                                                                      ",
    "     4444444444444444444                                 x                                                                                                      ",
    "      44444444444444444                                  x                                                                                                      ",
    "         444444444444                       XwwwX        x                                                                                                      ",
    "             444                    XXXXX  XXXXXXX  XXXXXx                                                                                                      ",
    "              44                    XXVXXXXXXXXXXXXXXXVXXx                                                                                                      ",
    "              44                    XXVXXXXXXXXXXXXXXXVXXx                                                                                                      ",
    "              44                    XXVXXXXXXXXXXXXXXXVXXx                                                                                                      ",
    "              44                    XXXXXXXXXXXXXXXXXXXXXx                                                                                                      ",
    "               444                   XXXXXXXXXXXXXXXXXXX x                                                                                                      ",
    "                 4444                    XXXXXXXXXXX     x                                                                                                      ",
    "                    44                   XXXXXXXXXXX     x                                                                                                      ",
    "                    44               XXXXXXXXXXXXXXXXXXX x                                                                                                      ",
    "                    44              XXXXXXXXXXXXXXXXXXXXXx                                                                                                      ",
    "                     4              XXXXXXXXXXHXXXXXXXXXXx                                                                                                      ",
    "                                    XXXXXXX   @   XXXXXXXx                                                                                                      ",
    "                                    XXVVVXX       XXVVVXXx                                                                                                      ",
    "                                                         x                                                                                                      ",
    "                                                        xx                                                                                                      ",
    "                                                    xxxxx                                                                                                       ",
    "                                                                                                                                                                ",
    "                                                                                                                                                                ",
    "                                                                                                                                                                ",
]
ship_door = ship_block_entrance.get_instance()
stage_mask_key = {
    "1": wood_wall_entity,
    "2": (basic_door_entity, True, basic_floor_entity),
    "3": (window_entity, False, basic_floor_entity),
    "4": (None, None, water_entity),
    "5": (human_adult_male, True, basic_floor_entity),
    "6": (human_adult_female, True, basic_floor_entity),
    "7": (wolf_entity, True, basic_floor_entity),
    # "8": ,
    "9": (None, None, farm_bg),
    "a": hidden_door_entity,
    "b": (basic_chest, True, basic_floor_entity),
    "c": (get_signpost_entity("The Hammer And Hare Inn"), False, basic_ground_entity),
    "d": (counter_entity, False, basic_floor_entity),
    "e": (stool_entity, False, basic_floor_entity),
    "f": (human_adult_female, True, basic_floor_entity),
    "g": (goblin_entity, True, basic_floor_entity),
    "h": (get_signpost_entity("General Store"), False, basic_ground_entity),
    "i": (gate_door_entity, True, basic_floor_entity),
    "j": (get_signpost_entity("Jail"), False, basic_ground_entity),
    "k": (basic_chest, True, basic_floor_entity),
    "m": (human_adult_male, True, basic_floor_entity),
    "w": (ship_block_window_lower, False, meta_black),
    "x": (None, None, basic_floor_entity),
    "X": ship_block,
    "A": ship_block_window,
    "B": basic_bed_entity,
    "H": (ship_door, False, basic_floor_entity),
    "V": ship_block_engine,
    "•": (wall_fence_wood_entity, False, basic_ground_entity),
    ".": (wall_fence_wood_entity, False, basic_floor_entity),
    "@": meta_marker_player_placement
}

place(demo_stage, 0, 0, stage_mask, stage_mask_key)
place(player_ship, 0, 0, small_ship_mask, small_ship_mask_key)
demo_stage.add_stage_portal(ship_door, player_ship)
player_ship.add_stage_portal(exit_door, demo_stage)
demo_stage.scan_and_fill_stage_containers()
player_ship.scan_and_fill_stage_containers()

player_station = Stage(demo_planet.planet_area, 0, 0, 30, 80, ship_floor_entity, outside=False)
player_station_mask = [
    "UUU█H█UUUUUUUUU███UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "U███@███UUUUU███ ███UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "U█     █UUUUU█     █UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "U█     █UUUUU█     █UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "U███D████WWW████D███UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UU█   █       █   █UUUUUUUUUUUU████WWWWW████UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UU█   █       █   █UUUUUUUUUUUU█           █UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUW   █       █   WUUUUUUUUUUUU█Q  ccccc   █UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UU█   W       W   █UUUUUUUUUUUU█Q  tttttc  █UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUW   █ ttttt █   WUUUUUUUUUUUU█Q  ccccc   █UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UU█   D       D   █UUUUUUUUUUUU█           █UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UU█   D       D   █UUUUUUUUUUUU██████D██████UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UU████████D████████UUUUUUUUUUUUUUUU█   █UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUU█   █UUUUUUUUU████WW████UUU█   █UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUW   WUUUUUUUUU█G█ PP █G█UUU█   █UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUUUU█   █UUUUUUUUU█G█    █G█UUU█   █UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUU████D████UUUUUUU█G█    █G█UUU█   █UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    "UUUUUU█       ████████████DD████████   ██████████████████████UUUUUUUUUUUUUUUUUUU",
    "UU█████       D                                             █UUUUUUUUUUUUUUUUUUU",
    "UU█   █       ███████████████████████WWW█████WWW█████WWW█████UUUUUUUUUUUUUUUUUUU",
    "UU█   █       █UUUUUUUUUUUUUUUUUUU█UUUUUUU█UUUUUUU█UUUUUUU█UUUUUUUUUUUUUUUUUUUUU",
    "UU█   D       █UUUUUUUUUUUUUUUUUUU█UUUUUUU█UUUUUUU█UUUUUUU█UUUUUUUUUUUUUUUUUUUUU",
    "UU████████D████████████████UUUUU█████UUU█████UUU█████UUU█████UUUUUUUUUUUUUUUUUUU",
    "UU█                       █UUUUU█GGG█UUU█GGG█UUU█GGG█UUU█GGG█UUUUUUUUUUUUUUUUUUU",
    "UU█                       █UUUUU█GGG█UUU█GGG█UUU█GGG█UUU█GGG█UUUUUUUUUUUUUUUUUUU",
    "UU█D██D██D██D██D██D██D██D██UUUUU█GGG█UUU█GGG█UUU█GGG█UUU█GGG█UUUUUUUUUUUUUUUUUUU",
    "UU█  █  █  █  █  █  █  █  █UUUUU█GGG█UUU█GGG█UUU█GGG█UUU█GGG█UUUUUUUUUUUUUUUUUUU",
    "UU█1B█1B█1B█1B█1B█1B█1B█1B█UUUUU█GGG█UUU█GGG█UUU█GGG█UUU█GGG█UUUUUUUUUUUUUUUUUUU",
    "UU█████████████████████████UUUUU█████UUU█████UUU█████UUU█████UUUUUUUUUUUUUUUUUUU",
    "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU"
]
player_station_mask_key = {
    "█": (station_block, False, station_block_bg),
    "B": basic_bed_entity,
    "D": (station_door, True, ship_floor_entity),
    "G": (station_block_glowing, False, meta_black),
    "H": (ship_door_entity, False, basic_floor_entity),
    "P": (station_control_panel, False, station_block_bg),
    "Q": (station_panel_left, False, station_block_bg),
    "U": space_block,
    "W": (station_window, False, meta_black),
    "t": table_entity,
    "c": stool_entity,
    "1": (station_locker, True, station_block_bg),
    "@": meta_marker_player_placement
}
place(player_station, 0, 0, player_station_mask, player_station_mask_key)
