from poc.engine import DialogueGenerator
from poc.engine.actions.environment_actions import RestPromptAction
from poc.engine.actions.menu_actions import OpenDialogueAction
from poc.engine.useables.useable import SimpleTextUseable
from poc.entity.entity import Entity, ActiveToggleEntity, StatBlock, Tag
from poc.entity.item.armour import Armour
from poc.entity.item.container import Container
from poc.entity.item.item import Item, EnergyPack
from poc.entity.item.weapon import Weapon
from poc.entity.tile import Tile, FlashingTile
from poc.entity.tile_lib import *
from poc.ui.color import *

meta_marker_player_placement = Entity(
    "meta_marker_player_placement",
    "",
    "",
    None,
    makes_noise=False,
    stat_block=None,
    on_death=[],
    tags=[Tag.META]
)
meta_hidden = Entity(
    "meta_hidden",
    "",
    "",
    tile=Tile(CHAR_SHADE_DARK, color=BLACK),
    makes_noise=False,
    stat_block=None,
    on_death=[],
    tags=[Tag.META, Tag.TRANSPARENT]
)
meta_black = Entity(
    "",
    "",
    "",
    tile=Tile(CHAR_BLOCK_FULL, color=BLACK),
    makes_noise=False,
    stat_block=None,
    on_death=[],
    tags=[Tag.META]
)

mat_fur = Item("mat_fur", "Fur", "Some discarded fur of an animal.", tags=[Tag.MATERIAL], base_price=1)
mat_fang = Item("mat_fang", "Fang", "A sharp fang.", tags=[Tag.MATERIAL], base_price=1)
mat_iron = Item("mat_iron", "Iron", "A hard metal used in construction.", tags=[Tag.MATERIAL])

material_item_library = [
    mat_fang,
    mat_fur,
    mat_iron,
]

ammo_bullet_sc = Item(
    item_id="bullet_sc",
    name="Bullet (Small Caliber)",
    description="Bullets for a small-caliber firearm.",
    tags=[Tag.AMMO],
    base_price=1
)

ammo_item_library = [
    ammo_bullet_sc
]

# Daggers - High-variance low damage, positive bonus
weapon_dagger_iron = Weapon(
    weapon_id="weapon_dagger_iron",
    name="Iron Dagger",
    description="A simple iron dagger.",
    dmg=(1, 4),
    bonus=1,
    ranged=False,
    governing_attribute="strength",
    tags=[Tag.BLADE, Tag.WEAPON],
)
# Short swords - High-variance medium damage, no bonus
weapon_short_sword_iron = Weapon(
    weapon_id="weapon_short_sword_iron",
    name="Iron Short Sword",
    description="A simple iron short sword.",
    dmg=(1, 6),
    bonus=0,
    ranged=False,
    governing_attribute="strength",
    tags=[Tag.BLADE, Tag.WEAPON],
)
# Long swords - High-variance medium-high damage, no bonus
weapon_long_sword_iron = Weapon(
    weapon_id="weapon_long_sword_iron",
    name="Iron Long Sword",
    description="A simple iron long sword.",
    dmg=(1, 8),
    bonus=0,
    ranged=False,
    governing_attribute="strength",
    tags=[Tag.BLADE, Tag.WEAPON],
)
# Axes - High-variance high damage, negative bonus
weapon_axe_iron = Weapon(
    weapon_id="weapon_axe_iron",
    name="Iron Axe",
    description="A simple iron hand-axe.",
    dmg=(2, 12),
    bonus=-1,
    ranged=False,
    governing_attribute="strength",
    tags=[Tag.BLADE, Tag.WEAPON],
)
# Energy pistols - Ranged, requires energy, constant damage
weapon_junk_pistol = Weapon(
    weapon_id="weapon_junk_pistol",
    name="Junk Pistol",
    description="A simple energy pistol assembled from scrap materials.",
    dmg=(8, 8),
    bonus=0,
    ranged=True,
    required_energy=2,
    governing_attribute="dexterity",
    governing_attribute_bonus_damage=False
)
# Pistols - Ranged, low-variance medium-high damage
weapon_pistol = Weapon(
    weapon_id="weapon_pistol",
    name="Pistol",
    description="A simple ballistic pistol.",
    dmg=(4, 8),
    bonus=0,
    ranged=True,
    governing_attribute="dexterity",
    tags=[Tag.RANGED, Tag.BALLISTIC, Tag.WEAPON],
    required_ammo=ammo_bullet_sc,
    min_req=(0, 10, 0, 0, 0, 0)
)
weapon_revolver = Weapon(
    weapon_id="weapon_revolver",
    name="Revolver",
    description="A powerful ballistic pistol with a strong kickback.",
    dmg=(8, 12),
    bonus=-2,
    ranged=True,
    governing_attribute="dexterity",
    tags=[Tag.RANGED, Tag.BALLISTIC, Tag.WEAPON],
    required_ammo=ammo_bullet_sc,
    min_req=(0, 12, 0, 0, 0, 0)
)

weapon_item_library = [
    weapon_dagger_iron,
    weapon_long_sword_iron,
    weapon_short_sword_iron,
    weapon_axe_iron,
    weapon_pistol,
    weapon_revolver,
    weapon_junk_pistol,
]

armour_basic_shirt = Armour(
    armour_id="armour_basic_shirt",
    name="Basic Shirt",
    description="A simple shirt made of cloth, offering no extra protection.",
    ac_bonus=0,
    dam_reduce_ballistic=0,
    dam_reduce_blade=0,
    dam_reduce_energy=0,
    tags=[Tag.ARMOUR],
    min_req=(0, 0, 0, 0, 0, 0)
)

armour_item_library = [
    armour_basic_shirt
]

energy_pack_basic = EnergyPack("energy_pack_basic", "Basic Energy Pack", max_energy=10.0, energy_regen=0.2, description="A basic energy pack.")

accessory_item_library = [
    energy_pack_basic
]

junk_dog_plush = Item(
    "junk_dog_plush",
    "Pet Dog Plush",
    "A dog plushie. It is very soft to the touch.",
    use=SimpleTextUseable("You pet the plush dog."),
    tags=[Tag.JUNK],
)

junk_item_library = [
    junk_dog_plush,
]

basic_chest = Container("basic_chest", "Chest", "A simple wooden chest.")
basic_ground_entity = Entity(
    "basic_ground", "Ground", "A ground made of dirt.", Tile(" ", color=YELLOW), tags=[Tag.BACKGROUND, Tag.GROUND, Tag.PASSABLE, Tag.TRANSPARENT]
)
ground_dirt = basic_ground_entity
basic_floor_entity = Entity(
    "basic_floor", "Floor", "A floor.", Tile(" ", color=WHITE), tags=[Tag.BACKGROUND, Tag.PASSABLE, Tag.TRANSPARENT]
)
basic_bed_entity = Entity(
    identifier="basic_bed",
    name="Bed",
    description="Furniture used to sleep on. It does not look very comfortable or warm.",
    tile=Tile(char=ENTITY_BED, color=BLACK),
    tags=[Tag.TRANSPARENT, Tag.BED],
    on_use_action=RestPromptAction()
)

basic_door_entity = ActiveToggleEntity(
    identifier="basic_door",
    on_toggle_texts=("You close the door.", "You open the door."),
    toggle_costs=(1, 1),
    names=("Door", "Door"),
    descriptions=("A standard-looking door.", "A standard-looking door. It is open."),
    tiles=(Tile(ENTITY_DOOR_CLOSED), Tile(ENTITY_DOOR_OPEN)),
    make_noises=(False, False),
    tags=([Tag.DOOR], [Tag.DOOR, Tag.PASSABLE, Tag.TRANSPARENT]),
)
farm_bg = Entity(
    identifier="farm_bg",
    name="Farm Plot",
    description="A farm plot.",
    tile=Tile(char=CHAR_SHADE_LIGHT, color=GREEN),
    static=False,
    on_use_action=None,
    makes_noise=False,
    stat_block=None,
    tags=[Tag.BACKGROUND, Tag.PASSABLE, Tag.TRANSPARENT]
)
hidden_door_entity = ActiveToggleEntity(
    identifier="hidden_door",
    on_toggle_texts=("You close the hidden door.", "You open a hidden door."),
    toggle_costs=(1, 1),
    names=("A Strange Wall (?)", "Hidden Door"),
    descriptions=("The wall here looks strange...", "It was a hidden door."),
    tiles=(Tile(CHAR_BOX_100), Tile(ENTITY_DOOR_OPEN)),
    make_noises=(False, False),
    tags=([Tag.DOOR], [Tag.DOOR, Tag.PASSABLE, Tag.TRANSPARENT]),
)

table_entity = Entity(
    "table", "Table", "A table to place things on.", Tile(ENTITY_TABLE), tags=[Tag.TRANSPARENT]
)
tree_entity = Entity("tree", "Tree", "A large tree.", Tile(ENTITY_TREE, color=BLACK))
stool_entity = Entity(
    "stool", "Stool", "A small stool on which you can sit.", Tile(ENTITY_STOOL), tags=[Tag.TRANSPARENT, Tag.PASSABLE]
)
counter_entity = Entity(
    "counter", "Counter", "A counter on which things can be placed.", Tile(ENTITY_TABLE), tags=[Tag.TRANSPARENT]
)
window_entity = Entity(
    "window", "Window", "A window.", tile=Tile(CHAR_BOX_50), static=True, tags=[Tag.TRANSPARENT]
)
wood_wall_entity = Entity("wall_wood", "Wood Wall", "A wooden wall.", tile=Tile(CHAR_BOX_100), static=True)
wall_fence_wood_entity = Entity("wall_fence_wood", "Wood Fence", "A wooden fence.", tile=Tile(CHAR_BULLET), static=False, tags=[Tag.TRANSPARENT])
gate_door_entity = ActiveToggleEntity(
    identifier="gate_door",
    on_toggle_texts=("You close the gate.", "You open the gate."),
    toggle_costs=(1, 1),
    names=("Gate", "Gate"),
    descriptions=("A gate.", "An open gate."),
    tiles=(Tile(ENTITY_DOOR_CLOSED), Tile(ENTITY_DOOR_OPEN)),
    make_noises=(False, False),
    tags=([Tag.DOOR, Tag.TRANSPARENT], [Tag.DOOR, Tag.PASSABLE, Tag.TRANSPARENT]),
)
water_entity = Entity(
    "water", "Water", "Some water.", tile=Tile(" ", color=BLUE), static=True, tags=[Tag.BACKGROUND, Tag.TRANSPARENT]
)

space_block = Entity("space_block", "Empty Space", "The emptiness of space. When one gazes long into the abyss...", tile=Tile(CHAR_BLOCK_FULL, color=BLACK), tags=[Tag.TRANSPARENT])

station_block = Entity("station_block", "Station", "A space station. The walls are thick and shielded.", tile=Tile(CHAR_SHADE_DARK, color=BLACK), tags=[Tag.TRANSPARENT], static=True)
station_block_glowing = Entity("station_block_glowing", "Station", "A space station. The walls are thick and shielded.", tile=FlashingTile([CHAR_SHADE_DARK, CHAR_SHADE_MEDIUM], colors=[GREEN]), tags=[Tag.TRANSPARENT])
station_block_bg = Entity("station_block", "Station", "A space station. The walls are thick and shielded.", tile=Tile(CHAR_BLOCK_FULL, color=WHITE), tags=[Tag.TRANSPARENT])
station_control_panel = Entity("station_control_panel", "Station Main Control Panel", "A control panel for the station.", tile=Tile(CHAR_BLOCK_HALF_LOWER, GREEN), tags=[Tag.COMPOSED, Tag.TRANSPARENT])
station_door = ActiveToggleEntity(
    identifier="station_door",
    on_toggle_texts=("The door closes.", "The door opens."),
    toggle_costs=(1, 1),
    names=("Station Door", "Station Door"),
    descriptions=("A space station's door. Airtight.", "A space station's door. Airtight. It is open."),
    tiles=(Tile(ENTITY_DOOR_CLOSED), Tile(ENTITY_DOOR_OPEN)),
    make_noises=(False, False),
    tags=([Tag.DOOR, Tag.TRANSPARENT], [Tag.DOOR, Tag.PASSABLE, Tag.TRANSPARENT]),
)
station_locker = Container("station_locker", "Station Locker", "A locker used to store items. Electronically operated.", tile=Tile(CHAR_BLOCK_HALF_LOWER, color=YELLOW), tags=[Tag.CONTAINER, Tag.TRANSPARENT])
station_panel_left = Entity("station_panel_left", "Station Panel", "A control panel.", tile=Tile(CHAR_BLOCK_HALF_LEFT, color=GREEN), tags=[Tag.COMPOSED, Tag.TRANSPARENT])
station_window = Entity("station_window", "Station Window", "A window of a space station. Airtight.", tile=Tile(CHAR_SHADE_DARK, color=BLUE), tags=[Tag.TRANSPARENT], static=True)

ship_block = Entity("ship_block", "Ship", "A vehicle used to travel to outer space.", tile=Tile(CHAR_BLOCK_FULL, color=BLACK), tags=[Tag.COMPOSED, Tag.TRANSPARENT])
ship_block_lower = Entity("ship_block_lower", "Ship", "A vehicle used to travel to outer space.", tile=Tile(CHAR_BLOCK_HALF_LOWER, color=BLACK), tags=[Tag.COMPOSED, Tag.TRANSPARENT])
ship_block_upper = Entity("ship_block_upper", "Ship", "A vehicle used to travel to outer space.", tile=Tile(CHAR_BLOCK_HALF_UPPER, color=BLACK), tags=[Tag.COMPOSED, Tag.TRANSPARENT])
ship_block_left = Entity("ship_block_left", "Ship", "A vehicle used to travel to outer space.", tile=Tile(CHAR_BLOCK_HALF_LEFT, color=BLACK), tags=[Tag.COMPOSED, Tag.TRANSPARENT])
ship_block_right = Entity("ship_block_right", "Ship", "A vehicle used to travel to outer space.", tile=Tile(CHAR_BLOCK_HALF_RIGHT, color=BLACK), tags=[Tag.COMPOSED, Tag.TRANSPARENT])
ship_block_window = Entity("ship_block_window", "Ship", "A vehicle used to travel to outer space.", tile=Tile(CHAR_BLOCK_FULL, color=BLUE), tags=[Tag.COMPOSED, Tag.TRANSPARENT])
ship_block_window_lower = Entity("ship_block_window_lower", "Ship Window", "The window to the ship.", tile=Tile(CHAR_BLOCK_HALF_LOWER, color=BLUE), tags=[Tag.COMPOSED, Tag.TRANSPARENT])
ship_block_control_panel = Entity("ship_block_control_panel", "Ship Main Control Panel", "A control panel for the ship. It allows you to select a destination, and control other options.", tile=Tile(CHAR_BLOCK_HALF_LOWER, color=GREEN), tags=[Tag.COMPOSED, Tag.TRANSPARENT])
ship_block_control_panel_right = Container("ship_block_control_panel_right", "Ship Main Control Panel", "A control panel for the ship. It allows you to select a destination, and control other options.", tile=Tile(CHAR_BLOCK_HALF_LEFT, color=GREEN), tags=[Tag.COMPOSED, Tag.TRANSPARENT])
ship_block_control_panel_left = Container("ship_block_control_panel_left", "Ship Main Control Panel", "A control panel for the ship. It allows you to select a destination, and control other options.", tile=Tile(CHAR_BLOCK_HALF_RIGHT, color=GREEN), tags=[Tag.COMPOSED, Tag.TRANSPARENT])
ship_block_panel_upper = Entity("ship_block_panel_upper", "Ship Control Panel", "A control panel for the ship. Lots of flashing buttons!", tile=Tile(CHAR_BLOCK_HALF_UPPER, color=GREEN), tags=[Tag.COMPOSED, Tag.TRANSPARENT])
ship_block_engine = Entity("ship_block_engine", "Ship", "A vehicle used to travel to outer space.", tile=FlashingTile([CHAR_BLOCK_FULL, CHAR_SHADE_DARK, CHAR_SHADE_MEDIUM, CHAR_SHADE_DARK], colors=[RED]), tags=[Tag.COMPOSED, Tag.TRANSPARENT])
ship_block_seat = Entity("ship_block_seat", "Ship Seat", "A seat used to securely travel through space.", tile=Tile(ENTITY_STOOL, color=BLACK), tags=[Tag.COMPOSED, Tag.TRANSPARENT, Tag.PASSABLE])
ship_block_entrance = Entity("ship_block_entrance", "Ship Door", "The entrance to the ship.", tile=Tile(ENTITY_DOOR_CLOSED, color=BLACK), tags=[Tag.COMPOSED, Tag.TRANSPARENT])
ship_locker_right = Container("ship_locker_right", "Ship Locker", "A locker used to store items.", tile=Tile(CHAR_BLOCK_HALF_LEFT, color=YELLOW), tags=[Tag.COMPOSED, Tag.TRANSPARENT, Tag.CONTAINER])
ship_locker_left = Container("ship_locker_left", "Ship Locker", "A locker used to store items.", tile=Tile(CHAR_BLOCK_HALF_RIGHT, color=YELLOW), tags=[Tag.COMPOSED, Tag.TRANSPARENT, Tag.CONTAINER])
ship_floor_entity = Entity("ship_floor", "Ship Floor", "A floor.", Tile(" ", color=WHITE), tags=[Tag.BACKGROUND, Tag.PASSABLE, Tag.TRANSPARENT])
ship_door_entity = ActiveToggleEntity(
    identifier="ship_door",
    on_toggle_texts=("You close the door.", "You open the door."),
    toggle_costs=(1, 1),
    names=("Ship Door", "Ship Door"),
    descriptions=("An electronic sliding door.", "An electronic sliding door. It is open."),
    tiles=(Tile(ENTITY_DOOR_CLOSED), Tile(ENTITY_DOOR_OPEN)),
    make_noises=(False, False),
    tags=([Tag.DOOR, Tag.TRANSPARENT], [Tag.DOOR, Tag.PASSABLE, Tag.TRANSPARENT]),
)

def get_signpost_entity(text: str = None) -> Entity:
    import uuid
    return Entity(
        "signpost::" + str(uuid.uuid4()), "Signpost%s" % ((": '%s'" % text) if text else ""), "A signpost. %s" % ("Its writing has faded." if not text else ("The sign says says: '%s'" % text)),
        Tile(ENTITY_SIGNPOST, color=BLACK),
        tags=[Tag.TRANSPARENT]
    )

wolf_entity = Entity(
    "creature_wolf",
    "Wolf",
    "A four-legged aggressive canine.",
    Tile(ENTITY_WOLF),
    makes_noise=True,
    stat_block=StatBlock(12, 15, 12, 3, 12, 6, max_health=11, default_ac=13, challenge=1),
    equipment=[Weapon("wolf_claw", "Wolf Claw", "A wolf's claw.")],
    on_death=[(mat_fur, 100), (mat_fur, 50), (mat_fang, 50), (mat_fang, 50)],
    tags=[Tag.ATTACKABLE, Tag.BEHAVIOUR, Tag.BEHAVIOUR_HOSTILE, Tag.RACE_CANINE, Tag.TRANSPARENT]
)

goblin_entity = Entity(
    "creature_goblin",
    "Goblin",
    "A short, green-skinned little creature.",
    Tile(ENTITY_BIPED, color=BLACK),
    makes_noise=True,
    stat_block=StatBlock(8, 14, 10, 10, 8, 8, max_health=7, default_ac=15, challenge=1),
    equipment=[weapon_short_sword_iron],
    on_death=[(mat_fang, 50), (mat_fang, 50), (weapon_short_sword_iron, 95)],
    tags=[Tag.ATTACKABLE, Tag.BEHAVIOUR, Tag.BEHAVIOUR_HOSTILE, Tag.RACE_GOBLINOID, Tag.TRANSPARENT]
)

human_stat_block = StatBlock(10, 10, 10, 10, 10, 10, 4, 10)
human_adult_male = Entity(
    "human_adult_male",
    "Human (Male)",
    "A bipedal ape-like creature.",
    Tile(ENTITY_HUMAN_MALE, color=BLUE),
    makes_noise=True,
    stat_block=human_stat_block,
    on_death=[],
    tags=[Tag.ATTACKABLE, Tag.BEHAVIOUR, Tag.BEHAVIOUR_PASSIVE, Tag.RACE_HUMAN, Tag.TRANSPARENT],
    on_use_action=OpenDialogueAction(DialogueGenerator().get_random("Human"))
)
human_adult_female = Entity(
    "human_adult_female",
    "Human (Female)",
    "A bipedal ape-like creature.",
    Tile(ENTITY_HUMAN_FEMALE, color=RED),
    makes_noise=True,
    stat_block=human_stat_block,
    on_death=[],
    tags=[Tag.ATTACKABLE, Tag.BEHAVIOUR, Tag.BEHAVIOUR_PASSIVE, Tag.RACE_HUMAN, Tag.TRANSPARENT],
    on_use_action=OpenDialogueAction(DialogueGenerator().get_random("Human"))
)

living_entity_library = [
    goblin_entity,
    human_adult_female,
    human_adult_male,
    wolf_entity,
]

all_item_library = [material_item_library, ammo_item_library, weapon_item_library, armour_item_library,
                    accessory_item_library, junk_item_library]


def random_fill_container(
        container: Container,
        material_odds: (float, float) = (0.5, 0.4, 3),
        ammo_odds: (float, float) = (0.4, 0.4),
        weapon_odds: (float, float) = (0.1, 0.2),
        armour_odds: (float, float) = (0.1, 0.2),
        accessory_odds: (float, float) = (0.1, 0.2),
        junk_odds: (float, float) = (0.5, 0.2)):
    import random
    from poc.utils.log_manager import logger

    extra = True
    if random.random() < material_odds[0]:
        while extra:
            container.insert(random.choice(material_item_library), 1)
            extra = random.random() < material_odds[1]

    extra = True
    if random.random() < ammo_odds[0]:
        while extra:
            container.insert(random.choice(ammo_item_library), 6)
            extra = random.random() < ammo_odds[1]

    extra = True
    if random.random() < weapon_odds[0]:
        while extra:
            container.insert(random.choice(weapon_item_library).get_instance(), 1)
            extra = random.random() < weapon_odds[1]

    # extra = True
    # if random.random() < armour_odds[0]:
    #     while extra:
    #         container.insert(random.choice(armour_item_library), 1)
    #         extra = random.random() < armour_odds[1]

    extra = True
    if random.random() < accessory_odds[0]:
        while extra:
            container.insert(random.choice(accessory_item_library), 1)
            extra = random.random() < accessory_odds[1]

    extra = True
    if random.random() < junk_odds[0]:
        while extra:
            container.insert(random.choice(junk_item_library), 1)
            extra = random.random() < junk_odds[1]


