import unittest

from poc.engine.actions.action import Action
from poc.entity.behaviour import PriorityCompositeBehaviour, BehaviourFunction
from poc.utils.exceptions import UnimplementedException


class DummyAction(Action):
    pass


class NiceBehaviourFunction(BehaviourFunction):
    def __init__(self):
        self.action = DummyAction()
        self.ticks = 5

    def get_action(self, entity_instance) -> (Action, int):
        return self.action, self.ticks


class BadBehaviourFunction(BehaviourFunction):
    def get_action(self, entity_instance) -> (Action, int):
        return None, 0


class BehaviourFunctionTest(unittest.TestCase):
    def test_behaviourFunctionGet_unimplementedExceptionRaised(self):
        bf = BehaviourFunction()
        with self.assertRaises(UnimplementedException):
            bf.get_action(None)


class PriorityCompositeBehaviourTest(unittest.TestCase):
    def test_noAction_get_returnNone(self):
        pcb = PriorityCompositeBehaviour([])
        self.assertEqual((None, 0), pcb.get_action(None))

    def test_firstFunctionEntityInstance_get_returnFirstFunctionAction(self):
        nbf = NiceBehaviourFunction()
        pcb = PriorityCompositeBehaviour([nbf, BadBehaviourFunction()])
        self.assertEqual((nbf.action, nbf.ticks), pcb.get_action(None))

    def test_secondFunctionAcceptsEntityInstance_get_returnSecondFunctionAction(self):
        nbf = NiceBehaviourFunction()
        pcb = PriorityCompositeBehaviour([BadBehaviourFunction(), nbf])
        self.assertEqual((nbf.action, nbf.ticks), pcb.get_action(None))

    def test_noFunctionAcceptsEntityInstance_get_returnNoneAction(self):
        pcb = PriorityCompositeBehaviour([BadBehaviourFunction(), BadBehaviourFunction()])
        self.assertEqual((None, 0), pcb.get_action(None))
