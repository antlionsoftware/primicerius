import random
from abc import ABC

from poc.engine.actions.action import Action
from poc.utils.exceptions import UnimplementedException


class BehaviourFunction():
    def get_action(self, entity_instance) -> (Action, int):
        """
        Returns an Action along with a tick cost.
        """
        raise UnimplementedException()


class PriorityCompositeBehaviour(BehaviourFunction):
    def __init__(self, behaviour_functions: [BehaviourFunction]):
        self._behaviour_functions = behaviour_functions

    def get_action(self, entity_instance) -> (Action, int):
        for behaviour_function in self._behaviour_functions:
            action, cost = behaviour_function.get_action(entity_instance)
            if action:
                return action, cost
        return None, 0


class CompositeBehaviour(BehaviourFunction):
    def __init__(self, behaviour_functions: [(int, BehaviourFunction)]):
        self._behaviour_functions = behaviour_functions
        total = 0
        for (chance, behaviour) in self._behaviour_functions:
            total = total + chance
        assert total == 100

    def get_action(self, entity_instance) -> (Action, int):
        dice_roll = random.randint(1, 100)
        for (chance, behaviour) in self._behaviour_functions:
            if dice_roll <= chance:
                return behaviour.get_action(entity_instance)
            else:
                dice_roll = dice_roll - chance
        raise Exception("A choice was supposed to have been made but the dice roll was %i." % dice_roll)
