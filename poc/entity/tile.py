from poc.ui.color import BLACK, WHITE


class Tile:
    def __init__(
        self, char: str, color: int = BLACK
    ):
        self.tile_char = char
        self.tile_color = color

    def char(self):
        return self.tile_char

    def color(self):
        return self.tile_color


class FlashingTile(Tile):
    def __init__(self, chars: [], colors: [] = [BLACK, WHITE]):
        self.chars = chars
        self.colors = colors

    def char(self):
        from poc.ui.singletons import flash_controller
        return flash_controller.obj_if_flash_num(self.chars)

    def color(self):
        from poc.ui.singletons import flash_controller
        return flash_controller.obj_if_flash_num(self.colors)
