import random
import time

from poc.engine.actions.action import Action, NoOp
from poc.engine.keyboard_input_manager import Key
from poc.entity.behaviour import BehaviourFunction
from poc.entity.entity import Tag


class DoNothingBehaviour(BehaviourFunction):
    def get_action(self, entity_instance) -> (Action, int):
        return NoOp(), 0


class WandererBehaviour(BehaviourFunction):
    def __init__(self, wander_cost: int, random_delay: int = 0):
        self.wander_cost = wander_cost
        self.random_delay = random_delay

    def get_action(self, entity_instance) -> (Action, int):
        dirs = [Key.A_UP, Key.A_DOWN, Key.A_LEFT, Key.A_RIGHT]
        from poc.engine.actions.entity_actions import MoveAction

        return (
            MoveAction(entity_instance, random.choice(dirs)),
            self.wander_cost + random.randint(0, self.random_delay),
        )


class BiasedWandererBehaviour(BehaviourFunction):
    def __init__(self, wander_cost: int, random_delay: int = 0, same_dir_chance: int = 0):
        self.wander_cost = wander_cost
        self.random_delay = random_delay
        self._previous_wanderers = {}
        self._same_dir_chance = same_dir_chance

    def get_action(self, entity_instance) -> (Action, int):
        from poc.engine.actions.entity_actions import MoveAction

        if (
            entity_instance.get_identifier() in self._previous_wanderers.keys()
            and random.randint(1, 100) < self._same_dir_chance
        ):
            return (
                MoveAction(entity_instance, self._previous_wanderers[entity_instance.get_identifier()], 0),
                self.wander_cost + random.randint(0, self.random_delay),
            )
        dirs = [Key.A_UP, Key.A_DOWN, Key.A_LEFT, Key.A_RIGHT]
        rdir = random.choice(dirs)
        self._previous_wanderers[entity_instance.get_identifier()] = rdir
        return MoveAction(entity_instance, rdir), self.wander_cost + random.randint(0, self.random_delay)


class SnarlingBehaviour(BehaviourFunction):
    def __init__(self, snarl_cost: int, delay_between_snarl_attempts: int, random_delay: int = 0):
        self.snarl_cost = snarl_cost
        self.random_delay = random_delay
        self.delay_between_snarl_attempts = delay_between_snarl_attempts

    def get_action(self, entity_instance) -> (Action, int):
        from poc.engine.actions.game_window_actions import DisplayInTextScrollAction
        from poc.engine import stage_manager_inst
        from poc.entity import player_inst
        from poc.engine.actions.action import NoOp

        player_y, player_x = stage_manager_inst.get_stage().get_pos(player_inst)
        entity_y, entity_x = stage_manager_inst.get_stage().get_pos(entity_instance)
        if stage_manager_inst.is_visible(player_y, player_x, entity_y, entity_x):
            player_inst.add_caution(entity_instance)
            return (
                DisplayInTextScrollAction("The %s snarls at you." % entity_instance.get_name()),
                self.snarl_cost + random.randint(0, self.random_delay),
            )
        else:
            player_inst.remove_caution(entity_instance)
            return NoOp(), self.delay_between_snarl_attempts


class HostileBehaviour(BehaviourFunction):
    def __init__(self, entity, move_cost: int = 1, attack_cost: int = 6):
        from poc.utils.log_manager import logger
        self._weapons = [item for item in entity.get_equipment() if item.has_tag(Tag.WEAPON)]
        if len(self._weapons) == 0:
            logger.warn("Entity %s does not have any weapons for an aggressive behaviour." % entity.get_identifier())
        self._entity = entity
        self._target = None
        self._move_cost = move_cost
        self._attack_cost = attack_cost

    def _weapons_to_attack_target(self) -> []:
        from poc.engine import stage_manager_inst
        from poc.entity.world import Stage
        y, x = stage_manager_inst.get_stage().get_pos(self._entity)
        ty, tx = stage_manager_inst.get_stage().get_pos(self._target)
        dist = Stage.get_dist(y, x, ty, tx)
        aw = []
        for w in self._weapons:
            if not w.is_ranged() and dist == 1:
                aw.append(w)
            elif w.is_ranged():
                aw.append(w)
        return aw

    def get_action(self, _) -> (Action, int):
        from poc.engine import stage_manager_inst
        from poc.entity import player_inst
        from poc.engine.actions.entity_actions import AttackAction
        from poc.engine.actions.entity_actions import MoveAction
        from poc.utils.log_manager import logger

        if self._entity.get_terminated():
            return

        start = time.time()
        try:
            # Acquire target
            if not self._target:
                get_all_entities = time.time()
                # TODO This is super slow with a large number of entities... for now, let's just target the player.
                # race_tag = Tag.get_race_tag_from(self._entity.get_tags())
                # for e in stage_manager_inst.get_stage().get_entities_matching_tag(Tag.ATTACKABLE):
                #     if not race_tag or race_tag not in e.get_tags() and stage_manager_inst.is_entity_visible_from_entity(self._entity, e):
                #         self._target = e
                #         if self._target == player_inst:
                #             player_inst.danger_entities.append(self._entity)
                #         break
                if stage_manager_inst.is_entity_visible_from_entity(self._entity, player_inst):
                    self._target = player_inst
                    if self._target == player_inst:
                        player_inst.danger_entities.append(self._entity)
                logger.time("HostileBehaviour target acquisition for %s." % self._entity.get_identifier(), time.time() - get_all_entities, 0.01)

            # If the target is no longer visible, abandon pursuit
            if self._target and not stage_manager_inst.is_entity_visible_from_entity(self._entity, self._target):
                if self._target == player_inst:
                    player_inst.danger_entities.remove(self._entity)
                self._target = None

            # Attack target if possible
            if self._target and len(self._weapons_to_attack_target()) > 0:
                possible_weapons = self._weapons_to_attack_target()
                ty, tx = stage_manager_inst.get_stage().get_pos(self._target)
                # TODO Ammo weapons + inventory
                return AttackAction(self._entity, None, random.choice(possible_weapons), ty, tx), self._attack_cost

            # Move toward target if needed
            if self._target:
                from poc.entity.world import Stage
                y, x = stage_manager_inst.get_stage().get_pos(self._entity)
                ty, tx = stage_manager_inst.get_stage().get_pos(self._target)
                dist = Stage.get_dist(y, x, ty, tx)
                directions = []
                if Stage.get_dist(y - 1, x, ty, tx) < dist and y - 1 >= 0 and not stage_manager_inst.get_stage().entity_at(y - 1, x):
                    directions.append(Key.A_UP)
                if Stage.get_dist(y + 1, x, ty, tx) < dist and y + 1 < stage_manager_inst.get_stage().height and not stage_manager_inst.get_stage().entity_at(y + 1, x):
                    directions.append(Key.A_DOWN)
                if Stage.get_dist(y, x - 1, ty, tx) < dist and x - 1 >= 0 and not stage_manager_inst.get_stage().entity_at(y, x - 1):
                    directions.append(Key.A_LEFT)
                if Stage.get_dist(y, x + 1, ty, tx) < dist and x + 1 < stage_manager_inst.get_stage().width and not stage_manager_inst.get_stage().entity_at(y, x + 1):
                    directions.append(Key.A_RIGHT)
                if len(directions) != 0:
                    return MoveAction(self._entity, random.choice(directions)), self._move_cost

            return None, 0
        finally:
            logger.time("HostileBehaviour calculation time for %s." % self._entity.get_identifier(), time.time() - start, 0.01)
