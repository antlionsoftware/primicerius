from _curses import COLOR_BLACK
from enum import Enum

from poc.engine.event_listener import EventListener, Event
from poc.entity.entity import Entity, EntityInstance, StatBlock, get_stat_modifier, Tag
from poc.entity.item.inventory import Inventory
from poc.entity.item.item import Item
from poc.entity.item.weapon import Weapon
from poc.entity.status import (
    EntityInstanceStatus,
    StatusValue)
from poc.entity.tile import Tile
from poc.entity.tile_lib import PLAYER_TILE_CHAR

KEY_HUNGER = "hunger"
KEY_THIRST = "thirst"
KEY_ENERGY = "energy"


class SafetyRating(Enum):
    SAFE = "Safe"
    CAUTION = "Caution"
    DANGER = "DANGER"


class Mode(Enum):
    LOOK = "LOK"
    USE = "USE"
    MOVE = "MOV"
    ATTACK = "ATK"


class PlayerInfo:
    def __init__(self, name: str, gender: str, age: int):
        self.name = name
        import uuid
        self.primicerius_id = str(uuid.uuid4())
        self.gender = gender
        self.age = age


class Player(Entity):
    def __init__(self, player_info: PlayerInfo, player_nation):
        starting_stat_block = StatBlock(12, 12, 12, 12, 12, 12, 20, 10, None, 1)
        super().__init__(
            "PLAYER_ID",
            "Player",
            "You",
            Tile(PLAYER_TILE_CHAR, color=COLOR_BLACK),
            stat_block=starting_stat_block,
            tags=[Tag.ATTACKABLE, Tag.TRANSPARENT],
            on_death=[]
        )
        from poc.engine.actions.player_status_actions import RefreshPlayerInventoryAction

        self.player_info = player_info
        self.nation = player_nation
        self.event_listener = _PlayerEventListener(self)
        self.inventory = Inventory(RefreshPlayerInventoryAction())
        self.equipped = []
        self._default_weapon = Weapon("player_fist", "Fist", "Your closed fist.", (1, 2), -2, False, "strength", [])
        self.weapon = None
        self.armour = None
        self.energy_pack = None
        self.key_mode = Mode.MOVE
        self.click_mode = Mode.LOOK
        self.credits = 100

        self.status = EntityInstanceStatus(starting_stat_block.max_health)
        self._time_before_death_thirst = 3 * 24 * 60 * 60
        self._time_before_death_hunger = 5 * 24 * 60 * 60
        self.survival_stats = {
            KEY_HUNGER: StatusValue(self._time_before_death_hunger, self._time_before_death_hunger),
            KEY_THIRST: StatusValue(self._time_before_death_thirst, self._time_before_death_thirst),
        }

        self.show_vis = False
        self.move_cost = 1
        self.attack_cost = 6
        self.autouse = True
        self.autoclosedoor = True

        self.caution_entities = []
        self.danger_entities = []

    def cycle_key_mode(self):
        self.key_mode = {
            Mode.MOVE: Mode.USE,
            Mode.USE: Mode.ATTACK,
            Mode.ATTACK: Mode.MOVE
        }[self.key_mode]

    def cycle_click_mode(self):
        self.click_mode = {
            Mode.LOOK: Mode.ATTACK,
            Mode.ATTACK: Mode.LOOK
        }[self.click_mode]

    def equip(self, item: Item):
        if not item.can_use(self):
            return
        if item in self.inventory:
            if Tag.WEAPON in item.get_tags():
                for equipped in self.equipped:
                    if Tag.WEAPON in equipped.get_tags():
                        self.unequip(equipped)
                        self.weapon = None
                self.weapon = item
            elif Tag.ENERGY_PACK in item.get_tags():
                for equipped in self.equipped:
                    if Tag.ENERGY_PACK in equipped.get_tags():
                        self.unequip(equipped)
                        self.energy_pack = None
                self.energy_pack = item
            elif Tag.ARMOUR in item.get_tags():
                for equipped in self.equipped:
                    if Tag.ARMOUR in equipped.get_tags():
                        self.unequip(equipped)
                        self.armour = None
                self.armour = item
            self.equipped.append(item)

    def get_equipped_weapon(self) -> Weapon:
        return self.weapon if self.weapon else self._default_weapon

    def is_equipped(self, item: Item) -> bool:
        return item in self.equipped

    def unequip(self, item: Item):
        if item in self.inventory:
            if item == self.weapon:
                self.weapon = None
            elif item == self.energy_pack:
                self.energy_pack = None
            elif item == self.armour:
                self.armour = None
            self.equipped.remove(item)

    def get_ac(self) -> int:
        return 10 + get_stat_modifier(self.get_stat_block().dexterity) + (self.armour.get_ac() if self.armour else 0)

    def notify(self, event: Event):
        from poc.engine.event_manager import GAME_TICK

        if event.key == GAME_TICK:
            ticks = event.event
            self.survival_stats[KEY_THIRST].curr = self.survival_stats[KEY_THIRST].curr - ticks
            self.survival_stats[KEY_HUNGER].curr = self.survival_stats[KEY_HUNGER].curr - ticks
            if self.energy_pack:
                self.energy_pack.recharge(ticks)
            self._death_check()

    def check_death(self):
        return self.status.current_health <= 0

    def _death_check(self):
        from poc.engine import menu_manager_inst

        if self.survival_stats[KEY_THIRST].curr <= 0:
            menu_manager_inst.show_death_screen("You have died of thirst.")
        elif self.survival_stats[KEY_HUNGER].curr <= 0:
            menu_manager_inst.show_death_screen("You have died of hunger.")
        elif self.status.current_health <= 0:
            menu_manager_inst.show_death_screen("You have died.")

    def add_caution(self, entity: EntityInstance):
        if entity not in self.caution_entities:
            self.caution_entities.append(entity)

    def remove_caution(self, entity: EntityInstance):
        if entity in self.caution_entities:
            self.caution_entities.remove(entity)

    def get_safety_rating(self) -> SafetyRating:
        if len(self.danger_entities) > 0:
            return SafetyRating.DANGER
        elif len(self.caution_entities) > 0:
            return SafetyRating.CAUTION
        else:
            return SafetyRating.SAFE


class _PlayerEventListener(EventListener):
    def __init__(self, player: Player):
        super().__init__("PlayerEventListener", player)
