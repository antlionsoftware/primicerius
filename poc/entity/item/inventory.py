from poc.engine.actions.action import Action
from poc.entity.item.item import Item


class Inventory:
    def __init__(self, update_action: Action = None):
        # tuple info is (item_id, item_ref, quantity)
        self._contents = []
        self._update_action = update_action

    def __len__(self):
        return len(self._contents)

    def __contains__(self, item):
        for id, item_inst, qty in self._contents:
            if item_inst == item:
                return True
            if item.get_identifier() == id:
                return True
        return False

    def re_sort(self):
        self._contents.sort(key=lambda tup: tup[1].get_name())

    def get_all(self) -> [(str, Item, int)]:
        contents = []
        for identifier, item, qty in self._contents:
            contents.append((identifier, item, qty))
        return contents

    def get_item_at_index(self, i: int) -> Item:
        return self._contents[i][1]

    def get_item_quantity_at_index(self, i: int) -> int:
        return self._contents[i][2]

    def get_item_quantity(self, item: Item) -> int:
        for tup in self._contents:
            if tup[1] == item:
                return tup[2]
        return 0

    def add_item(self, item: Item, qty: int = 1) -> bool:
        assert qty > 0
        for item_tuple in self._contents:
            if item_tuple[0] == item.get_identifier():
                prev_identifier = item_tuple[0]
                prev_item_ref = item_tuple[1]
                prev_qty = item_tuple[2]
                self._contents.remove(item_tuple)
                new_tuple = (prev_identifier, prev_item_ref, prev_qty + qty)
                self._contents.append(new_tuple)
                if self._update_action is not None:
                    self._update_action.act()
                self.re_sort()
                return True
        self._contents.append((item.get_identifier(), item, qty))
        self.re_sort()
        if self._update_action is not None:
            self._update_action.act()
        return True

    def remove_item(self, item_id: str, qty: int = 1) -> bool:
        assert qty > 0
        remove = None
        result = False
        for item_tuple in self._contents:
            if item_tuple[0] == item_id:
                if item_tuple[2] < qty:
                    # not enough items
                    result = False
                else:
                    prev_identifier = item_tuple[0]
                    prev_item_ref = item_tuple[1]
                    prev_qty = item_tuple[2]
                    self._contents.remove(item_tuple)
                    new_tuple = (prev_identifier, prev_item_ref, prev_qty - qty)
                    self._contents.append(new_tuple)
                    if new_tuple[2] <= 0:
                        remove = new_tuple
                    result = True
        if remove:
            self.re_sort()
            self._contents.remove(remove)
        if result and self._update_action is not None:
            self._update_action.act()
        return result
