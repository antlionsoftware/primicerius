from poc.entity.entity import Tag, Entity
from poc.entity.item.item import Item
from poc.entity.item.weapon import Weapon


class Armour(Item):
    def __init__(
        self,
        armour_id: str,
        name: str,
        description: str,
        ac_bonus: int = 0,
        # TODO These are not supported yet
        dam_reduce_ballistic: int = 0,
        dam_reduce_blade: int = 0,
        dam_reduce_energy: int = 0,
        tags: [] = [Tag.ARMOUR],
        min_req: (int, int, int, int, int, int) = (0, 0, 0, 0, 0, 0)
    ):
        super().__init__(armour_id, name, description, None, tags)
        self._min_req = min_req
        self._ac_bonus = ac_bonus
        self._dam_reduction = {
            Tag.BALLISTIC: dam_reduce_ballistic,
            Tag.BLADE: dam_reduce_blade,
            Tag.ENERGY: dam_reduce_energy
        }

    def get_ac(self) -> int:
        return self._ac_bonus

    def calculate_damage_reduction(self, weapon: Weapon) -> int:
        total = 0
        for tag in weapon.get_tags():
            if tag in self._dam_reduction.keys():
                total = total + self._dam_reduction[tag]
        return total

    def can_use(self, user: Entity):
        stat_block = user.get_stat_block()
        if not stat_block:
            return False
        return (
            stat_block.strength >= self._min_req[0]
            and stat_block.dexterity >= self._min_req[1]
            and stat_block.constitution >= self._min_req[2]
            and stat_block.intelligence >= self._min_req[3]
            and stat_block.wisdom >= self._min_req[4]
            and stat_block.charisma >= self._min_req[5]
        )

    def is_equippable(self):
        return True
