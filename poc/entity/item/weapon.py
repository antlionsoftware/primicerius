import random

from poc.entity.entity import Entity, get_stat_modifier, Tag
from poc.entity.item.inventory import Inventory
from poc.entity.item.item import Item, EnergyPack


class Weapon(Item):
    def __init__(
        self,
        weapon_id: str,
        name: str,
        description: str,
        dmg: (int, int) = (1, 4),
        bonus: int = 0,
        ranged: bool = False,
        governing_attribute: str = "strength",
        governing_attribute_bonus_damage: bool = True,
        tags: [] = [Tag.WEAPON],
        required_ammo: Item = None,
        required_energy: float = None,
        min_req: (int, int, int, int, int, int) = (0, 0, 0, 0, 0, 0)
    ):
        super().__init__(weapon_id, name, description, None, tags)
        self._min_dmg = dmg[0]
        self._max_dmg = dmg[1]
        self._bonus = bonus
        self._ranged = ranged
        self._governing_attribute = governing_attribute
        self._governing_attribute_bonus_damage = governing_attribute_bonus_damage
        self._required_ammo = required_ammo
        self._min_req = min_req
        self._required_energy = required_energy

    def get_spec(self):
        if self._bonus != 0:
            bonus_str = "(%s%s)" % (("+" if self._bonus > 0 else ""), self._bonus)
        else:
            bonus_str = ""
        if self._required_energy:
            nrg = " " + "{:.1f}".format(self._required_energy) + "nrg"
        else:
            nrg = ""
        return "(%i-%i)%s%s%s" % (self._min_dmg, self._max_dmg, bonus_str, (" RGD" if self._ranged else ""), nrg)

    def is_ranged(self):
        return self._ranged

    def calc_damage(self, user: Entity = None):
        from poc.utils.log_manager import logger
        total = random.randint(self._min_dmg, self._max_dmg)
        logger.debug("Rolled %i for raw damage." % total)
        total = total + self._bonus
        logger.debug("Damage is %i after weapon bonus." % total)
        if user and self._governing_attribute_bonus_damage:
            sb = user.get_stat_block()
            total = total + get_stat_modifier(getattr(sb, self._governing_attribute))
        logger.debug("Damage is %i after attribute bonus." % total)
        return max([total, 0])

    def calc_hit_bonus(self, user: Entity = None):
        total = self._bonus
        if user:
            sb = user.get_stat_block()
            total = total + get_stat_modifier(getattr(sb, self._governing_attribute))
        return max([total, 0])

    def has_ammo(self, user_inventory: Inventory):
        return self._required_ammo in user_inventory if self._required_ammo else True
        return max([total, 0])

    def has_energy(self, energy_pack: EnergyPack):
        return energy_pack.get_current() >= self._required_energy if self._required_energy else True

    def get_required_ammo(self):
        return self._required_ammo

    def get_energy_requirement(self):
        return self._required_energy

    def can_use(self, user: Entity):
        stat_block = user.get_stat_block()
        if not stat_block:
            return False
        return (
            stat_block.strength >= self._min_req[0]
            and stat_block.dexterity >= self._min_req[1]
            and stat_block.constitution >= self._min_req[2]
            and stat_block.intelligence >= self._min_req[3]
            and stat_block.wisdom >= self._min_req[4]
            and stat_block.charisma >= self._min_req[5]
        )

    def is_equippable(self):
        return True
