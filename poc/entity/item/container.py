import random
from copy import deepcopy

from poc.entity.entity import Entity, Tag, EntityInstance
from poc.entity.item.inventory import Inventory
from poc.entity.item.item import Item
from poc.entity.tile import Tile
from poc.entity.tile_lib import ENTITY_CONTAINER
from poc.ui.color import RED


class Container(Entity):
    def __init__(
            self,
            identifier: str,
            name: str,
            description: str = "A container.",
            tile: Tile = Tile(ENTITY_CONTAINER),
            static: bool = False,
            use_cost: int = 0,
            tags: [Tag] = [Tag.CONTAINER, Tag.TRANSPARENT],
            items: Inventory = Inventory()
    ):
        self._items = items
        super().__init__(identifier, name, description, tile, static, None, use_cost, False, None, tags)

    def insert(self, item: Item, qty: int):
        self._items.add_item(item, qty)

    def get_items(self) -> [(Item, int)]:
        response = []
        for i in range(0, len(self._items)):
            response.append((self._items.get_item_at_index(i), self._items.get_item_quantity_at_index(i)))
        return response

    def remove(self, item: Item, qty: int) -> bool:
        return self._items.remove_item(item.get_identifier(), qty)

    def on_use(self) -> (bool, int, str):
        from poc.engine.menu import ContainerMenu
        from poc.engine import keyboard_input_manager_inst
        from poc.engine.actions.menu_actions import OpenPopUpMenuAction
        from poc.engine.actions.game_window_actions import SetGameKillEnabledAction
        from poc.utils.log_manager import logger
        logger.debug("Opening container %s" % self.get_identifier())
        menu = ContainerMenu(self.get_identifier() + "::Menu", self._items, self.get_name())
        OpenPopUpMenuAction(menu, 5, 15).act()
        keyboard_input_manager_inst.promote(menu.menu_event_listener)
        SetGameKillEnabledAction(False).act()
        return True, 0, None

    def get_instance(self):
        import uuid
        return Container(self.get_identifier() + "::" + str(uuid.uuid4()), self.get_name(), self.get_description(), self.get_tile(), self.get_static(), self._use_cost, self.get_tags(), Inventory())

    @staticmethod
    def corpse_from(entity: EntityInstance):
        inventory = Inventory()
        for item, drop_odds in entity.get_on_death():
            if random.randint(1, 100) < drop_odds:
                inventory.add_item(item)
        return Container(
            entity.get_identifier() + "::corpse",
            "Corpse of " + entity.get_name(),
            "The corpse of a " + entity.get_name(),
            Tile("†", color=RED),
            False,
            0,
            [Tag.CONTAINER, Tag.CORPSE, Tag.TRANSPARENT],
            inventory
        )
