from copy import deepcopy

from poc.engine.useables.useable import Useable
from poc.entity.entity import Entity, Tag


class Item(Entity):
    def __init__(self, item_id: str, name: str, description: str = "An item.", use: Useable = None, tags: [Tag] = None, base_price: int = 0):
        super().__init__(item_id, name, description, tags=tags)
        self.item_id = item_id
        self.use = use
        self.base_price = base_price

    def __str__(self):
        return self.item_id

    def get_spec(self):
        return ""

    def use(self, target: Entity):
        if self.use:
            self.use.use(target)
            from poc.engine import event_manager_inst
            from poc.engine.event_manager import ITEM_USED

            event_manager_inst.dispatch_event(ITEM_USED, (self, target))

    def is_equippable(self):
        return Tag.WEAPON in self.get_tags() or Tag.ENERGY_PACK in self.get_tags()

    def get_instance(self) -> Entity:
        return deepcopy(self)

    def can_use(self, user: Entity):
        return True


class EnergyPack(Item):
    def __init__(self, item_id: str, name: str, max_energy: float, energy_regen: float, regen_delay: int = 5, description: str = "An energy pack.", use: Useable = None, tags: [Tag] = [Tag.ENERGY_PACK], base_price: int = 0):
        super().__init__(item_id, name, description, None, tags, base_price)
        self.max_energy = max_energy
        self.current_energy = max_energy
        self.energy_regen = energy_regen
        self.regen_delay = regen_delay
        self._remaining_delay = 0

    def get_spec(self):
        m = "{:.1f}".format(self.max_energy)
        regen = "{:.1f}".format(self.energy_regen)
        return "(max %s, regen %s)" % (m, regen)

    def get_current(self) -> float:
        return self.current_energy

    def drain(self, value: float):
        self.current_energy = self.current_energy - value
        if self.current_energy < 0:
            self.current_energy = 0
        self._remaining_delay = self.regen_delay

    def recharge(self, ticks: int):
        if self._remaining_delay != 0:
            self._remaining_delay = self._remaining_delay - 1
        else:
            self.current_energy = self.current_energy + (ticks * self.energy_regen)
            if self.current_energy > self.max_energy:
                self.current_energy = self.max_energy
