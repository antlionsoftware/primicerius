import copy
import math
from enum import Enum
from uuid import uuid4

from poc.engine.actions.action import Action
from poc.engine.event_listener import Event
from poc.entity.status import EntityInstanceStatus
from poc.entity.tick_timer import TickTimer
from poc.entity.tile import Tile


def get_stat_modifier(ability: int):
    return math.floor((ability - 10) / 2)


class Tag(Enum):
    AMMO = "AMMO"
    ARMOUR = "ARMOUR"
    ATTACKABLE = "ATTACKABLE"  # This entity can be attacked.
    BACKGROUND = "BACKGROUND"
    BALLISTIC = "BALLISTIC"
    BED = "BED"
    BEHAVIOUR = "BEHAVIOUR"  # The entity can have behaviour.
    BEHAVIOUR_FRIENDLY = "BEHAVIOUR: FRIENDLY"
    BEHAVIOUR_HOSTILE = "BEHAVIOUR: HOSTILE"
    BEHAVIOUR_PASSIVE = "BEHAVIOUR: PASSIVE"
    BLADE = "BLADE"
    COMPOSED = "COMPOSED"  # This entity is part of a larger group forming a single "thing".
    CONTAINER = "CONTAINER"
    CORPSE = "CORPSE"
    DOOR = "DOOR"
    ENERGY = "ENERGY"
    ENERGY_PACK = "ENERGY_PACK"
    EXOTIC = "EXOTIC"
    GAS = "GAS"
    GROUND = "GROUND"
    JUNK = "JUNK"
    LIQUID = "LIQUID"
    MATERIAL = "MATERIAL"
    META = "$meta"
    METAL = "METAL"
    ON_DEATH_SCRAP = "ON_DEATH_SCRAP"  # On scrapping the entity, get on_death as material.
    PASSABLE = "PASSABLE"  # Other entities may move as though this entity was not placed.
    PLASMA = "PLASMA"
    RACE_CANINE = "RACE: CANINE"
    RACE_GOBLINOID = "RACE: GOBLINOID"
    RACE_HUMAN = "RACE: HUMAN"
    RAW = "RAW"  # Indicates that a material is raw.
    RANGED = "RANGED"
    SILICATE = "SILICATE"
    STACK = "MATERIAL_STACK"
    TRANSPARENT = "TRANSPARENT"  # This entity does not block view.
    WEAPON = "WEAPON"

    @staticmethod
    def get_race_tag_from(tags: []):
        race_tags = [Tag.RACE_CANINE, Tag.RACE_HUMAN]
        for tag in tags:
            if tag in race_tags:
                return tag
        return None


class StatBlock:
    def __init__(
        self,
        strength: int,
        dexterity: int,
        constitution: int,
        intelligence: int,
        wisdom: int,
        charisma: int,
        max_health: int,
        default_ac: int,
        challenge: int = 0,
        proficiency: int = 0,
    ):
        self.strength = strength
        self.dexterity = dexterity
        self.constitution = constitution
        self.intelligence = intelligence
        self.wisdom = wisdom
        self.charisma = charisma

        self.max_health = max_health
        self.default_ac = default_ac

        self.proficiency = proficiency
        self.challenge = challenge

    def get_perception(self):
        return get_stat_modifier(self.wisdom) + self.proficiency


class Entity:
    """ An individual entity in the program.

    Attributes
    ----------
    identifier : str
        The unique identifier of this entity. Specifically, the unique identifier is used not as
    """

    def __init__(
            self,
            identifier: str,
            name: str,
            description: str = "No description because dev got lazy...",
            tile: Tile = None,
            static: bool = False,
            on_use_action: Action = None,
            use_cost: int = 0,
            makes_noise: bool = False,
            stat_block: StatBlock = None,
            tags: [Tag] = [],
            equipment: [] = [],
            on_death: [(int, int)] = [],
            default_behaviour = None,
    ):
        self.identifier = identifier
        self._name = name
        self._description = description
        self._tile = tile
        self._static = static
        self._action = on_use_action
        self._use_cost = use_cost
        self._makes_noise = makes_noise
        self._stat_block = stat_block
        self._tags = tags
        self._equipment = equipment
        self._on_death = on_death
        self._terminated = False
        self._default_behaviour = default_behaviour

    def get_identifier(self):
        return self.identifier

    def get_name(self):
        return self._name

    def get_tags(self):
        return self._tags

    def get_description(self):
        return self._description

    def get_tile(self):
        return self._tile

    def get_static(self):
        return self._static

    def get_makes_noise(self):
        return self._makes_noise

    def get_stat_block(self):
        return self._stat_block

    def get_ac(self):
        return self._stat_block.default_ac if self._stat_block else None

    def set_on_use_action(self, on_use_action: Action, use_cost: int = 0):
        self._action = on_use_action
        self._use_cost = use_cost

    def on_use(self) -> (bool, int, str):
        if self._action is not None:
            self._action.act()
            return True, self._use_cost, "You used the %s." % self.get_name()
        else:
            return False, None, None

    def get_instance(self):
        return EntityInstance(self)

    def get_on_death(self):
        return self._on_death

    def get_terminated(self):
        return self._terminated

    def get_equipment(self):
        return self._equipment

    def terminate(self):
        self._terminated = True

    def has_tag(self, tag: Tag):
        return tag in self._tags

    def register_behaviour_to_instance(self, instance):
        if self._default_behaviour:
            instance.register_behaviour(self._default_behaviour)


class EntityInstance(Entity):
    def __init__(self, entity_blueprint: Entity):
        super().__init__(
            identifier=entity_blueprint.identifier,
            name=entity_blueprint._name,
            description=entity_blueprint._description,
            tile=entity_blueprint._tile,
            static=entity_blueprint._static,
            on_use_action=entity_blueprint._action,
            use_cost=entity_blueprint._use_cost,
            makes_noise=entity_blueprint._makes_noise,
            stat_block=entity_blueprint._stat_block,
            tags=entity_blueprint._tags,
            equipment=entity_blueprint._equipment,
            on_death=entity_blueprint._on_death
        )
        self.uid = uuid4()
        self._entity_blueprint = entity_blueprint
        if self._entity_blueprint.get_stat_block():
            self.status = EntityInstanceStatus(self._entity_blueprint.get_stat_block().max_health)
        self.event_listener_id = None
        self._behaviour_function = None
        self._tick_timer = None

    def __str__(self):
        return self.get_identifier()

    def get_blueprint_identifier(self):
        return self._entity_blueprint.get_identifier()

    def check_death(self) -> bool:
        return self.status.current_health <= 0

    def get_identifier(self):
        return "%s (%s)" % (super().get_identifier(), str(self.uid))

    def get_spec(self):
        from poc.entity.item.weapon import Weapon
        return self._entity_blueprint.get_spec() if isinstance(self._entity_blueprint, Weapon) else None

    def is_equippable(self):
        from poc.entity.item.item import Item
        return isinstance(self._entity_blueprint, Item) and self._entity_blueprint.is_equippable()

    def register_behaviour(self, behaviour_function):
        from poc.entity.behaviour import BehaviourFunction

        assert isinstance(behaviour_function, BehaviourFunction)

        self._behaviour_function = behaviour_function
        self._tick_timer = TickTimer()

        from poc.engine import event_manager_inst

        self.event_listener_id = self.get_identifier()
        event_manager_inst.register_listener(self)

    def notify(self, event: Event):
        from poc.engine.event_manager import GAME_TICK

        if event.key == GAME_TICK:
            if self._tick_timer.is_available():
                action, cost = self._behaviour_function.get_action(self)
                if action:
                    action.act()
                    self._tick_timer.increase_tick_timer(cost)
            else:
                self._tick_timer.on_tick(event.event)

    def get_behaviour_profile(self):
        if self._behaviour_function:
            return self._behaviour_function.get_behaviour_profile()
        else:
            return None

    def terminate(self):
        super().terminate()
        if self.event_listener_id:
            from poc.engine import event_manager_inst
            event_manager_inst.unregister_listener(self.event_listener_id)


class ActiveToggleEntity(Entity):
    def __init__(
        self,
        identifier: str,
        on_toggle_texts: (str, str),
        toggle_costs: (int, int),
        names: (str, str),
        descriptions: (str, str),
        tiles: (Tile, Tile),
        make_noises: (bool, bool),
        tags: ([Tag], [Tag]),
    ):
        super().__init__(identifier, "NameIrrelevant")
        self._identifier = identifier
        self._names = names
        self._descriptions = descriptions
        self._tiles = tiles
        self._make_noises = make_noises
        self._tag_sets = tags
        self._active = 0
        self._on_toggle_texts = on_toggle_texts
        self._toggle_costs = toggle_costs

    def get_name(self):
        return self._names[self._active]

    def get_description(self):
        return self._descriptions[self._active]

    def get_tile(self):
        return self._tiles[self._active]

    def get_static(self):
        return False

    def get_makes_noise(self):
        return self._make_noises[self._active]

    def get_tags(self):
        return self._tag_sets[self._active]

    def has_tag(self, tag: Tag):
        return tag in self._tag_sets[self._active]

    def on_use(self) -> (bool, int, str):
        self._active = 1 if not self._active else 0
        return True, self._toggle_costs[self._active], self._on_toggle_texts[self._active]

    def get_instance(self) -> Entity:
        return copy.deepcopy(self)

