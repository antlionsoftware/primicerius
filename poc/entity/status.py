class StatusValue:
    def __init__(self, curr: int, maximum: int):
        self.curr = curr
        self.maximum = maximum

    def get_percentage(self):
        return self.curr / self.maximum * 100


class EntityInstanceStatus:
    def __init__(self, health: int):
        self.bleeding = False
        self.poisoned = False
        self.fatigued = False
        self.extra_conditions = []

        # For logistical reasons this can be more than the entity's max health
        self.max_health = health
        self.current_health = health

    def restore(self):
        self.bleeding = False
        self.poisoned = False
        self.fatigued = False
        self.extra_conditions = []
        self.current_health = self.max_health
