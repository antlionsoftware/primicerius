class Nation:
    def __init__(self, name: str, influence: int, worlds: [], credits: int):
        self.name = name
        self.influence = influence
        self.worlds = worlds
        self.credits = credits
