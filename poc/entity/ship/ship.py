from enum import Enum

from poc.entity.item.item import Item


class ShipClass(Enum):
    CIVILIAN = 1        # Small, fast, very lightly armoured, this is the only class which anyone is allowed to pilot.
    LIGHT_FGT = 2       # Small, fast, lightly-armoured, and cheap, composes the bulk of a fleet. Unlike other ship
                        # types, this one cannot have a shield generator.
    HEAVY_FGT = 3       # Small, fast, and medium-armoured, carries strong weapons. The cornerstone of most fleets.
    FREIGHTER = 4       # Medium-sized, slow, and heavily-armoured, made to transport medium cargo.
    TRAIN = 5           # Large, slow, and heavily-armoured, made to transport large cargo. First class to be able to
                        # carry other (smaller) ships.
    CORVETTE = 6        # Small and fast, lightly-armoured. First warship class.
    DESTROYER = 7       # Small, fast, and lightly-armoured. Can exceptionally carry torpedoes.
    CRUISER = 8         # Small and heavily-armoured.
    BATTLESHIP = 9      # Medium-size, battleship-level armament and heavily-armoured. First ship class to have
                        # torpedoes.
    DREADNAUGHT = 10    # Largest non-capital ships. Heavily armoured. First ship class to have superlasers.
    CAPITAL = 11        # The largest and most important ships in a nation's fleet. Single one per company.


class PowerGenerator:
    def __init__(self, power: int):
        self.power = power


class Hull:
    def __init__(self, strength: int, ballistic_resistance: int, power_drain: int = 0):
        self.strength = strength
        self.ballistic_resistance = ballistic_resistance
        self.power_drain = power_drain


class Shield:
    def __init__(self, strength: int, energy_resistance: int, power_drain: int = 0):
        self.strength = strength
        self.energy_resistance = energy_resistance
        self.power_drain = power_drain


class ShipWeapon:
    def __init__(self, strength: int, energy_cost: int):
        self.strength = strength
        self.energy_cost = energy_cost


class Ship:
    def __init__(self, model: str, name: str, ship_class: ShipClass, power: PowerGenerator, hull: Hull, ballistic_weapon: ShipWeapon = None, energy_strength: ShipWeapon = None, shield: Shield = None, max_torpedoes: int = 0, construction_materials: [(Item, int)] = []):
        self.model = model
        self.name = self.model if name is None else name
        self.ship_class = ship_class
        self.power = power
        self.hull = hull
        self.ballistic_weapon = ballistic_weapon
        self.energy_strength = energy_strength
        self.shield = shield
        self.max_torpedoes = max_torpedoes
