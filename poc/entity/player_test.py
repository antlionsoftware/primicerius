import unittest

from poc.entity import Player, PlayerInfo, Nation
from poc.entity.player import Mode


def get_test_player() -> Player:
    pi = PlayerInfo("test_player_name", "test_player_gender", 18)
    n = Nation("test_nation_name", 1337, [], 1338)
    player = Player(pi, n)
    player.inventory._update_action = None
    return player


class TestPlayer(unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.player = get_test_player()

    def test_playerKeyModeMove_cycleKeyMode_playerModeUse(self):
        self.player.key_mode = Mode.MOVE

        self.player.cycle_key_mode()

        self.assertEqual(Mode.USE, self.player.key_mode)

    def test_playerKeyModeUse_cycleKeyMode_playerModeAttack(self):
        self.player.key_mode = Mode.USE

        self.player.cycle_key_mode()

        self.assertEqual(Mode.ATTACK, self.player.key_mode)

    def test_playerKeyModeAttack_cycleKeyMode_playerModeMove(self):
        self.player.key_mode = Mode.ATTACK

        self.player.cycle_key_mode()

        self.assertEqual(Mode.MOVE, self.player.key_mode)

    def test_playerClickModeLook_cycleClickMode_playerModeAttack(self):
        self.player.click_mode = Mode.LOOK

        self.player.cycle_click_mode()

        self.assertEqual(Mode.ATTACK, self.player.click_mode)

    def test_playerClickModeAttack_cycleClickMode_playerModeLook(self):
        self.player.click_mode = Mode.ATTACK

        self.player.cycle_click_mode()

        self.assertEqual(Mode.LOOK, self.player.click_mode)

    def test_playerCannotEquipItem_equip_itemNotEquipped(self):
        from poc.entity.entity_lib import mat_fur
        self.player.equip(mat_fur)
        self.assertFalse(self.player.is_equipped(mat_fur))

    def test_itemNotInInventory_equip_itemNotEquipped(self):
        from poc.entity.entity_lib import weapon_dagger_iron
        self.player.equip(weapon_dagger_iron)
        self.assertFalse(self.player.is_equipped(weapon_dagger_iron))

    def test_weaponInInventory_equip_itemIsEquipped(self):
        from poc.entity.entity_lib import weapon_dagger_iron
        self.player.inventory.add_item(weapon_dagger_iron)
        self.player.equip(weapon_dagger_iron)
        self.assertTrue(self.player.is_equipped(weapon_dagger_iron))

    def test_weaponInInventory_equip_itemIsEquippedWeapon(self):
        from poc.entity.entity_lib import weapon_dagger_iron
        self.player.inventory.add_item(weapon_dagger_iron)
        self.player.equip(weapon_dagger_iron)
        self.assertEqual(weapon_dagger_iron, self.player.get_equipped_weapon())

    def test_armourInInventory_equip_itemIsEquipped(self):
        from poc.entity.entity_lib import armour_basic_shirt
        self.player.inventory.add_item(armour_basic_shirt)
        self.player.equip(armour_basic_shirt)
        self.assertTrue(self.player.is_equipped(armour_basic_shirt))

    def test_armourInInventory_equip_acIsAdjusted(self):
        from poc.entity.item.armour import Armour
        base_ac = self.player.get_ac()
        dummy_armour = Armour(armour_id=None, name=None, description=None, ac_bonus=10)
        self.player.inventory.add_item(dummy_armour)
        self.player.equip(dummy_armour)
        self.assertEqual(base_ac + dummy_armour.get_ac(), self.player.get_ac())
