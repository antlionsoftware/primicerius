class TickTimer:
    def __init__(self):
        self.ticks_left = 0

    def on_tick(self, ticks: int = 1):
        self.ticks_left = self.ticks_left - ticks
        if self.ticks_left < 0:
            self.ticks_left = 0

    def is_available(self):
        return self.ticks_left == 0

    def increase_tick_timer(self, value: int):
        assert self.is_available()
        self.ticks_left = value
