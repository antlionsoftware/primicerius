from poc.engine.actions.action import Action
from poc.engine.dialogue.dialogue import Dialogue
from poc.engine.menu import Menu


class OpenPopUpMenuAction(Action):
    def __init__(self, menu_to_open: Menu, y: int = 0, x: int = 0):
        self.menu_to_open = menu_to_open
        self.y = y
        self.x = x

    def act(self):
        from poc.engine import menu_manager_inst

        menu_manager_inst.open_menu(self.menu_to_open, self.y, self.x)


class OpenDialogueAction(Action):
    def __init__(self, dialogue_menu: Dialogue, replace: bool = False):
        self.dialogue_menu = dialogue_menu
        self.replace = replace

    def act(self):
        from poc.engine import menu_manager_inst
        if self.replace:
            menu_manager_inst.close_menu()
        menu_manager_inst.open_menu(self.dialogue_menu.to_menu(), dialogue=True)


class OpenPopUpMenuActionCentered(Action):
    def __init__(self, menu_to_open: Menu):
        self.menu_to_open = menu_to_open

    def act(self):
        from poc.engine import menu_manager_inst
        from poc.utils import get_center_coordinates

        y, x = get_center_coordinates(
            self.menu_to_open.get_expected_draw_height(), self.menu_to_open.get_expected_draw_width()
        )
        menu_manager_inst.open_menu(self.menu_to_open, y, x)


class ClosePromotedPopUpAction(Action):
    def act(self):
        from poc.engine.actions.input_actions import UnpromoteKeyboardListenerAction
        UnpromoteKeyboardListenerAction().act()
        ClosePopUpMenuAction().act()


class OpenNotificationAction(OpenPopUpMenuActionCentered):
    def __init__(self, notif_text: str):
        menu = Menu("Menu::Notif", [("Ok", ClosePopUpMenuAction()),], title=notif_text)
        super().__init__(menu)


class ClosePopUpMenuAction(Action):
    def act(self):
        from poc.engine import menu_manager_inst

        menu_manager_inst.close_menu()


class CloseAllMenusAction(Action):
    def act(self):
        from poc.engine import menu_manager_inst

        while len(menu_manager_inst.menu_stack) != 0:
            menu_manager_inst.close_menu()
