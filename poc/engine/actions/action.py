class Action:
    def act(self):
        pass


class CompositeAction(Action):
    def __init__(self, actions: []):
        self.actions = actions

    def act(self):
        for action in self.actions:
            action.act()


class NoOp(Action):
    def act(self):
        pass


class UserAction:
    # TODO Add a "user" entity to the act method here
    def act(self):
        pass
