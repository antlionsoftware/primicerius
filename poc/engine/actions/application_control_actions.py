from poc.engine.actions.action import Action
from poc.utils.log_manager import logger


class KillApplicationAction(Action):
    def act(self):
        from poc.engine import app_ctrl_inst

        logger.info("Application kill action requested.")
        app_ctrl_inst.running = False
