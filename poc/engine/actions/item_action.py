from poc.engine.actions.action import Action
from poc.entity import Item
from poc.entity.entity import Entity


class UseItemAction(Action):
    def __init__(self, item: Item, target: Entity):
        self.item = item
        self.target = target

    def act(self):
        pass
