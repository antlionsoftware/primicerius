from poc.engine.actions.action import Action


class TextEntered:
    @staticmethod
    def do(command: str):
        from poc.ui.singletons import text_scroll

        text_scroll.new_text("> " + command)
        from poc.engine import window_manager_inst

        window_manager_inst.refresh()
        from poc.engine import command_processor_inst

        command_processor_inst.process_entered_text(command)
        # From hereon out we assume all resulting processing and actions are done in the text_input_processor


class UnpromoteKeyboardListenerAction(Action):
    def act(self):
        from poc.engine import keyboard_input_manager_inst

        keyboard_input_manager_inst.unpromote()
