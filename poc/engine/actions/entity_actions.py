import math

from poc.engine.actions.action import Action
from poc.engine.keyboard_input_manager import Key
from poc.entity import player_inst
from poc.entity.entity import Entity, EntityInstance, Tag
from poc.entity.item.inventory import Inventory
from poc.entity.item.item import EnergyPack
from poc.entity.item.weapon import Weapon


class MoveAction(Action):
    def __init__(self, entity: Entity, direction: Key):
        self.entity = entity
        self.direction = direction

    def act(self):
        from poc.engine import stage_manager_inst
        stage_manager_inst.get_stage().move_entity(self.entity, self.direction)


class UseAction(Action):
    def __init__(self, entity: Entity, direction: Key):
        self.entity = entity
        self.direction = direction

    def act(self):
        from poc.engine import stage_manager_inst

        entity = stage_manager_inst.get_entity_in_direction(self.entity, self.direction)
        if entity:
            success, cost, text = entity.on_use()
            if success:
                from poc.engine import event_manager_inst

                if text:
                    from poc.engine.actions.game_window_actions import DisplayInTextScroll

                    DisplayInTextScroll.do(text)
                event_manager_inst.game_tick(cost)


class AttackAction(Action):
    def __init__(self, attacker: EntityInstance, attacker_inventory: Inventory, weapon: Weapon, y: int, x: int, attacker_energy_pack: EnergyPack = None):
        self.attacker = attacker
        self.attacker_inventory = attacker_inventory
        self.attacker_energy_pack = attacker_energy_pack
        self.weapon = weapon
        self.y = y
        self.x = x

    def act(self):
        from poc.engine import stage_manager_inst
        from poc.engine.actions.game_window_actions import DisplayInTextScroll
        from poc.entity.item.container import Container
        from poc.entity.world import Stage
        if not (0 <= self.y < stage_manager_inst.get_stage().height) or not (0 <= self.x < stage_manager_inst.get_stage().width):
            return
        attacked = stage_manager_inst.get_stage().entity_at(self.y, self.x)
        if attacked and Tag.ATTACKABLE in attacked.get_tags() and attacked.get_stat_block() is not None:

            attacker_y, attacker_x = stage_manager_inst.get_stage().get_pos(self.attacker)
            dist = Stage.get_dist(attacker_y, attacker_x, self.y, self.x)
            if dist > 1:
                if not self.weapon.is_ranged():
                    DisplayInTextScroll.do("%s tried to attack the %s with its %s but was too far." % (self.attacker.get_name(), attacked.get_name(), self.weapon.get_name()))
                    return
                if not stage_manager_inst.is_visible(attacker_y, attacker_x, self.y, self.x):
                    DisplayInTextScroll.do("%s cannot attack a non-visible position." % self.attacker.get_name())
                    return

            if self.weapon.get_required_ammo() and (not self.attacker_inventory or not self.weapon.has_ammo(self.attacker_inventory)):
                DisplayInTextScroll.do("%s tried to attack the %s with its %s but did not have enough ammunition." % (self.attacker.get_name(), attacked.get_name(), self.weapon.get_name()))
                return

            if self.weapon.get_energy_requirement() and (not self.attacker_energy_pack or not self.weapon.has_energy(self.attacker_energy_pack)):
                DisplayInTextScroll.do("%s tried to attack the %s with its %s but did not have enough energy." % (self.attacker.get_name(), attacked.get_name(), self.weapon.get_name()))
                return

            ac = attacked.get_stat_block().default_ac
            if ac:
                import random
                roll = random.randint(1, 20) + self.weapon.calc_hit_bonus(self.attacker)
                if roll >= ac:
                    dmg = self.weapon.calc_damage(self.attacker)
                    attacked.status.current_health = attacked.status.current_health - dmg
                    self._consume_ammo()
                    self._drain_energy()
                    DisplayInTextScroll.do("%s dealt %i damage to the %s with its %s." % (self.attacker.get_name(), dmg, attacked.get_name(), self.weapon.get_name()))
                    if attacked.check_death():
                        DisplayInTextScroll.do("%s killed the %s." % (self.attacker.get_name(), attacked.get_name()))
                        y, x = stage_manager_inst.get_stage().get_pos(attacked)
                        attacked.terminate()
                        stage_manager_inst.get_stage().unset(y, x)
                        stage_manager_inst.get_stage().place_at(Container.corpse_from(attacked), y, x)
                else:
                    DisplayInTextScroll.do("%s missed the %s with its %s." % (self.attacker.get_name(), attacked.get_name(), self.weapon.get_name()))
                    self._consume_ammo()
                    self._drain_energy()
        elif attacked and self.attacker == player_inst:
            DisplayInTextScroll.do("You cannot attack the %s." % attacked.get_name())
        elif self.attacker == player_inst:
            DisplayInTextScroll.do("There is nothing to attack.")

    def _consume_ammo(self):
        if self.weapon.get_required_ammo():
            self.attacker_inventory.remove_item(self.weapon.get_required_ammo().get_identifier())

    def _drain_energy(self):
        if self.weapon.get_energy_requirement():
            self.attacker_energy_pack.drain(self.weapon.get_energy_requirement())


class MoveOrUseAction(Action):
    def __init__(self, entity: Entity, direction: Key):
        self.entity = entity
        self.direction = direction

    def act(self):
        from poc.engine import stage_manager_inst

        moved, blocking = stage_manager_inst.get_stage().move_entity(self.entity, self.direction)
        if not moved:
            if self.entity == player_inst and blocking:
                from poc.engine.actions.game_window_actions import DisplayInTextScroll
                from poc.ui.singletons import text_scroll
                DisplayInTextScroll.do("A %s blocks your path." % blocking.get_name())
                text_scroll.text_scroll_terminal_window.refresh()
            UseAction(self.entity, self.direction).act()


class ToggleEntityActiveAction(Action):
    def __init__(self, target: Entity):
        self.target = target

    def act(self):
        self.target.active = not self.target.active


class ChangeStageAction(Action):
    def __init__(self, target):
        self.target = target

    def act(self):
        from poc.engine import stage_manager_inst
        stage_manager_inst.set_stage(self.target)
