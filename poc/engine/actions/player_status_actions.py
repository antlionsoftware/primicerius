from poc.engine.actions.action import Action


class RefreshPlayerStatusAction(Action):
    def act(self):
        from poc.ui.singletons import status_window

        status_window.refresh()


class RefreshPlayerInventoryAction(Action):
    def act(self):
        from poc.ui.singletons import inventory_window

        inventory_window.refresh()
