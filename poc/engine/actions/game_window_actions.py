import time

from poc.engine.actions.action import Action, CompositeAction
from poc.engine.actions.menu_actions import CloseAllMenusAction
from poc.engine.actions.player_status_actions import RefreshPlayerStatusAction
from poc.engine.event_listener import Event
from poc.utils.log_manager import logger
from settings import player_vis_on_start


class InitializeGameAction(CompositeAction):
    def __init__(self):
        super().__init__([CloseAllMenusAction(), OpenDemoAction(), RefreshPlayerStatusAction()])


class OpenDemoAction(Action):
    def act(self):
        logger.info("Opening main game window...")
        from poc.entity import player_inst
        from poc.engine import window_manager_inst
        from poc.engine import keyboard_input_manager_inst
        from poc.engine import event_manager_inst
        from poc.engine import stage_manager_inst

        window_manager_inst.unregister_all()

        # Setup view port
        from poc.ui.singletons import view_port
        window_manager_inst.register(view_port.view_port_terminal_window)
        event_manager_inst.register_listener(view_port.event_listener)

        # Setup location infobox
        from poc.ui.singletons import loc_infobox
        window_manager_inst.register(loc_infobox.location_infobox_terminal_window)
        event_manager_inst.register_listener(loc_infobox.event_listener)

        # Set starting stage
        # from poc.entity.world import player_ship
        # stage_manager_inst.set_stage(player_ship)
        from poc.entity.world import player_station
        stage_manager_inst.set_stage(player_station)
        stage_manager_inst.get_stage().place_player_at_marker()

        # Setup status
        from poc.ui.singletons import status_window
        window_manager_inst.register(status_window.status_terminal_window)

        # Setup inventory
        from poc.ui.singletons import inventory_window
        keyboard_input_manager_inst.register_listener(inventory_window.inventory_event_listener)
        window_manager_inst.register(inventory_window.inventory_terminal_window)

        # Setup text scroll
        from poc.ui.singletons import text_scroll
        window_manager_inst.register(text_scroll.text_scroll_terminal_window)

        # Setup text enter
        from poc.ui.singletons import text_enter
        window_manager_inst.register(text_enter.text_enter_terminal_window)
        keyboard_input_manager_inst.register_listener(text_enter.event_listener)

        # Add starting items for demo
        from poc.entity.entity_lib import weapon_dagger_iron
        from poc.entity.entity_lib import energy_pack_basic
        from poc.entity.entity_lib import weapon_junk_pistol
        from poc.entity.entity_lib import armour_basic_shirt
        dagger = weapon_dagger_iron.get_instance()
        shirt = armour_basic_shirt.get_instance()
        player_inst.inventory.add_item(dagger, 1)
        player_inst.inventory.add_item(weapon_junk_pistol.get_instance(), 1)
        player_inst.inventory.add_item(energy_pack_basic.get_instance(), 1)
        player_inst.inventory.add_item(shirt, 1)
        player_inst.equip(dagger)
        player_inst.equip(shirt)

        # Update time to random thing
        from poc.engine import environment_manager_inst
        from poc.engine.universal_time import UniversalTime
        environment_manager_inst.time = UniversalTime(133, 12, 33, 54)

        # Register custom listeners
        from poc.engine.custom_listeners import PlayerMovedListener
        from poc.engine.custom_listeners import PlayerClickListener

        event_manager_inst.register_listener(PlayerMovedListener().listener)
        event_manager_inst.register_listener(PlayerClickListener().listener)
        event_manager_inst.register_listener(player_inst.event_listener)

        # Hide visibility by default
        from poc.ui.singletons import view_port

        view_port.center_on_player()
        player_inst.show_vis = player_vis_on_start
        if player_inst.show_vis:
            stage_manager_inst.draw_player_visibility()

        # Finally, make the game active
        from poc.engine import game_manager_inst
        game_manager_inst.active = True

        from poc.ui.singletons import flash_controller
        flash_controller.do_flash()

        RefreshAllViewsAction().act()
        from poc.engine.event_manager import GAME_STARTED_EVENT
        event_manager_inst.notify(Event(GAME_STARTED_EVENT, None))
        logger.info("Main game window is open.")


class DisplayInTextScrollAction(Action):
    def __init__(self, text: str):
        self.text = text

    def act(self):
        DisplayInTextScroll.do(self.text)


class DisplayInTextScroll:
    @staticmethod
    def do(text: str):
        from poc.ui.singletons import text_scroll
        start = time.time()
        text_scroll.new_text(text)
        logger.time("Displaying in text scroll.", time.time() - start, 0.01)

    @staticmethod
    def newline():
        DisplayInTextScroll.do("")


class RefreshAllViewsAction(Action):
    def act(self):
        # Setup view port
        from poc.ui.singletons import view_port

        view_port.refresh()
        # Setup location infobox
        from poc.ui.singletons import loc_infobox

        loc_infobox.refresh()
        # Setup status
        from poc.ui.singletons import status_window

        status_window.refresh()
        # Setup inventory
        from poc.ui.singletons import inventory_window

        inventory_window.refresh()
        from poc.engine import window_manager_inst

        window_manager_inst.refresh_all()


class ClearTextScrollAction(Action):
    def act(self):
        from poc.ui.singletons import text_scroll
        text_scroll.clear()
        RefreshAllViewsAction().act()


class SetGameKillEnabledAction(Action):
    def __init__(self, enabled: bool):
        self.enabled = enabled

    def act(self):
        from poc.engine import game_manager_inst
        game_manager_inst.set_kill_enabled(self.enabled)
