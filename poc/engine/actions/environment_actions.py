from poc.engine.actions.action import Action, CompositeAction
from poc.engine.universal_time import UniversalTime


class IncrementTimeAction(Action):
    def __init__(self, seconds: int = 1):
        self.seconds = seconds

    def act(self):
        from poc.engine import environment_manager_inst

        environment_manager_inst.time.incr(self.seconds)
        from poc.ui.singletons import loc_infobox

        loc_infobox.refresh()


class RestPromptAction(Action):
    def act(self):
        from poc.engine.menu import Menu
        from poc.engine.actions.menu_actions import ClosePromotedPopUpAction
        from poc.engine import environment_manager_inst
        from poc.engine.actions.menu_actions import OpenPopUpMenuActionCentered
        menu = Menu(
            "Menu::main",
            [
                ("6:00", CompositeAction([RestUntilAction(environment_manager_inst.time.next(6)), ClosePromotedPopUpAction()])),
                ("12:00", CompositeAction([RestUntilAction(environment_manager_inst.time.next(12)), ClosePromotedPopUpAction()])),
                ("18:00", CompositeAction([RestUntilAction(environment_manager_inst.time.next(18)), ClosePromotedPopUpAction()])),
                ("00:00", CompositeAction([RestUntilAction(environment_manager_inst.time.next(0)), ClosePromotedPopUpAction()])),
                ("Cancel", ClosePromotedPopUpAction())
            ],
            title="Rest Until:",
        )
        OpenPopUpMenuActionCentered(menu).act()


class RestUntilAction(Action):
    def __init__(self, time: UniversalTime):
        self.time = time

    def act(self):
        from poc.entity import player_inst
        from poc.engine import environment_manager_inst
        from poc.engine.actions.game_window_actions import DisplayInTextScroll
        from poc.engine import event_manager_inst
        from poc.engine.actions.game_window_actions import RefreshAllViewsAction
        player_inst.status.restore()
        diff = environment_manager_inst.time.diff_in_seconds(self.time)
        rest_total = UniversalTime(0, 0, 0, diff)
        DisplayInTextScroll.do("Slept for %s." % rest_total.duration_string())
        environment_manager_inst.time = self.time
        event_manager_inst.game_tick(diff, refresh_between_ticks=False, skip=True)
        RefreshAllViewsAction().act()
