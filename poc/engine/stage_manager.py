import time

from poc.engine.event_listener import Event
from poc.engine.keyboard_input_manager import Key
from poc.entity.entity import Entity, Tag, EntityInstance
from poc.entity.world import Stage
from poc.entity.world.placement_utils import place

STAGE_CHANGED_EVENT = "StageChangedKey"


class StageManager:
    def __init__(self):
        self.current_stage = None

    def draw_player_visibility(self):
        from poc.entity import player_inst
        from poc.ui.singletons import view_port
        from poc.entity.tile_lib import HIDDEN_TILE

        player_y, player_x = self.current_stage.get_pos(player_inst)
        visibility = self.visible_tiles(player_y, player_x, view_port.delta_y, view_port.delta_x,
                                                      view_port.height, view_port.width, True)
        for y in range(0, view_port.height):
            for x in range(0, view_port.width):
                if not visibility[y][x] and player_inst.show_vis:
                    view_port.overlay[y][x] = HIDDEN_TILE
                else:
                    view_port.overlay[y][x] = None
        view_port.refresh()

    def set_stage(self, stage, stage_mask: [] = None, stage_mask_key: {} = None):
        from poc.entity.world import Stage
        from poc.utils.log_manager import logger
        from poc.entity.behaviour_dict import BehaviourEntityDict
        from poc.engine import unregister_from_all
        from poc.entity import player_inst
        from poc.ui.singletons import view_port
        from poc.engine import event_manager_inst
        from poc.engine.actions.game_window_actions import RefreshAllViewsAction
        from poc.entity.tile_lib import HIDDEN_TILE
        assert isinstance(stage, Stage)

        if self.current_stage:
            logger.debug("Unregistering all entities from current stage.")
            for entity in self.current_stage.get_all_entities():
                if isinstance(entity, EntityInstance) and entity.event_listener_id:
                    unregister_from_all(entity.event_listener_id)

        logger.debug("Setting new stage.")
        self.current_stage = stage

        if stage_mask and stage_mask_key:
            logger.debug("Mask and mask key were provided, will fill stage.")
            place(stage, stage_mask, stage_mask_key)

        logger.debug("Placing player into stage.")
        stage.place_player_at_marker()

        logger.debug("Registering behaviours to all relevant entities.")
        for entity in stage.get_entities_matching_tag(Tag.BEHAVIOUR):
            BehaviourEntityDict.register_behaviour_to_entity(entity)

        logger.debug("Notifying all listeners of new stage.")
        event_manager_inst.notify(Event(STAGE_CHANGED_EVENT, stage))

        if player_inst.show_vis:
            logger.debug("Refreshing player visibility.")
            py, px = stage.get_pos(player_inst)
            visibility = self.visible_tiles(py, px, view_port.delta_y, view_port.delta_x, view_port.height, view_port.width, True)
            for y in range(0, view_port.height):
                for x in range(0, view_port.width):
                    if not visibility[y][x] and player_inst.show_vis:
                        view_port.overlay[y][x] = HIDDEN_TILE
                    else:
                        view_port.overlay[y][x] = None
            view_port.refresh()

        RefreshAllViewsAction().act()

    def get_stage(self) -> Stage:
        return self.current_stage

    def get_entity_in_direction(self, entity: Entity, direction: Key) -> Entity:
        y, x = self.current_stage.get_pos(entity)
        if direction == Key.A_UP:
            y = y - 1
        elif direction == Key.A_DOWN:
            y = y + 1
        elif direction == Key.A_LEFT:
            x = x - 1
        elif direction == Key.A_RIGHT:
            x = x + 1
        return self.current_stage.entity_at(y, x)

    def visible_tiles(self, viewer_y: int, viewer_x: int, start_y: int, start_x: int, max_height: int, max_width: int, player: bool = False):
        assert self.current_stage
        assert self.current_stage.height >= max_height + start_y
        assert self.current_stage.width >= max_width + start_x
        from poc.engine import environment_manager_inst
        start_time = time.time()
        try:
            result = [[True for _ in range(0, max_width)] for _ in range(0, max_height)]
            for result_y, stage_y in ((y, y + start_y) for y in range(0, max_height)):
                for result_x, stage_x in ((x, x + start_x) for x in range(0, max_width)):
                    if stage_x == viewer_x and stage_y == viewer_y:
                        continue

                    if player and self.current_stage.is_outside():
                        dist = Stage.get_dist(viewer_y, viewer_x, stage_y, stage_x)
                        if dist > environment_manager_inst.visibility_range():
                            result[result_y][result_x] = False
                            continue

                    vis = list(self._bresenham(stage_x, stage_y, viewer_x, viewer_y, 8 if player else 2))
                    vis.remove((viewer_x, viewer_y))
                    vis.remove((stage_x, stage_y))
                    for tup in vis:
                        # Optimization: If one of the closer tuples was already invisible, the tile is invisible. Note
                        # that after calculation, this isn't much of an optimization
                        if not result[tup[1] - start_y][tup[0] - start_x] and Stage.get_dist(tup[1], tup[0], viewer_y, viewer_x) < Stage.get_dist(result_y, result_x, viewer_y, viewer_x):
                            result[result_y][result_x] = False
                            break
                        else:
                            blocking_entity = self.current_stage.entity_at(tup[1], tup[0])
                            if blocking_entity and blocking_entity.get_tile() and not blocking_entity.has_tag(Tag.TRANSPARENT):
                                result[result_y][result_x] = False
                                break

                    if (
                        result[result_y][result_x]
                        and player
                        and self.current_stage.entity_at(stage_y, stage_x)
                        and self.current_stage.entity_at(stage_y, stage_x).get_static()
                    ):
                        self.current_stage.set_static_seen_by_player(stage_y, stage_x)

            return result
        finally:
            from poc.utils.log_manager import logger
            delta = time.time() - start_time
            logger.time("Visibility calculation%s." % (" for player" if player else ""), delta, 0.2)

    def is_visible(self, start_y: int, start_x: int, target_y: int, target_x: int, player: bool = False):
        from poc.utils.log_manager import logger
        bresenham_time = time.time()
        vis = list(self._bresenham(target_x, target_y, start_x, start_y, 8 if player else 2))
        logger.time("is_visible L.O.S. with bresenham.", time.time() - bresenham_time, 0.1)

        if (start_x, start_y) in vis:
            vis.remove((start_x, start_y))
        if (target_x, target_y) in vis:
            vis.remove((target_x, target_y))
        for tup in vis:
            blocking_entity = self.current_stage.entity_at(tup[1], tup[0])
            if blocking_entity and blocking_entity.get_tile() and not blocking_entity.has_tag(Tag.TRANSPARENT):
                return False
        return True

    def is_entity_visible_from_entity(self, entity, target):
        from poc.entity import player_inst
        ey, ex = self.get_stage().get_pos(entity)
        ty, tx = self.get_stage().get_pos(target)
        return self.is_visible(ey, ex, ty, tx, entity == player_inst)

    @staticmethod
    def _bresenham(x0, y0, x1, y1, sensitivity=2):
        dx = x1 - x0
        dy = y1 - y0

        xsign = 1 if dx > 0 else -1
        ysign = 1 if dy > 0 else -1

        dx = abs(dx)
        dy = abs(dy)

        if dx > dy:
            xx, xy, yx, yy = xsign, 0, 0, ysign
        else:
            dx, dy = dy, dx
            xx, xy, yx, yy = 0, ysign, xsign, 0

        D = sensitivity * dy - dx
        y = 0

        for x in range(dx + 1):
            yield x0 + x * xx + y * yx, y0 + x * xy + y * yy
            if D >= 0:
                y += 1
                D -= sensitivity * dx
            D += sensitivity * dy
