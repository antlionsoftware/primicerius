import curses
import time
from enum import Enum

from poc.engine.event_listener import EventListener, Event

KEYBOARD_EVENT_PRESS = "KEYBOARD_EVENT_PRESS"
KEYBOARD_EVENT_RELEASE = "KEYBOARD_EVENT_RELEASE"


class Key(Enum):
    ENTER = 1
    ESC = 2
    BACKSPACE = 3
    A_UP = 4
    A_DOWN = 5
    A_LEFT = 6
    A_RIGHT = 7
    INV_UP = 8
    INV_DOWN = 9

    def __str__(self):
        return self.name

    @staticmethod
    def arrow_keys():
        return [Key.A_UP, Key.A_DOWN, Key.A_LEFT, Key.A_RIGHT]


class _KeyMap:
    def __init__(self):
        self.key_map = {
            10: Key.ENTER,
            27: Key.ESC,
            91: Key.INV_UP,
            93: Key.INV_DOWN,
            127: Key.BACKSPACE,
            curses.KEY_UP: Key.A_UP,
            curses.KEY_DOWN: Key.A_DOWN,
            curses.KEY_LEFT: Key.A_LEFT,
            curses.KEY_RIGHT: Key.A_RIGHT,
        }

    def get_mapping(self, key: int):
        actual = key
        if key in self.key_map.keys():
            actual = self.key_map[key]
        return actual


class KeyboardInputManager:
    def __init__(self):
        self._first_listener = None
        self._listeners = {}
        self._key_map = _KeyMap()
        self._promoted = None

    def on_press(self, key):
        start = time.time()
        try:
            actual = self._key_map.get_mapping(key)

            event = Event(KEYBOARD_EVENT_PRESS, actual)
            if self._promoted:
                self._promoted.notify(event)
            else:
                if self._first_listener:
                    self._first_listener.notify(event)
                for listener_key in list(self._listeners.keys()):
                    if not event.consumed:
                        self._listeners[listener_key].notify(event)
        finally:
            from poc.utils.log_manager import logger
            logger.time("KeyboardInputManager.on_press %s" % key, time.time() - start, 0.2)

    def promote(self, to_promote: EventListener):
        self._promoted = to_promote

    def unpromote(self, id: str = None):
        if self._promoted and (not id or self._promoted.event_listener_id == id):
            self._promoted = False

    def register_listener(self, new_listener: EventListener, first: bool = False):
        if first and not self._first_listener:
            self._first_listener = new_listener
        else:
            self._listeners[new_listener.event_listener_id] = new_listener

    def unregister_listener(self, listener_id: str):
        if self._first_listener and self._first_listener.event_listener_id == listener_id:
            self._first_listener = None
        elif listener_id in self._listeners.keys():
            del self._listeners[listener_id]
