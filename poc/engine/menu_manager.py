from poc.engine.menu import Menu
from poc.ui.terminal_window import TightMenuWindow, MenuWindow, DialogueWindow, TerminalWindow


class MenuManager:
    def __init__(self):
        self.menu_stack = []

    def open_menu(self, menu: Menu, y_start: int = 0, x_start: int = 0, dialogue: bool = False):
        from poc.engine import keyboard_input_manager_inst
        from poc.engine import window_manager_inst
        from settings import app_window_height
        from settings import app_window_width

        from poc.utils.log_manager import logger
        logger.debug("Opening menu %s." % menu.menu_id)

        # Unregister old menu
        if len(self.menu_stack) != 0:
            self.menu_stack[-1].active = False
            keyboard_input_manager_inst.unregister_listener(self.menu_stack[-1].menu_event_listener.event_listener_id)
            window_manager_inst.refresh_all()
            window_manager_inst.refresh_all()
        self.menu_stack.append(menu)
        menu.active = True
        if dialogue:
            window_manager_inst.register(DialogueWindow(menu, app_window_height - 12, app_window_width - 20, 6, 10))
        else:
            window_manager_inst.register(TightMenuWindow(menu, y_start, x_start))
        window_manager_inst.promote_to_top(menu.menu_id)
        keyboard_input_manager_inst.register_listener(menu.menu_event_listener)
        keyboard_input_manager_inst.promote(menu.menu_event_listener)
        window_manager_inst.refresh_all()
        logger.debug("Menu %s opened." % menu.menu_id)

    def close_menu(self):
        from poc.engine import keyboard_input_manager_inst
        from poc.engine import window_manager_inst

        if len(self.menu_stack) == 0:
            return

        menu = self.menu_stack[-1]
        menu.reset()
        self.menu_stack.pop()
        # Make sure to re-register previous menu
        if len(self.menu_stack) != 0:
            self.menu_stack[-1].active = True
            keyboard_input_manager_inst.register_listener(self.menu_stack[-1].menu_event_listener)
        keyboard_input_manager_inst.unpromote(menu.menu_id)
        window_manager_inst.unregister(menu.menu_id)
        keyboard_input_manager_inst.unregister_listener(menu.menu_event_listener.event_listener_id)
        window_manager_inst.refresh_all()

    @staticmethod
    def show_quit_screen():
        from poc.engine.menu import Menu
        from poc.engine.actions.application_control_actions import KillApplicationAction
        from poc.engine.actions.menu_actions import ClosePopUpMenuAction
        from poc.engine.actions.menu_actions import OpenPopUpMenuActionCentered
        from poc.engine.actions.action import CompositeAction
        from poc.engine.actions.input_actions import UnpromoteKeyboardListenerAction
        from poc.engine import keyboard_input_manager_inst

        kill_menu = Menu(
            "Quit",
            [
                ("Yes", KillApplicationAction()),
                ("No", CompositeAction([UnpromoteKeyboardListenerAction(), ClosePopUpMenuAction()])),
            ],
            "Are You Sure You Wish To Quit?",
        )
        OpenPopUpMenuActionCentered(kill_menu).act()
        keyboard_input_manager_inst.promote(kill_menu.menu_event_listener)

    @staticmethod
    def show_death_screen(cod: str):
        from poc.engine.menu import Menu
        from poc.engine.actions.application_control_actions import KillApplicationAction
        from poc.engine.actions.menu_actions import OpenPopUpMenuActionCentered
        from poc.engine import keyboard_input_manager_inst
        from poc.engine import event_manager_inst

        event_manager_inst._hard_interrupt = True
        kill_menu = Menu("Death", [("Quit", KillApplicationAction())], cod)
        OpenPopUpMenuActionCentered(kill_menu).act()
        keyboard_input_manager_inst.promote(kill_menu.menu_event_listener)
