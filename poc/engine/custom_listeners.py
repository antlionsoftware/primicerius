from poc.engine.event_listener import EventListener, Event
from poc.engine.event_manager import PLAYER_USED_ENTITY_EVENT, PLAYER_CLICK_EVENT, PLAYER_MOVED_EVENT


class PlayerMovedListener:
    def __init__(self):
        self.listener = EventListener("PlayerMovedListener", self)

    def notify(self, event: Event):
        if event.key == PLAYER_MOVED_EVENT or event.key == PLAYER_USED_ENTITY_EVENT:
            from poc.engine import stage_manager_inst
            stage_manager_inst.draw_player_visibility()


class PlayerClickListener:
    def __init__(self):
        self.listener = EventListener("PlayerClickListener", self)

    def notify(self, event: Event):
        from poc.entity import player_inst
        from poc.entity.player import Mode
        from poc.engine.actions.game_window_actions import DisplayInTextScroll

        if event.key == PLAYER_CLICK_EVENT and player_inst.click_mode == Mode.LOOK:
            from poc.engine import stage_manager_inst
            from poc.utils.log_manager import logger

            player_y, player_x = stage_manager_inst.get_stage().get_pos(player_inst)
            y, x = event.event[0], event.event[1]
            logger.debug("Player is (%i,%i) and click is (%i,%i)." % (player_y, player_x, y, x))
            if player_y == y and player_x == x:
                under = stage_manager_inst.current_stage.entity_under(y, x)
                bg_entity = stage_manager_inst.current_stage.bg_entity_at(y, x)
                text = "This is you. %s%s" % (
                    (("You are on top of a %s. " % under.get_name()) if under else ""),
                    ("The ground underneath you is %s." % bg_entity.get_name()),
                )
                DisplayInTextScroll.do(text)
            elif (
                not 0 <= y < stage_manager_inst.get_stage().height or not 0 <= x < stage_manager_inst.get_stage().width
            ):
                return
            elif stage_manager_inst.is_visible(player_y, player_x, y, x, True):
                entity = stage_manager_inst.current_stage.entity_at(y, x)
                under = stage_manager_inst.current_stage.entity_under(y, x)
                bg_entity = stage_manager_inst.current_stage.bg_entity_at(y, x)
                text = "%s%s%s%s%s" % (
                    (("You see a %s. " % entity.get_name()) if entity else ""),
                    (("(%s) " % entity.get_description()) if entity else ""),
                    (("It is on top of a %s. " % under.get_name()) if entity and under else ""),
                    ("The ground is %s." % bg_entity.get_name()),
                    ((" (%s)" % bg_entity.get_description()) if bg_entity else ""),
                )
                DisplayInTextScroll.do(text)
            else:
                DisplayInTextScroll.do("You cannot see the clicked position.")
