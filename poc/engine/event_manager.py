import time

from poc.engine.actions.entity_actions import MoveOrUseAction, UseAction, MoveAction, AttackAction
from poc.engine.event_listener import EventListener, Event
from poc.engine.keyboard_input_manager import KEYBOARD_EVENT_PRESS, Key

# Generic event keys
from poc.entity.player import Mode

ITEM_USED = "ItemUsed"
GAME_TICK = "GameTick"
GAME_STARTED_EVENT = "GameStartedEvent"
PLAYER_CLICK_EVENT = "PlayerClickEvent"
PLAYER_MOVED_EVENT = "PlayerMovedEvent"
PLAYER_USED_ENTITY_EVENT = "PlayerUsedEntityEvent"
PLAYER_WAIT_EVENT = "PlayerWaitEvent"
ENTITY_ATTACK_EVENT = "EntityAttackEvent"


class EventManager:
    def __init__(self):
        self.listeners = {}
        self.promoted = None
        self.event_listener = EventListener("EventManager", self)
        # Optimization for the listeners that just want to know a game tick happened
        self._maps_to_game_tick = [ITEM_USED, PLAYER_MOVED_EVENT]
        self._seconds_per_tick = 1
        self._hard_interrupt = False

    def notify(self, event: Event):
        from poc.entity import player_inst
        from poc.engine import game_manager_inst
        from poc.engine import stage_manager_inst
        from poc.utils.log_manager import logger

        logger.debug("Event: %s" % (event))
        start = time.time()
        try:
            if game_manager_inst.active:
                if self._is_move_action(event, player_inst.key_mode):
                    event.consumed = True
                    if player_inst.autouse:
                        MoveOrUseAction(player_inst, event.event).act()
                    else:
                        MoveAction(player_inst, event.event).act()
                    self.notify(Event(PLAYER_MOVED_EVENT, None))
                    return
                if self._is_use_action(event, player_inst.key_mode):
                    event.consumed = True
                    UseAction(player_inst, event.event).act()
                    entity = stage_manager_inst.get_entity_in_direction(player_inst, event.event)
                    self.notify(Event(PLAYER_USED_ENTITY_EVENT, entity))
                    return
                if self._is_key_attack_action(event, player_inst.key_mode) or self._is_click_attack_action(event, player_inst.click_mode):
                    event.consumed = True
                    y, x = stage_manager_inst.get_stage().get_pos(player_inst)
                    if event.key == KEYBOARD_EVENT_PRESS:
                        if event.event == Key.A_UP:
                            y = y - 1
                        elif event.event == Key.A_DOWN:
                            y = y + 1
                        elif event.event == Key.A_LEFT:
                            x = x - 1
                        elif event.event == Key.A_RIGHT:
                            x = x + 1
                    elif event.key == PLAYER_CLICK_EVENT:
                        y = event.event[0]
                        x = event.event[1]
                    logger.debug("Player will attack (%i,%i)" % (y, x))
                    AttackAction(player_inst, player_inst.inventory, player_inst.get_equipped_weapon(), y, x, player_inst.energy_pack).act()
                    self.game_tick(player_inst.attack_cost, True)
                    return
                if event.key == KEYBOARD_EVENT_PRESS and not isinstance(event.event, Key) and chr(event.event) == ".":
                    event.consumed = True
                    self.game_tick()
                    return
                if event.key == KEYBOARD_EVENT_PRESS and not isinstance(event.event, Key) and chr(event.event) == "-":
                    event.consumed = True
                    player_inst.cycle_key_mode()
                    return
                if event.key == KEYBOARD_EVENT_PRESS and not isinstance(event.event, Key) and chr(event.event) == "=":
                    event.consumed = True
                    player_inst.cycle_click_mode()
                    return
                if event.key == PLAYER_WAIT_EVENT:
                    seconds = event.event
                    self.game_tick(seconds, True)
                    return

            if not event.consumed:
                also_send_game_state_changed = False
                if event.key in self._maps_to_game_tick:
                    also_send_game_state_changed = True


                notify_start = time.time()
                try:
                    if self.promoted:
                        self.promoted.notify(event)
                    else:
                        for listener_key in list(self.listeners.keys()):
                            self.listeners[listener_key].notify(event)
                finally:
                    logger.time("event_manager listener notify and tick loop for event %s." % event, time.time() - notify_start, 0.1)

                tick_start = time.time()
                try:
                    if also_send_game_state_changed:
                        self.game_tick()
                finally:
                    logger.time("event_manager game tick for event %s" % event, time.time() - tick_start, 0.1)
        finally:
            logger.time("event_manager full notify for event %s." % event, time.time() - start, 0.2)

    def _is_move_action(self, event: Event, player_mode: Mode):
        return event.key == KEYBOARD_EVENT_PRESS and event.event in Key.arrow_keys() and player_mode == Mode.MOVE

    def _is_key_attack_action(self, event: Event, player_mode: Mode):
        return event.key == KEYBOARD_EVENT_PRESS and event.event in Key.arrow_keys() and player_mode == Mode.ATTACK

    def _is_click_attack_action(self, event: Event, player_mode: Mode):
        return event.key == PLAYER_CLICK_EVENT and player_mode == Mode.ATTACK

    def _is_use_action(self, event: Event, player_mode: Mode):
        return event.key == KEYBOARD_EVENT_PRESS and event.event in Key.arrow_keys() and player_mode == Mode.USE

    def game_tick(self, ticks: int = 1, refresh_between_ticks: bool = False, skip: bool = False):
        if ticks == 0:
            return
        from poc.engine.actions.environment_actions import IncrementTimeAction
        from poc.engine.actions.game_window_actions import RefreshAllViewsAction
        from poc.utils.log_manager import logger

        logger.debug("Ticking for %i ticks." % ticks)

        if not skip:
            for i in range(0, ticks):
                if self._hard_interrupt:
                    return

                if self.promoted:
                    ptick_start = time.time()
                    try:
                        self.promoted.notify(Event(GAME_TICK, None))
                    finally:
                        logger.time("Tick time for promoted listener.", time.time() - ptick_start, 0.05)
                else:
                    ttick_start = time.time()
                    try:
                        for listener_key in list(self.listeners.keys()):
                            stick_start = time.time()
                            try:
                                self.listeners[listener_key].notify(Event(GAME_TICK, 1))
                            finally:
                                logger.time("Sending ticks to listener %s." % listener_key, time.time() - stick_start, 0.05)
                    finally:
                        logger.time("Sending ticks to all listeners.", time.time() - ttick_start, 0.1)

                IncrementTimeAction().act()
                if refresh_between_ticks:
                    RefreshAllViewsAction().act()

                if self._hard_interrupt:
                    return

        else:
            if self.promoted:
                self.promoted.notify(Event(GAME_TICK, ticks))
            else:
                for listener_key in list(self.listeners.keys()):
                    self.listeners[listener_key].notify(Event(GAME_TICK, ticks))

    def promote(self, to_promote: EventListener):
        self.promoted = to_promote

    def unpromote(self):
        self.promoted = None

    def register_listener(self, new_listener: EventListener):
        self.listeners[new_listener.event_listener_id] = new_listener

    def unregister_listener(self, listener_id: EventListener):
        del self.listeners[listener_id]
