from poc.entity.entity import Entity


class Useable:
    def use(self, target: Entity):
        pass


class CompositeUseable(Useable):
    def __init__(self, useables: []):
        self.useables = useables

    def use(self, target: Entity):
        for useable in self.useables:
            useable.use(target)


class SimpleTextUseable(Useable):
    def __init__(self, text: str):
        self.text = text

    def use(self, target: Entity):
        from poc.engine.actions.game_window_actions import DisplayInTextScroll

        DisplayInTextScroll.do(self.text)
