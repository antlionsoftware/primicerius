import random
import uuid

from poc.engine.actions.action import CompositeAction
from poc.engine.menu import Menu


class DialogueNode:
    def __init__(self, text: str, options: [()] = []):
        assert text
        self.text = text
        self.options = options

    def add_option(self, option, next_node, action):
        self.options.append((option, next_node, action))

    def get_dialogue_node(self):
        return self


class RandomDialogueNode(DialogueNode):
    def __init__(self, dialogues: [DialogueNode]):
        self.dialogues = dialogues

    def get_dialogue_node(self):
        return random.choice(self.dialogues)


class Dialogue:
    def __init__(self, name: str, initial_node: DialogueNode, is_spoken: bool = False):
        self.name = name
        self.initial_node = initial_node
        self.is_spoken = is_spoken

    def _build_menu_recursively(self, node: DialogueNode, first: bool) -> Menu:
        from poc.engine.actions.menu_actions import OpenDialogueAction
        items = []  # Option, Action
        for option, next_node, action in node.options:
            next_node_menu = None
            if next_node:
                next_node_menu = self._build_menu_recursively(next_node, False)

            if next_node_menu and not action:
                items.append((option, OpenDialogueAction(next_node_menu, not first)))
            elif action and not next_node_menu:
                items.append((option, action))
            elif action and next_node_menu:
                items.append((option, CompositeAction([action, OpenDialogueAction(next_node_menu)])))
        d_text = node.text if not self.is_spoken else "\"" + node.text + "\""
        return Menu(("Dialogue::%s::%s" % (self.name, uuid.uuid4())), items, self.name, d_text)

    def to_menu(self):
        return self._build_menu_recursively(self.initial_node.get_dialogue_node(), True)

