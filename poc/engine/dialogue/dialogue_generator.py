import random

from poc.engine.dialogue.dialogue import Dialogue, DialogueNode, RandomDialogueNode


class DialogueGenerator:
    def __init__(self):
        self.random_stuff = {
            "random": [
                "Be careful; monsters roam the wilderness!",
                "I wish I was Primicerius of a Company...",
                "...",
                "Do you own a spaceship? I heard they were very expensive.",
                "I feel observed...",
                "I like to wear shorts. Their comfy and easy to wear.",
                "I hope you'll stay a while.",
                "The day changes, but life stays the same.",
                "I hope you're enjoying this demo!"
            ]
        }

    def get_random_node(self, random_key: str = "random") -> DialogueNode:
        dialogues = []
        for r in self.random_stuff[random_key]:
            dialogues.append(DialogueNode(r, [self.get_exit("Okay...")]))
        return RandomDialogueNode(dialogues)

    def get_random(self, speaker_name: str, random_key: str = "random") -> Dialogue:
        return Dialogue(speaker_name, self.get_random_node(random_key), True)

    def single_text_dialogue(self, speaker_name: str, text: str) -> Dialogue:
        return Dialogue(speaker_name, DialogueNode(text, [self.get_exit("Okay")]), True)

    @staticmethod
    def get_exit(option_name: str = "<Exit>"):
        from poc.engine.actions.menu_actions import CloseAllMenusAction
        return option_name, None, CloseAllMenusAction()
