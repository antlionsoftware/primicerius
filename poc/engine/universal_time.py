import math


class UniversalTime:
    def __init__(self, date: int = 0, hour: int = 0, min: int = 0, sec: int = 0):
        assert hour < 24 and min < 60
        if sec >= 60 and date == 0 and hour == 0 and min == 0:
            self.universal_second = round(sec % 60)
            x = math.floor(sec / 60)
            self.universal_minute = round(x % 60)
            x = math.floor(x / 60)
            self.universal_hour = round(x % 24)
            x = math.floor(x / 24)
            self.universal_date = round(x)
        else:
            self.universal_date = date
            self.universal_hour = hour
            self.universal_minute = min
            self.universal_second = sec

    def __add__(self, other):
        m = 0
        h = 0
        d = 0
        self.universal_second = self.universal_second + other.universal_second
        if self.universal_second >= 60:
            self.universal_second = self.universal_second - 60
            m = 1
        self.universal_minute = self.universal_minute + other.universal_minute + m
        if self.universal_minute >= 60:
            self.universal_minute = self.universal_minute - 60
            h = 1
        self.universal_hour = self.universal_hour + other.universal_hour + h
        if self.universal_hour >= 24:
            self.universal_hour = self.universal_hour - 24
            d = 1
        self.universal_date = self.universal_date + other.universal_date + d

    def to_seconds(self):
        return (self.universal_second) + (self.universal_minute * 60) + (self.universal_hour * 60 * 60) + (self.universal_date * 24 * 60 * 60)

    def diff_in_seconds(self, other):
        return other.to_seconds() - self.to_seconds()

    def next(self, hour: int):
        if hour <= self.universal_hour:
            return UniversalTime(self.universal_date + 1, hour, 0, 0)
        else:
            return UniversalTime(self.universal_date, hour, 0, 0)

    def tod(self):
        if self.universal_hour >= 20 or self.universal_hour < 6:
            return "Night"
        if 6 <= self.universal_hour < 7:
            return "Dawn"
        if 7 <= self.universal_hour < 11:
            return "Morning"
        if 11 <= self.universal_hour < 14:
            return "Midday"
        if 14 <= self.universal_hour < 17:
            return "Afternoon"
        if 17 <= self.universal_hour < 19:
            return "Evening"
        if 19 <= self.universal_hour < 20:
            return "Dusk"

    def __str__(self):
        sec = "{:02d}".format(self.universal_second)
        minutes = "{:02d}".format(self.universal_minute)
        hour = "{:02d}".format(self.universal_hour)
        return "Day %i %s:%s:%s" % (self.universal_date, hour, minutes, sec)

    def duration_string(self):
        from poc.utils.log_manager import logger

        logger.debug(self.universal_minute)
        return "%i day%s, %i hour%s, %i minute%s, and %i second%s" % (
            self.universal_date,
            "s" if self.universal_date != 1 else "",
            self.universal_hour,
            "s" if self.universal_hour != 1 else "",
            self.universal_minute,
            "s" if self.universal_minute != 1 else "",
            self.universal_second,
            "s" if self.universal_second != 1 else "",
        )

    def incr(self, seconds: int = 1):
        self.__add__(UniversalTime(0, 0, 0, seconds))
