from enum import Enum

from poc.engine.universal_time import UniversalTime


class TemperatureRange:
    def __init__(self, lower: int, upper: int, text: str):
        self.lower = lower
        self.upper = upper
        self.text = text


class TemperatureRange(Enum):
    FRIGID = TemperatureRange(-273, -15, "!! Frigid !!")
    COLD = TemperatureRange(-14, 9, "Cold")
    CHILLY = TemperatureRange(10, 15, "Chilly")
    WARM = TemperatureRange(16, 24, "Warm")
    HOT = TemperatureRange(25, 35, "Hot")
    TORRID = TemperatureRange(36, 45, "Torrid")
    SCORCHING = TemperatureRange(46, 60, "!! Scorching !!")
    BURNING = TemperatureRange(61, 10000000, "!! Burning !!")

    @staticmethod
    def get_enum_from_temp(temp: int):
        for temp_range in TemperatureRange:
            if temp_range.value.lower <= temp <= temp_range.value.upper:
                return temp_range
        return None


class Weather:
    def __init__(self):
        self.temperature = 21

    def __str__(self):
        return "Clear %i°C (%s)" % (
            self.temperature,
            TemperatureRange.get_enum_from_temp(self.temperature).value.text,
        )


class EnvironmentManager:
    def __init__(self):
        self.weather = Weather()
        self.time = UniversalTime()

    def visibility_range(self):
        base_visibility_max = 60
        # Dawn/Dusk reduce
        if self.time.universal_hour >= 17 or self.time.universal_hour < 7:
            base_visibility_max = 30
        if self.time.universal_hour >= 18 or self.time.universal_hour < 6:
            base_visibility_max = 22
        if self.time.universal_hour >= 19 or self.time.universal_hour < 6:
            base_visibility_max = 14
        # Night reduce
        if self.time.universal_hour >= 20 or self.time.universal_hour < 6:
            base_visibility_max = 10
        return base_visibility_max
