import threading


class GameManager:
    def __init__(self):
        self.active = False
        self._realtime_timer = None
        self._realtime_on = False
        self._kill_enabled = True

    def enable_realtime(self):
        if self.active:
            if not self._realtime_on:
                self._start_realtime_timer()
                self._realtime_on = True
            else:
                self._realtime_timer.cancel()
                self._realtime_on = False

    def set_kill_enabled(self, enabled: bool):
        self._kill_enabled = enabled

    def get_kill_enabled(self):
        return self._kill_enabled

    def disable_realtime(self):
        if self._realtime_on:
            self._realtime_timer.cancel()
            self._realtime_on = False

    def _realtime_tick(self):
        from poc.engine import event_manager_inst

        event_manager_inst.game_tick(1)
        self._start_realtime_timer()

    def _start_realtime_timer(self):
        if self.active:
            # self._realtime_timer = threading.Timer(1.0, self._realtime_tick)
            self._realtime_timer = threading.Timer(0.001, self._realtime_tick)
            self._realtime_timer.start()
