import curses
import time

from poc.ui.terminal_window import TerminalWindow


class WindowManager:
    def __init__(self):
        self.terminal_windows = {}
        self.promoted = []
        self.menu_stack = []

    def register(self, terminal_window: TerminalWindow) -> bool:
        if (
            terminal_window.terminal_id in self.terminal_windows.keys()
            and self.terminal_windows[terminal_window.terminal_id] is not None
        ):
            return False
        self.terminal_windows[terminal_window.terminal_id] = terminal_window
        from poc.utils.log_manager import logger
        logger.debug("Registered window %s." % terminal_window.terminal_id)
        return True

    def unregister(self, terminal_id: str) -> bool:
        if terminal_id not in self.terminal_windows.keys():
            return False
        del self.terminal_windows[terminal_id]
        if terminal_id in self.promoted:
            self.promoted.remove(terminal_id)
        return True

    def unregister_all(self):
        self.terminal_windows.clear()

    def refresh(self):
        start = time.time()
        if len(self.promoted) != 0:
            terminal_window = self.terminal_windows[self.promoted[-1]]
            self._redraw(terminal_window)
        else:
            for terminal_window_name in list(self.terminal_windows.keys()):
                self._redraw(self.terminal_windows[terminal_window_name])
        from poc.utils.log_manager import logger
        logger.time("window_manager.refresh", time.time() - start, 0.02)

    def refresh_all(self):
        for terminal_window_name in list(self.terminal_windows.keys()):
            self._redraw(self.terminal_windows[terminal_window_name])
        if len(self.promoted) != 0:
            terminal_window = self.terminal_windows[self.promoted[-1]]
            self._redraw(terminal_window)

    @staticmethod
    def _redraw(terminal_window: TerminalWindow):
        terminal_window.refresh()

    def promote_to_top(self, terminal_id: str):
        if terminal_id not in self.terminal_windows.keys():
            from poc.utils.log_manager import logger
            logger.warn("Could not promote terminal %s." % terminal_id)
            return False
        if terminal_id in self.promoted:
            self.promoted.remove(terminal_id)
        self.promoted.append(terminal_id)
