from poc.engine.actions.action import NoOp
from poc.engine.actions.game_window_actions import DisplayInTextScroll, ClearTextScrollAction
from poc.engine.event_listener import Event
from poc.entity import player_inst
from poc.entity.entity import EntityInstance
from poc.entity.player import Mode, KEY_HUNGER, KEY_THIRST
from poc.entity.tile_lib import HIDDEN_TILE
from settings import enable_debug_commands


class CommandProcessor:
    def __init__(self):
        self._plain_text_mapping = {
            "hello there": "General Kenobi *lightsaber noises*",
            "lorem": "ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            "wq!": "Not implemented - save and quit",
        }
        self._command_action_mapping = {
            "noop": (NoOp, "A Command which does nothing"),
            # Used for help
            "debug": (NoOp, "Executes the selected debug command."),
            "clear": (ClearTextScrollAction, "Clears the text scroll."),
            # TODO
        }
        from poc.engine import MenuManager
        self._command_dynamic_function_mapping = {
            "quit": (MenuManager.show_quit_screen, "Prompts the user to quit the game."),
            "autouse": (self.show_autouse, "Displays whether the player is in auto-use mode."),
            "autoclosedoor": (self.show_autoclosedoor, "Displays whether the player is in automatic close door mode."),
            "realtime": (self._realtime, "realtime: Turns on/off the realtime passing of the game."),
            "stat": (self._show_player_stats, "stat/stats: Displays player statistics."),
            "stats": (self._show_player_stats, "stat/stats: Displays player statistics."),
            "nation": (self._show_player_nation, "nation: Displays the statistics of the player's Nation."),
        }
        self._action_command_mapping = {
            "lok": ((None, Mode.LOOK), "Changes mode action to LOOK."),
            "use": ((Mode.USE, None), "Changes mode action to USE."),
            "atk": ((Mode.ATTACK, Mode.ATTACK), "Changes mode action to ATTACK."),
            "mov": ((Mode.MOVE, None), "Changes mode action to MOVE."),
            "look": ((None, Mode.LOOK), "Changes mode action to LOOK."),
            "attack": ((Mode.ATTACK, Mode.ATTACK), "Changes mode action to ATTACK."),
            "move": ((Mode.MOVE, None), "Changes mode action to MOVE."),
        }
        self._multi_word_commands = {
            "item": (self._parse_item_use, "item index: Uses the item at the selected index."),  # use items
            "help": (self.display_help, "help command: Displays help for a specific command."),
            "toggle": (
                self._toggle,
                "toggle s: Toggles parameter. Using toggle without specifying s will print the toggle-ables.",
            ),
            "wait": (self._wait, "wait n: Moves the game forward by n seconds."),
            "equip": (self._equip, "equip n: Equips the inventory item at location n, if it is equippable."),
            "unequip": (self._unequip, "unequip n: Unequips the inventory item at location n, if it is equippable."),
            # TODO
        }

    def process_entered_text(self, command: str):
        from poc.utils.log_manager import logger

        logger.debug("Command entered: %s" % command)
        lower_command = command.lower()

        if enable_debug_commands and lower_command.startswith("debug"):
            self._process_debug(lower_command)

        elif lower_command in self._action_command_mapping.keys():
            new_key = self._action_command_mapping[lower_command][0][0]
            new_click = self._action_command_mapping[lower_command][0][1]
            if new_key:
                player_inst.key_mode = new_key
                DisplayInTextScroll.do("Arrow key mode changed to %s" % new_key.name)
            if new_click:
                player_inst.click_mode = new_click
                DisplayInTextScroll.do("Click mode changed to %s" % new_click.name)

        elif lower_command in self._plain_text_mapping.keys():
            DisplayInTextScroll.do(self._plain_text_mapping[lower_command])

        elif lower_command in self._command_action_mapping.keys():
            self._command_action_mapping[lower_command][0]().act()

        elif lower_command in self._command_dynamic_function_mapping.keys():
            self._command_dynamic_function_mapping[lower_command][0](lower_command)

        elif lower_command.startswith(tuple(self._multi_word_commands.keys())):
            self._parse_multi_word_command(lower_command.split(" "))

        else:
            DisplayInTextScroll.do("Unrecognized command")

    def show_autouse(self, command):
        DisplayInTextScroll.do("Auto use on move is %s." % ("enabled" if player_inst.autouse else "disabled"))

    def show_autoclosedoor(self, command):
        DisplayInTextScroll.do(
            "Auto close door on move is %s." % ("enabled" if player_inst.autoclosedoor else "disabled")
        )

    def _realtime(self, command):
        from poc.engine import game_manager_inst

        game_manager_inst.enable_realtime()

    def _show_player_stats(self, command):
        stat_block = player_inst.get_stat_block()
        from poc.entity.entity import get_stat_modifier

        pi = player_inst.player_info
        DisplayInTextScroll.do(pi.name.upper())
        DisplayInTextScroll.do("%s Primicerius (Aged %i)" % (pi.gender, pi.age))
        DisplayInTextScroll.do("Primicerius ID  : %s" % pi.primicerius_id)
        DisplayInTextScroll.do("Primicerius Rank: %s" % "Rank 1 - Neophyte")
        DisplayInTextScroll.newline()

        DisplayInTextScroll.do(
            "Strength    : %i (%s%i)"
            % (
                stat_block.strength,
                "+" if get_stat_modifier(stat_block.strength) > 0 else "",
                get_stat_modifier(stat_block.strength),
            )
        )
        DisplayInTextScroll.do(
            "Dexterity   : %i (%s%i)"
            % (
                stat_block.dexterity,
                "+" if get_stat_modifier(stat_block.dexterity) > 0 else "",
                get_stat_modifier(stat_block.dexterity),
            )
        )
        DisplayInTextScroll.do(
            "Constitution: %i (%s%i)"
            % (
                stat_block.constitution,
                "+" if get_stat_modifier(stat_block.constitution) > 0 else "",
                get_stat_modifier(stat_block.constitution),
            )
        )
        DisplayInTextScroll.do(
            "Intelligence: %i (%s%i)"
            % (
                stat_block.intelligence,
                "+" if get_stat_modifier(stat_block.intelligence) > 0 else "",
                get_stat_modifier(stat_block.intelligence),
            )
        )
        DisplayInTextScroll.do(
            "Wisdom      : %i (%s%i)"
            % (
                stat_block.wisdom,
                "+" if get_stat_modifier(stat_block.wisdom) > 0 else "",
                get_stat_modifier(stat_block.wisdom),
            )
        )
        DisplayInTextScroll.do(
            "Charisma    : %i (%s%i)"
            % (
                stat_block.charisma,
                "+" if get_stat_modifier(stat_block.charisma) > 0 else "",
                get_stat_modifier(stat_block.charisma),
            )
        )
        DisplayInTextScroll.newline()
        DisplayInTextScroll.do("Health      : %i/%i" % (player_inst.status.current_health, stat_block.max_health))
        DisplayInTextScroll.do("Base AC     : %i" % player_inst.get_ac())
        DisplayInTextScroll.do(
            "Proficiency : %s%i" % ("+" if stat_block.proficiency > 0 else "", stat_block.proficiency)
        )

    def _show_player_nation(self, command):
        n = player_inst.nation
        DisplayInTextScroll.do("%s Nation" % n.name.upper())
        DisplayInTextScroll.do("Owner     : %s" % player_inst.get_name())
        DisplayInTextScroll.do("Influence : %i" % n.influence)
        DisplayInTextScroll.do("Worlds    : %i" % len(n.worlds))
        DisplayInTextScroll.do("Credits   : %i" % n.credits)

    def display_help(self, commands: []):
        if len(commands) == 1:
            all_commands = sorted(
                list(
                    dict.fromkeys(
                        list(self._command_action_mapping)
                        + list(self._command_dynamic_function_mapping)
                        + list(self._action_command_mapping)
                        + list(self._multi_word_commands)
                    )
                )
            )
            DisplayInTextScroll.do("Enabled commands: %s" % all_commands)
            DisplayInTextScroll.do("Type 'help command' for help on a specific command.")
            DisplayInTextScroll.newline()
            DisplayInTextScroll.do("[ or ]: Scroll up/down inventory panel.")
            DisplayInTextScroll.do(".: Move game by one tick.")
            DisplayInTextScroll.do("Arrows: Perform selected mode action in the selected direction.")
            DisplayInTextScroll.do("-: Cycle through arrow key actions.")
            DisplayInTextScroll.do("=: Cycle through mouse click actions.")
            DisplayInTextScroll.newline()
        else:
            specific_help = commands[1]
            if specific_help in self._command_action_mapping.keys():
                DisplayInTextScroll.do("%s: %s" % (specific_help, self._command_action_mapping[specific_help][1]))
            elif specific_help in self._command_dynamic_function_mapping.keys():
                DisplayInTextScroll.do(
                    "%s: %s" % (specific_help, self._command_dynamic_function_mapping[specific_help][1])
                )
            elif specific_help in self._action_command_mapping.keys():
                DisplayInTextScroll.do("%s: %s" % (specific_help, self._action_command_mapping[specific_help][1]))
            elif specific_help in self._multi_word_commands.keys():
                DisplayInTextScroll.do("%s" % self._multi_word_commands[specific_help][1])
            else:
                DisplayInTextScroll.do("Could not find help topic for %s command." % specific_help)

    def _parse_multi_word_command(self, command_parts: []):
        command_key = command_parts[0]
        self._multi_word_commands[command_key][0](command_parts)

    def _toggle(self, command_parts: []):
        if len(command_parts) == 1:
            DisplayInTextScroll.do("Toggleables: [autoclosedoor, autouse]")
        elif command_parts[1] == "autouse":
            player_inst.autouse = not player_inst.autouse
            DisplayInTextScroll.do("Player autouse is now %s." % ("on" if player_inst.autouse else "off"))
        elif command_parts[1] == "autoclosedoor":
            player_inst.autoclosedoor = not player_inst.autoclosedoor
            DisplayInTextScroll.do("Player autoclosedoor is now %s." % ("on" if player_inst.autoclosedoor else "off"))

    @staticmethod
    def _equip(command_parts: []):
        from poc.ui.singletons import inventory_window
        from poc.engine.actions.player_status_actions import RefreshPlayerInventoryAction

        if len(command_parts) < 2:
            DisplayInTextScroll.do("You need to specify the item to equip.")
            return

        item = inventory_window.get_item_at_index(int(command_parts[1]))
        if item and item.is_equippable():
            player_inst.equip(item)
            RefreshPlayerInventoryAction().act()
            DisplayInTextScroll.do("You equipped the %s." % item.get_name())
        elif item and not item.is_equippable():
            DisplayInTextScroll.do("You cannot equip the %s." % item.get_name())
        else:
            DisplayInTextScroll.do("No item at index %i." % int(command_parts[1]))

    @staticmethod
    def _unequip(command_parts: []):
        from poc.ui.singletons import inventory_window
        from poc.engine.actions.player_status_actions import RefreshPlayerInventoryAction

        item = inventory_window.get_item_at_index(int(command_parts[1]))
        if item and player_inst.is_equipped(item):
            player_inst.unequip(item)
            RefreshPlayerInventoryAction().act()
            DisplayInTextScroll.do("You unequipped the %s." % item.get_name())
        elif item and not player_inst.is_equipped(item):
            DisplayInTextScroll.do("The %s is not equipped." % item.get_name())
        else:
            DisplayInTextScroll.do("No item at index %i." % int(command_parts[1]))

    @staticmethod
    def _parse_item_use(command_parts: []):
        from poc.ui.singletons import inventory_window

        item = inventory_window.get_item_at_index(int(command_parts[1]))
        if item and item.use:
            item.use.use(player_inst)
        else:
            DisplayInTextScroll.do("No item at index %i." % int(command_parts[1]))

    @staticmethod
    def _wait(command_parts: []):
        allow = False
        if len(command_parts) >= 3 and command_parts[2] == "allow":
            allow = True
        if len(command_parts) <= 1:
            DisplayInTextScroll.do("Wait 1 minute: wait %i" % 60)
            DisplayInTextScroll.do("Wait 10 minutes: wait %i" % (10 * 60))
            DisplayInTextScroll.do("Wait 30 minutes: wait %i" % (30 * 60))
            DisplayInTextScroll.do("Wait 1 hour: wait %i" % (60 * 60))
            DisplayInTextScroll.do("Wait 6 hours: wait %i" % (6 * 60 * 60))
            DisplayInTextScroll.do("Wait 12 hours: wait %i" % (12 * 60 * 60))
            DisplayInTextScroll.do("Wait 1 day (max): wait %i" % (24 * 60 * 60))
        else:
            seconds = int(command_parts[1])
            if seconds > 86400 and not allow:
                DisplayInTextScroll.do("Cannot wait more than 1 day (86400 seconds).")
            else:
                from poc.engine import event_manager_inst
                from poc.engine.event_manager import PLAYER_WAIT_EVENT
                from poc.engine.universal_time import UniversalTime

                duration = UniversalTime(0, 0, 0, seconds)
                DisplayInTextScroll.do("Waiting for %s." % duration.duration_string())
                if seconds > 86400 and allow:
                    DisplayInTextScroll.do("WARNING: This will make the game wait for a long time!")
                event_manager_inst.notify(Event(PLAYER_WAIT_EVENT, seconds))

    @staticmethod
    def _process_debug(command: str):
        from poc.utils.log_manager import logger
        from poc.engine import stage_manager_inst

        logger.debug("Debug command: %s" % command)
        command_details = command.split(" ")
        if len(command_details) == 1 or (len(command_details) == 2 and command_details[1] == "help"):
            DisplayInTextScroll.do("Debug commands available: [fillinv, set, getpos, vis]")
            return
        elif command_details[1] == "set":
            if (
                command_details[2] not in player_inst.status.limb_condition.keys()
                and command_details[2] not in player_inst.status.armour_condition.keys()
                and command_details[2] not in player_inst.survival_stats.keys()
            ):
                DisplayInTextScroll.do(
                    "Invalid 'set' command; allowed values are: %s"
                    % (
                        str(
                            list(player_inst.status.limb_condition)
                            + list(player_inst.status.armour_condition)
                            + list(player_inst.survival_stats)
                        )
                    )
                )
            elif not command_details[3].isnumeric():
                DisplayInTextScroll.do(
                    "Invalid 'set %s' command; make sure the fourth parameter is a number." % command_details[2]
                )
                return
            elif command_details[2] in player_inst.status.limb_condition.keys():
                player_inst.status.limb_condition[command_details[2]].curr = int(command_details[3])
            elif command_details[2] in player_inst.status.armour_condition.keys():
                player_inst.status.armour_condition[command_details[2]].curr = int(command_details[3])
            elif command_details[2] in player_inst.survival_stats.keys():
                player_inst.survival_stats[command_details[2]].curr = int(command_details[3])
        elif command_details[1] == "getpos":
            y, x = stage_manager_inst.get_stage().get_pos(player_inst)
            DisplayInTextScroll.do("Player position: (%i,%i) in (y,x) coords." % (y, x))
        elif command_details[1] == "vis":
            from poc.ui.singletons import view_port

            player_inst.show_vis = not player_inst.show_vis
            if not player_inst.show_vis:
                stage_manager_inst.current_stage.clear_visible()
            player_y, player_x = stage_manager_inst.get_stage().get_pos(player_inst)
            visibility = stage_manager_inst.visible_tiles(player_y, player_x, view_port.delta_y, view_port.delta_x, view_port.height, view_port.width, True)
            for y in range(0, view_port.height):
                for x in range(0, view_port.width):
                    if not visibility[y][x] and player_inst.show_vis:
                        view_port.overlay[y][x] = HIDDEN_TILE
                    else:
                        view_port.overlay[y][x] = None
            view_port.refresh()
        else:
            logger.debug("Debug command not recognized")
            DisplayInTextScroll.do("Debug command not recognized")
