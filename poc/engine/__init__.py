from poc.engine.command_processor import CommandProcessor
from poc.engine.dialogue.dialogue_generator import DialogueGenerator
from poc.engine.environment_manager import EnvironmentManager
from poc.engine.event_manager import EventManager
from poc.engine.game_manager import GameManager
from poc.engine.keyboard_input_manager import KeyboardInputManager
from poc.engine.menu import MenuLibrary
from poc.engine.menu_manager import MenuManager
from poc.engine.stage_manager import StageManager
from poc.engine.window_manager import WindowManager


class AppController:
    def __init__(self):
        self.running = True


app_ctrl_inst = AppController()
menu_manager_inst = MenuManager()
window_manager_inst = WindowManager()
keyboard_input_manager_inst = KeyboardInputManager()
stage_manager_inst = StageManager()
menu_library_inst = MenuLibrary()
command_processor_inst = CommandProcessor()
environment_manager_inst = EnvironmentManager()
game_manager_inst = GameManager()

event_manager_inst = EventManager()
keyboard_input_manager_inst.register_listener(event_manager_inst.event_listener, True)


def unregister_from_all(identifier):
    keyboard_input_manager_inst.unregister_listener(identifier)
    event_manager_inst.unregister_listener(identifier)
