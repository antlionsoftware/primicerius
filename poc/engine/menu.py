from poc.engine.actions.action import Action
from poc.engine.actions.application_control_actions import KillApplicationAction
from poc.engine.event_listener import EventListener
from poc.engine.keyboard_input_manager import KEYBOARD_EVENT_PRESS, Key


class Menu:
    def __init__(self, menu_id: str, menu_items: [(str, Action)], title: str = None, extra_text: str = None):
        self.menu_id = menu_id
        self.number_of_items = len(menu_items)
        self.menu_item_action_tuples = menu_items
        self.selected_item = 0
        self.menu_event_listener = MenuEventListener(menu_id, self)
        self.active = False
        self.title = title
        self.extra_text = extra_text
        # Fix menu items which may not have been sent as (label, action) tuples
        for i in range(self.number_of_items):
            if not isinstance(self.menu_item_action_tuples[i], tuple):
                self.menu_item_action_tuples[i] = (self.menu_item_action_tuples[i], None)
            elif len(self.menu_item_action_tuples[i]) == 1:
                self.menu_item_action_tuples[i] = (self.menu_item_action_tuples[i][0], None)

    def __str__(self):
        return self.menu_id + " " + str(self.menu_item_action_tuples) + " sel:" + str(self.selected_item)

    def get_expected_draw_height(self):
        return len(self.menu_item_action_tuples) + (1 if self.title is not None else 0)

    def get_expected_draw_width(self):
        width = 2
        for menu_item, menu_action in self.menu_item_action_tuples:
            if len(menu_item) + 2 > width:
                width = len(menu_item) + 2
        if self.title is not None and len(self.title) > width:
            width = len(self.title)
        return width

    def increment_selected_item(self) -> int:
        self.selected_item = self.selected_item + 1 if self.selected_item + 1 < self.number_of_items else 0
        return self.selected_item

    def decrement_selected_item(self) -> int:
        self.selected_item = self.selected_item - 1 if self.selected_item - 1 > -1 else self.number_of_items - 1
        return self.selected_item

    def get_current_menu_item(self):
        return self.menu_item_action_tuples[self.selected_item]

    def get_selected_item_index(self) -> int:
        return self.selected_item

    def reset(self):
        self.selected_item = 0
        self.active = False

    def notify(self, event):
        if event.key == KEYBOARD_EVENT_PRESS:
            if event.event == Key.A_DOWN:
                self.increment_selected_item()
            elif event.event == Key.A_UP:
                self.decrement_selected_item()
            elif event.event == Key.ENTER:
                item, action = self.get_current_menu_item()
                if action is not None:
                    action.act()


class MenuEventListener(EventListener):
    def __init__(self, menu_id: str, menu: Menu):
        super().__init__(menu_id, menu)


class ContainerMenu(Menu):
    def __init__(self, menu_id: str, inventory, inventory_name: str):
        self.inventory = inventory
        super().__init__(menu_id, self._get_actions(), inventory_name)

    def _get_actions(self):
        from poc.engine.actions.menu_actions import ClosePopUpMenuAction
        from poc.engine.actions.game_window_actions import SetGameKillEnabledAction
        actions = [("%s x%i" % (item[1].get_name(), item[2]), item[1]) for item in self.inventory.get_all()]
        actions.append(("Close", [ClosePopUpMenuAction(), SetGameKillEnabledAction(True)]))
        return actions

    def refresh(self):
        self.menu_item_action_tuples = self._get_actions()
        self.number_of_items = len(self.menu_item_action_tuples)
        self.selected_item = 0

    def notify(self, event):
        if event.key == KEYBOARD_EVENT_PRESS:
            if event.event == Key.A_DOWN:
                self.increment_selected_item()
            elif event.event == Key.A_UP:
                self.decrement_selected_item()
            elif event.event == Key.ENTER:
                from poc.entity import player_inst
                from poc.engine import menu_manager_inst
                from poc.engine import keyboard_input_manager_inst
                from poc.engine.actions.menu_actions import ClosePopUpMenuAction
                item_name, item_ref = self.get_current_menu_item()
                if self.get_selected_item_index() == self.number_of_items - 1:
                    for action in item_ref:
                        action.act()
                    keyboard_input_manager_inst.unpromote(self.menu_id)
                    return
                qty = self.inventory.get_item_quantity(item_ref)
                player_inst.inventory.add_item(item_ref, qty)
                self.inventory.remove_item(item_ref.get_identifier(), qty)
                self.refresh()
                menu_manager_inst.close_menu()
                menu_manager_inst.open_menu(self, 5, 15)
                keyboard_input_manager_inst.promote(self.menu_event_listener)



class MenuLibrary:
    def __init__(self):
        from poc.engine.actions.game_window_actions import InitializeGameAction
        from poc.engine.actions.menu_actions import OpenNotificationAction

        self.main_menu = Menu(
            "Menu::main",
            [
                ("Demo", InitializeGameAction()),
                ("New Simulation", OpenNotificationAction("Not Implemented Yet")),
                ("Load Simulation", OpenNotificationAction("Not Implemented Yet")),
                ("Options", OpenNotificationAction("Not Implemented Yet")),
                ("Close Application", KillApplicationAction()),
            ],
            title="Primicerius :: Main Menu",
        )
