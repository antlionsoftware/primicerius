class Event:
    def __init__(self, key, event):
        self.consumed = False
        self.key = key
        self.event = event

    def __str__(self):
        return "key:%s; event:%s%s" % (self.key, self.event, ("; consumed" if self.consumed else ""))


class EventListener:
    def __init__(self, event_listener_id, notifiable):
        self.event_listener_id = event_listener_id
        self.notifiable = notifiable

    def notify(self, event: Event):
        self.notifiable.notify(event)
