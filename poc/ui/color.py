from _curses import init_pair

# Copied from curses for overwriting
BLACK = 0
RED = 1
GREEN = 2
YELLOW = 3
BLUE = 4
MAGENTA = 5
CYAN = 6
WHITE = 7


class ColorManager:
    def __init__(self):
        self.colors = [BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE]
        self.color_codes = {}

    def init_colors(self):
        # Let the default index 1 be white on black
        init_pair(1, WHITE, BLACK)
        index = 2
        for color_fg in self.colors:
            for color_bg in self.colors:
                self.color_codes[(color_fg, color_bg)] = index
                init_pair(index, color_fg, color_bg)
                index = index + 1

    def get_color_pair_index(self, color_fg: int = WHITE, color_bg: int = BLACK):
        return self.color_codes[(color_fg, color_bg)]

    def invert_black_and_white(self):
        bw = self.color_codes[(BLACK, WHITE)]
        wb = self.color_codes[(WHITE, BLACK)]
        self.color_codes[(BLACK, WHITE)] = wb
        self.color_codes[(WHITE, BLACK)] = bw


color_manager_inst = ColorManager()
