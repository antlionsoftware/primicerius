import threading


class FlashController:
    def __init__(self):
        self._flash_on = False
        self._flash_frequency_seconds = 0.5
        self._flash_num = 0
        self._timer = None

    def do_flash(self):
        # TODO See if we can remove the threading here.
        self._flash_on = not self._flash_on
        self._flash_num = self._flash_num + 1
        from poc.ui.singletons import view_port

        view_port.refresh()
        self._timer = threading.Timer(self._flash_frequency_seconds, self.do_flash)
        self._timer.start()

    def cancel(self):
        if self._timer:
            self._timer.cancel()

    def obj_if_flash(self, obj, alt):
        return obj if self._flash_on else alt

    def obj_if_flash_num(self, obj: []):
        return obj[self._flash_num % len(obj)]
