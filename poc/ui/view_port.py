import time

from poc.engine.event_listener import EventListener, Event
from poc.engine.event_manager import GAME_TICK, PLAYER_MOVED_EVENT
from poc.engine.stage_manager import STAGE_CHANGED_EVENT
from poc.entity.entity import Tag
from poc.ui.color import *
from poc.ui.terminal_window import TerminalWindow
from poc.entity.tile_lib import UNKNOWN_TILE, UNKNOWN_BG_TILE, HIDDEN_TILE, NOISE_TILE, CHAR_BOX_0, CHAR_COMMA, \
    CHAR_PERIOD
from poc.utils import space_text
from settings import view_height, view_width, view_x, view_y, loc_info_height, loc_info_width, loc_info_y, loc_info_x

view_port_id = "ViewPort"
location_infobox_id = "LocationInfobox"


class ViewPort:
    def __init__(self):
        self._current_stage = None
        self.view_port_terminal_window = TerminalWindow(
            view_port_id + "::TerminalWindow", view_height, view_width, view_y, view_x
        )
        self.event_listener = EventListener(view_port_id + "::EventListener", self)
        self.overlay = [[None for x in range(0, view_width)] for y in range(0, view_height)]
        self.height = view_height
        self.width = view_width
        self.delta_y = 0
        self.delta_x = 0

    def notify(self, event: Event):
        if event.key == STAGE_CHANGED_EVENT:
            self._current_stage = event.event
            self.overlay = [[None for x in range(0, self._current_stage.width)] for y in range(0, self._current_stage.height)]
            self.center_on_player()
            self.refresh()

        if event.key == PLAYER_MOVED_EVENT:
            self.center_on_player()
            self.refresh()

        if event.key == GAME_TICK:
            self.refresh()

    def center_on_player(self):
        from poc.entity import player_inst
        py, px = self._current_stage.get_pos(player_inst)
        ty = round(self.height / 2)
        tx = round(self.width / 2)

        if py < ty:
            self.delta_y = 0
        else:
            self.delta_y = min(self._current_stage.height - self.height, py - ty)
        if px < tx:
            self.delta_x = 0
        else:
            self.delta_x = min(self._current_stage.width - self.width, px - tx)
        from poc.utils.log_manager import logger
        logger.debug("Delta set to (%i,%i) for pos (%i,%i)." % (self.delta_y, self.delta_x, py, px))

    def refresh(self):
        from poc.ui.color import color_manager_inst
        from poc.ui.singletons import flash_controller
        from poc.entity import player_inst
        from _random import Random

        # TODO: Support drawing larger stages based on position
        random = Random()

        for (screen_y, stage_y) in ((y, y + self.delta_y) for y in range(0, view_height)):
            for (screen_x, stage_x) in ((x, x + self.delta_x) for x in range(0, view_width)):
                random.seed(stage_y + (stage_x * 1000) + (self._current_stage.y_coord * 1000000) + (self._current_stage.x_coord * 1000000000))
                rand_chance_for_tile = random.random()
                entity_at = self._current_stage.entity_at(stage_y, stage_x)
                bg_entity_at = self._current_stage.bg_entity_at(stage_y, stage_x)
                overlay = self.overlay[screen_y][screen_x]
                fg = BLACK
                bg = WHITE
                # Overlay
                if overlay:
                    if entity_at and entity_at.get_makes_noise() and overlay == HIDDEN_TILE:

                        self.view_port_terminal_window.chars[screen_y][screen_x] = flash_controller.obj_if_flash(
                            NOISE_TILE, CHAR_BOX_0
                        )

                        if Tag.BEHAVIOUR_HOSTILE in entity_at.get_tags():
                            fg = RED
                        elif Tag.BEHAVIOUR_FRIENDLY in entity_at.get_tags():
                            fg = GREEN
                        elif Tag.BEHAVIOUR_PASSIVE in entity_at.get_tags():
                            fg = BLUE
                        else:
                            fg = BLACK
                    elif (
                        entity_at
                        and entity_at.get_static()
                        and entity_at.get_tile()
                        and self._current_stage.was_static_entity_seen_by_player(stage_y, stage_x)
                    ):
                        self.view_port_terminal_window.chars[screen_y][screen_x] = entity_at.get_tile().char()
                        fg = entity_at.get_tile().color()
                    else:
                        self.view_port_terminal_window.chars[screen_y][screen_x] = overlay
                        fg = BLACK
                # No overlay but FG
                elif entity_at:
                    if entity_at.get_tile():
                        if entity_at in player_inst.danger_entities:
                            fg = flash_controller.obj_if_flash(BLACK, RED)
                        else:
                            fg = entity_at.get_tile().color()
                        self.view_port_terminal_window.chars[screen_y][screen_x] = entity_at.get_tile().char()
                    else:
                        self.view_port_terminal_window.chars[screen_y][screen_x] = UNKNOWN_TILE
                        fg = BLACK
                    if bg_entity_at:
                        if bg_entity_at.get_tile():
                            bg = bg_entity_at.get_tile().color()
                        else:
                            bg = WHITE
                # Only BG
                elif bg_entity_at:
                    if bg_entity_at.get_tile():
                        if Tag.GROUND in bg_entity_at.get_tags() and rand_chance_for_tile < 0.1:
                            fg = GREEN
                            self.view_port_terminal_window.chars[screen_y][screen_x] = CHAR_COMMA
                        elif Tag.GROUND in bg_entity_at.get_tags() and rand_chance_for_tile < 0.2:
                            fg = GREEN
                            self.view_port_terminal_window.chars[screen_y][screen_x] = CHAR_PERIOD
                        else:
                            fg = BLACK
                            self.view_port_terminal_window.chars[screen_y][screen_x] = bg_entity_at.get_tile().char()
                        bg = bg_entity_at.get_tile().color()
                    else:
                        self.view_port_terminal_window.chars[screen_y][screen_x] = UNKNOWN_BG_TILE
                        bg = WHITE
                self.view_port_terminal_window.colors[screen_y][screen_x] = color_manager_inst.get_color_pair_index(fg, bg)


class LocationInfobox:
    def __init__(self):
        self._current_stage = None
        self.location_infobox_terminal_window = TerminalWindow(
            location_infobox_id + "::TerminalWindow", loc_info_height, loc_info_width, loc_info_y, loc_info_x
        )
        self.event_listener = EventListener(location_infobox_id + "::EventListener", self)

    def notify(self, event: Event):
        if event.key == STAGE_CHANGED_EVENT:
            self._current_stage = event.event
            self.refresh()

    def refresh(self):
        area = self._current_stage.area
        planet = area.planet
        system = planet.system

        planet_text = system.name
        planet_text = planet_text + ", " + planet.name
        loc_text = area.name
        loc_text = loc_text + " (" + str(self._current_stage.x_coord) + "," + str(self._current_stage.y_coord) + ")"

        from poc.engine import environment_manager_inst

        weather_text = str(environment_manager_inst.weather) + ", " + environment_manager_inst.time.tod()
        # TODO
        left_mid = ""
        from poc.entity import player_inst

        safety_text = player_inst.get_safety_rating().value

        top_text = space_text(weather_text, planet_text, loc_info_width)
        mid_text = space_text(left_mid, loc_text, loc_info_width)
        bottom_text = space_text(safety_text, str(environment_manager_inst.time), loc_info_width)

        self.location_infobox_terminal_window.draw(top_text, 0)
        self.location_infobox_terminal_window.draw(mid_text, 1)
        self.location_infobox_terminal_window.draw(bottom_text, 2)
