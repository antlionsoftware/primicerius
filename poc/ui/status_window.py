from poc.entity import player_inst
from poc.entity.player import KEY_ENERGY, KEY_THIRST, KEY_HUNGER
from poc.ui.terminal_window import TerminalWindow
from poc.entity.tile_lib import *


class StatusWindow:
    def __init__(self):
        self.player_status = player_inst.status
        # TODO put these in settings
        self.status_terminal_window = TerminalWindow("Status", 10, 25, 0, 82)
        self._max_bars = 6

    def refresh(self):
        self.status_terminal_window.draw(self._first_line(), 0)
        self.status_terminal_window.draw(self._second_line(), 1)
        self.status_terminal_window.draw(self._third_line(), 2)
        self.status_terminal_window.draw("            ░            ", 3)
        self.status_terminal_window.draw(self._fifth_line(), 4)
        self.status_terminal_window.draw("         █ ███ █         ", 5)
        self.status_terminal_window.draw(self._seventh_line(), 6)
        self.status_terminal_window.draw(self._eighth_line(), 7)
        self.status_terminal_window.draw(self._ninth_line(), 8)
        self.status_terminal_window.draw(self._tenth_line(), 9)

    def _first_line(self):
        line = ""
        if self.player_status.bleeding:
            line = line + "BLD"
        else:
            line = line + "   "
        line = line + " "
        if len(self.player_status.extra_conditions) > 0:
            pass
            # TODO
        else:
            line = line + "   "
        line = line + "               "
        line = line + player_inst.key_mode.value
        return line

    def _second_line(self):
        line = ""
        if self.player_status.bleeding:
            line = line + "PSN"
        else:
            line = line + "   "
        line = line + " "
        if len(self.player_status.extra_conditions) > 0:
            pass
            # TODO
        else:
            line = line + "   "
        line = line + "    ███     "
        return line + "   " + player_inst.click_mode.value

    def _third_line(self):
        line = ""
        if self.player_status.fatigued:
            line = line + "FTG"
        else:
            line = line + "   "
        line = line + " "
        if len(self.player_status.extra_conditions) > 0:
            pass
            # TODO
        else:
            line = line + "   "
        line = line + "    ███           "
        return line

    def _fifth_line(self):
        line = "        "
        line = line + " █░███░█     "
        return line

    def _seventh_line(self):
        line = "           ░ ░           "
        return line

    def _eighth_line(self):
        line = "          "
        line = line + " █ █     "
        line = line + "  " + STAT_HEALTH
        if player_inst.status.current_health > 999:
            line = line + "+++"
        else:
            line = line + ("%i" % player_inst.status.current_health).rjust(3)
        return line

    def _ninth_line(self):
        line = "[" + self._get_bars(player_inst.survival_stats[KEY_HUNGER].get_percentage()).ljust(self._max_bars)
        line = line + "]H  █ █       " + STAT_AC
        line = line + ("%i" % player_inst.get_ac()).rjust(3)
        return line

    def _tenth_line(self):
        line = "[" + self._get_bars(player_inst.survival_stats[KEY_THIRST].get_percentage()).ljust(self._max_bars)
        line = line + "]T       E["
        if player_inst.energy_pack:
            line = line + self._get_bars(player_inst.energy_pack.current_energy / player_inst.energy_pack.max_energy * 100).rjust(self._max_bars)
        else:
            line = line + ("X" * self._max_bars)
        return line + "]"

    @staticmethod
    def _draw_percent_right(value: int):
        width = 3
        return "%s" % str(round(value)).rjust(width)

    def _get_bars(self, percent: int):
        bar = "="
        nil = "X"
        if percent > 99:
            return bar * self._max_bars
        if percent <= 0:
            return nil * self._max_bars
        else:
            return round(percent / 100 * self._max_bars) * bar
