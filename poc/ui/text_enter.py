from poc.engine.actions.input_actions import TextEntered
from poc.engine.event_listener import EventListener
from poc.engine.keyboard_input_manager import KEYBOARD_EVENT_PRESS, Key
from poc.ui.terminal_window import TerminalWindow
from poc.entity.tile_lib import CHAR_BOX_0
from settings import text_enter_x, text_enter_y, text_enter_width, text_enter_height

text_enter_id = "TextEnter"


class TextEnter:
    def __init__(self):
        self.text_enter_terminal_window = _TextEnterTerminalWindow()
        self.event_listener = _TextEnterEventListener(self)
        self.buffer = "> "
        self.text_enter_terminal_window.draw(self.buffer)
        self._max_input_buffer = text_enter_width - 4
        self._reserved_keys = []

    def notify(self, event):
        updated = False
        if event.key == KEYBOARD_EVENT_PRESS:
            key = event.event
            if key == Key.BACKSPACE and len(self.buffer) > 2:
                self.buffer = self.buffer[0:-1]
                updated = True
            # Scenarios where we will not enter text
            elif self._reserved(key):
                pass
            elif not isinstance(key, Key) and chr(key).isprintable():
                # Cannot begin the buffer with a digit
                self.buffer = self.buffer + chr(key)
                updated = True
            elif key == Key.ENTER and len(self.buffer) > 2:
                TextEntered.do(self.buffer[2:])
                self.buffer = "> "
                updated = True
        if updated:
            self.text_enter_terminal_window.fill(CHAR_BOX_0)
            self.text_enter_terminal_window.draw(self.buffer)

    def _reserved(self, key) -> bool:
        # Do not allow the first character to be a digit
        return (
            (not isinstance(key, Key) and chr(key).isdigit() and not self._text_entered())
            or (len(self.buffer) > self._max_input_buffer)
            or (not self._text_entered() and not isinstance(key, Key) and chr(key) == ".")
            or (not self._text_entered() and not isinstance(key, Key) and chr(key) == "-")
            or (not self._text_entered() and not isinstance(key, Key) and chr(key) == "=")
        )

    def _text_entered(self) -> bool:
        return not len(self.buffer) == 2


class _TextEnterTerminalWindow(TerminalWindow):
    def __init__(self):
        super().__init__(
            text_enter_id + "::TerminalWindow", text_enter_height, text_enter_width, text_enter_y, text_enter_x
        )


class _TextEnterEventListener(EventListener):
    def __init__(self, text_enter):
        super().__init__(text_enter_id + "::EventListener", text_enter)
