import textwrap

from poc.ui.terminal_window import TerminalWindow
from poc.entity.tile_lib import CHAR_BOX_0
from settings import text_scroll_width, text_scroll_height, text_scroll_y, text_scroll_x

text_scroll_id = "TextScroll"


class TextScroll:
    def __init__(self):
        self.text_scroll_terminal_window = _TextScrollTerminalWindow()
        self.text_scroll_terminal_window.fill(CHAR_BOX_0)
        self._max_lines = text_scroll_height
        self._next_line = self._max_lines
        self._empty = CHAR_BOX_0 * text_scroll_width

    def clear(self):
        for i in range(0, self._max_lines):
            self.new_text()

    def new_text(self, text: str = ""):
        if not text:
            self._draw_line("")
        else:
            lines = textwrap.wrap(text, text_scroll_width, break_long_words=False)
            for line in lines:
                self._draw_line(line)

    def _draw_line(self, line: str):
        # Input is _next_line
        if self._next_line == self._max_lines:
            self._scroll()
        # Draw at end of scroll
        self.text_scroll_terminal_window.draw(self._fill_line(line), self._next_line)
        self._next_line = self._next_line + 1
        if self._next_line > self._max_lines:
            self._next_line = self._max_lines

    @staticmethod
    def _fill_line(line: str):
        return line + CHAR_BOX_0 * (text_scroll_width - len(line))

    def _scroll(self):
        for i in range(1, self._max_lines):
            self.text_scroll_terminal_window.draw(self.text_scroll_terminal_window.chars[i], i - 1)
        self._next_line = self._max_lines - 1


class _TextScrollTerminalWindow(TerminalWindow):
    def __init__(self):
        super().__init__(
            text_scroll_id + "::TerminalWindow", text_scroll_height, text_scroll_width, text_scroll_y, text_scroll_x
        )
