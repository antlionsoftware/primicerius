from poc.engine.event_listener import EventListener, Event
from poc.engine.keyboard_input_manager import KEYBOARD_EVENT_PRESS, Key
from poc.entity import player_inst
from poc.ui.terminal_window import TerminalWindow
from poc.entity.tile_lib import CHAR_BOX_0, CHAR_ARROW_UP, CHAR_ARROW_DOWN
from settings import inv_height, inv_width, inv_y, inv_x

inventory_id = "Inventory"


class InventoryWindow:
    def __init__(self):
        self.inventory_terminal_window = TerminalWindow(
            inventory_id + "::TerminalWindow", inv_height, inv_width, inv_y, inv_x
        )
        self.inventory_event_listener = _InventoryEventListener(self)
        self._inventory_item_label_max_width = inv_width - 8
        self._inventory_offset = 0
        self._player_inventory_hook = player_inst.inventory
        # This has to be at the bottom
        self.refresh()

    def refresh(self):
        # Draw above line
        if self._has_more_above():
            self.inventory_terminal_window.draw(CHAR_ARROW_UP, 0)
        else:
            self.inventory_terminal_window.draw("=" * inv_width, 0)

        inv_size = len(self._player_inventory_hook)
        for i in range(0, inv_size if inv_size < 8 else 8):
            item = self._player_inventory_hook.get_item_at_index(i + self._inventory_offset)
            qty = self._player_inventory_hook.get_item_quantity_at_index(i + self._inventory_offset)
            name = item.get_name()
            from poc.entity.entity import Tag
            if Tag.WEAPON in item.get_tags() or Tag.ENERGY_PACK in item.get_tags():
                name = name + " " + item.get_spec()
            self.inventory_terminal_window.draw(
                self._get_inventory_label(i + 1, name, qty, player_inst.is_equipped(item)), i + 1
            )

        for i in range(inv_size if inv_size < 8 else 8, 8):
            self.inventory_terminal_window.draw(self._get_inventory_label(i + 1, "", -1, False), i + 1)

        credits_string = "Credits: %i" % player_inst.credits
        filler_char = CHAR_ARROW_DOWN if self._has_more_below() else "="

        self.inventory_terminal_window.draw(filler_char * (inv_width - len(credits_string) - 1) + " " + credits_string, 9)

    def _get_inventory_label(self, index: int, inventory_item_label: str, quantity: int, is_equipped: bool):
        if quantity < 0:
            quantity_string = "XX"
        elif quantity > 99:
            quantity_string = "++"
        else:
            quantity_string = "{:02d}".format(quantity)

        inventory_item_label_string = inventory_item_label
        if len(inventory_item_label_string) < self._inventory_item_label_max_width:
            inventory_item_label_string = (
                inventory_item_label_string
                + (self._inventory_item_label_max_width - len(inventory_item_label_string)) * CHAR_BOX_0
            )
        elif len(inventory_item_label_string) > self._inventory_item_label_max_width and not is_equipped:
            inventory_item_label_string = inventory_item_label_string[0 : self._inventory_item_label_max_width]

        if len(inventory_item_label_string) > self._inventory_item_label_max_width - 4 and is_equipped:
            inventory_item_label_string = inventory_item_label_string[0 : self._inventory_item_label_max_width - 4]

        template = "%i: %s (%s)" if not is_equipped else "%i: %s [E] (%s)"
        return template % (index, inventory_item_label_string, quantity_string)

    def notify(self, event: Event):
        from poc.engine.actions.game_window_actions import DisplayInTextScroll
        from poc.ui.singletons import text_enter

        # Only listen to key presses if the player did not insert any other text
        if event.key == KEYBOARD_EVENT_PRESS and len(text_enter.buffer) == 2:
            key = event.event
            if key == Key.INV_UP:
                if self._has_more_above():
                    self._inventory_offset = self._inventory_offset - 1
                    self.refresh()
            elif key == Key.INV_DOWN:
                if self._has_more_below():
                    self._inventory_offset = self._inventory_offset + 1
                    self.refresh()
            elif not isinstance(key, Key) and chr(key).isdigit():
                i = int(chr(key))
                if len(self._player_inventory_hook) >= i + self._inventory_offset:
                    item = self._player_inventory_hook.get_item_at_index(i + self._inventory_offset - 1)
                    tags = None
                    if item.get_tags():
                        tags = list(map(lambda x: x.value, item.get_tags()))
                    from poc.entity.entity import Tag
                    DisplayInTextScroll.do(
                        "%s: %s%s%s"
                        % (
                            item.get_name(),
                            (item.get_spec() + " " if Tag.WEAPON in item.get_tags() else ""),
                            ((str(tags).replace("'", "") + " ") if tags else ""),
                            item.get_description(),
                        )
                    )
                else:
                    DisplayInTextScroll.do("No item in position %i" % i)

    def get_item_at_index(self, key: int):
        if len(self._player_inventory_hook) >= key + self._inventory_offset:
            return self._player_inventory_hook.get_item_at_index(key + self._inventory_offset - 1)
        else:
            return None

    def _has_more_above(self) -> bool:
        return self._inventory_offset != 0

    def _has_more_below(self) -> bool:
        return self._inventory_offset < len(self._player_inventory_hook) - 8


class _InventoryEventListener(EventListener):
    def __init__(self, inventory_window: InventoryWindow):
        super().__init__(inventory_id + "::EventListener", inventory_window)
