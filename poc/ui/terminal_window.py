import curses
import textwrap

from poc.engine.event_listener import EventListener
from poc.engine.menu import Menu
from poc.ui.color import BLACK, WHITE
from poc.utils import space_text
from poc.utils.log_manager import logger


class TerminalWindow:
    """ An individually-modifiable window in the terminal.

    Attributes
    ----------
    height : int
        The height of the terminal window
    width : int
        The width of the terminal window
    window : curses window
        A handle to the underlying curses window
    chars : str[][]
        A 2D array of the characters to draw
    """

    def __init__(self, terminal_id: str, height: int, width: int, y_start: int, x_start: int):
        self.terminal_id = terminal_id
        self.height = height
        self.width = width
        self.chars = [[" " for x in range(width)] for y in range(height)]
        self.colors = [[1 for x in range(width)] for y in range(height)]
        self.dim = [[False for x in range(width)] for y in range(height)]
        # +2 for borders
        self.window = curses.newwin(height + 2, width + 2, y_start, x_start)
        self.has_border = True

    def draw(self, text: str, line: int = 0):
        """ Draws the provided string at the provided line.

        :param text: The string to draw.
        :param line: The line on which to draw it.
        """
        if len(text) > self.width:
            raise Exception("String too large")
        for i in range(len(text)):
            self.chars[line][i] = text[i]

    def draw_centered(self, text: str, line: int, custom_spacing=None):
        """ Draws the provided string at the provided line, centered.

        :param text: The string to draw.
        :param line: The line on which to draw it.
        :param custom_spacing: [OPTIONAL] The spacing (from the left)
        :raises Exception: If the string is too long.
        """
        if len(text) > self.width:
            raise Exception("String too large")
        spacing = 0 if custom_spacing is None else custom_spacing
        for i in range(len(text)):
            self.chars[line][int(spacing + i)] = text[i]

    def draw_spaced(self, text: [], start_line: int = 0, left: bool = False):
        spacing = 10000 if not left else 0
        for line_text in text:
            if len(line_text) > self.width:
                raise Exception("String too large")
            line_spacing = (self.width - len(line_text)) / 2
            if line_spacing < spacing:
                spacing = line_spacing

        line_no = start_line
        for line_text in text:
            self.draw_centered(line_text, line_no, spacing)
            line_no += 1

    def lower_right(self, text: str):
        if len(text) > self.width:
            raise Exception("String too large")
        self.draw(text.rjust(self.width), self.height - 1)

    def lower_left(self, text: str):
        if len(text) > self.width:
            raise Exception("String too large")
        self.draw(text, self.height - 1)

    def fill(self, char: str):
        """ Fills the entire character array with the provided character.

        :param char: The character to fill the screen with.
        """
        for y in range(self.height):
            for x in range(self.width):
                self.chars[y][x] = char[0]

    def refresh(self):
        for y in range(0, self.height):
            for x in range(0, self.width):
                self.window.attron(curses.color_pair(self.colors[y][x]))
                self.window.addstr(y + 1, x + 1, self.chars[y][x])
                self.window.attroff(curses.color_pair(self.colors[y][x]))
        if self.has_border:
            self.window.border()
        self.window.refresh()


class MenuWindow(TerminalWindow):
    def __init__(self, menu: Menu, height: int, width: int, y_start: int, x_start: int):
        super().__init__(menu.menu_id, height, width, y_start, x_start)
        self.menu = menu

    def refresh(self):
        super().refresh()
        full_text = []
        line = 0
        if self.menu.title is not None:
            self.draw_centered(self.menu.title, line)
            line = 1
        for i in range(self.menu.number_of_items):
            menu_item, menu_action = self.menu.menu_item_action_tuples[i]
            current_is_selected = self.menu.get_selected_item_index() == i
            if current_is_selected and self.menu.active:
                to_draw = "["
            else:
                to_draw = " "
            to_draw += menu_item
            if current_is_selected and self.menu.active:
                to_draw += "]"
            else:
                to_draw += " "
            line = line + 1
            full_text.append(to_draw)

        self.draw_spaced(full_text, 1 if self.menu.title is not None else 0)


class DialogueWindow(TerminalWindow):
    def __init__(self, menu: Menu, height: int, width: int, y_start: int, x_start: int):
        assert menu.extra_text
        super().__init__(menu.menu_id, height, width, y_start, x_start)
        from poc.ui.color import color_manager_inst
        self.colors = [[color_manager_inst.get_color_pair_index(BLACK, WHITE) for x in range(width)] for y in range(height)]
        self.menu = menu

    def refresh(self):
        super().refresh()
        text = [self.menu.title, ""]

        dialogue = textwrap.wrap(self.menu.extra_text, self.width, break_long_words=False)
        for line in dialogue:
            text.append(line)
        text.append("")

        for i in range(0, self.height - len(text)):
            item = self.menu.menu_item_action_tuples[i][0] if i < self.menu.number_of_items else ""
            if item == "":
                break
            if self.menu.get_selected_item_index() == i:
                item = "[" + item + "]"
            else:
                item = " " + item + " "

            text.append(item)

        self.draw_spaced(text, 0, True)


class TightMenuWindow(MenuWindow):
    def __init__(self, menu: Menu, y_start: int, x_start: int):
        height = menu.get_expected_draw_height()
        width = menu.get_expected_draw_width()
        super().__init__(menu, height, width, y_start, x_start)
