from poc.ui.flash_controller import FlashController
from poc.ui.inventory_window import InventoryWindow
from poc.ui.status_window import StatusWindow
from poc.ui.terminal_window import TerminalWindow
from poc.ui.text_enter import TextEnter
from poc.ui.text_scroll import TextScroll
from poc.ui.view_port import ViewPort, LocationInfobox
from settings import app_window_height, app_window_width

app_window = TerminalWindow("AppWindow", app_window_height, app_window_width, 0, 0)
view_port = ViewPort()
loc_infobox = LocationInfobox()
status_window = StatusWindow()
inventory_window = InventoryWindow()
text_scroll = TextScroll()
text_enter = TextEnter()

flash_controller = FlashController()
