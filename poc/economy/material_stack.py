from poc.entity.entity import Tag
from poc.entity.item.item import Item


class MaterialStack(Item):
    """
    A stack of 1000 materials, the base unit for construction and engineering.
    """
    def __init__(self, material: Item):
        assert Tag.MATERIAL in material.get_tags()
        super().__init__(
            material.get_identifier() + "::Stack",
            material.get_name() + " (Stack)",
            material.get_description(),
            use=None,
            tags=[Tag.STACK] + material.get_tags(),
            base_price=material.base_price)
        self.material = material
        self.base_cost = material.base_price * 1000
