from enum import Enum
from inspect import getframeinfo, stack

from settings import log_level


class PrimiceriusLogLevel(Enum):
    KEYSTROKE = 0
    DEBUG = 1
    INFO = 2
    WARN = 3
    ERROR = 4
    FATAL = 5


class PrimiceriusLogger:
    def __init__(self):
        self._clear()
        self.log_file = open("log.txt", "a+")
        self.log_level = log_level

    def _clear(self):
        open("log.txt", "w").close()

    def close(self):
        self.log_file.close()

    def keystroke(self, key):
        if self.log_level > PrimiceriusLogLevel.KEYSTROKE.value:
            return
        self.log_file.write("<KEY> %i\r\n" % key)
        self.log_file.flush()

    def debug(self, message: str):
        if self.log_level > PrimiceriusLogLevel.DEBUG.value:
            return
        caller = getframeinfo(stack()[1][0])
        self.log_file.write("[DEBUG] %s:%d - %s\r\n" % (caller.filename, caller.lineno, message))
        self.log_file.flush()

    def time(self, message: str, seconds: float, threshold: float = 0):
        if seconds > threshold:
            self.warn("%s%s (%fs)" % (("Threshold exceeded. " if threshold != 0 else ""), message, seconds))

    def info(self, message: str):
        if self.log_level > PrimiceriusLogLevel.INFO.value:
            return
        self.log_file.write("[INFO] %s\r\n" % message)
        self.log_file.flush()

    def warn(self, message: str):
        if self.log_level > PrimiceriusLogLevel.WARN.value:
            return
        caller = getframeinfo(stack()[1][0])
        self.log_file.write("[WARN] %s:%d - %s\r\n" % (caller.filename, caller.lineno, message))
        self.log_file.flush()

    def error(self, message: str, exception: Exception = None):
        if self.log_level > PrimiceriusLogLevel.ERROR.value:
            return
        caller = getframeinfo(stack()[1][0])
        self.log_file.write(">>>[ERROR]<<< %s:%d - %s (%s)\r\n" % (caller.filename, caller.lineno, message, exception))
        self.log_file.flush()

    def fatal(self, message: str, exception: Exception = None):
        if self.log_level > PrimiceriusLogLevel.FATAL.value:
            return
        caller = getframeinfo(stack()[1][0])
        self.log_file.write(">>>[ERROR]<<< %s:%d - %s (%s)\r\n" % (caller.filename, caller.lineno, message, exception))
        self.log_file.flush()


logger = PrimiceriusLogger()
