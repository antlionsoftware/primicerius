class ModelException(Exception):
    def __init__(self, message):
        super().__init__(message)


class EntityUninstantiableException(Exception):
    def __init__(self, entity):
        super().__init__("Cannot create instance of %s." % entity)
