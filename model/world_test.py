import unittest

from model.world import *


class TestPlanet(unittest.TestCase):
    def test_tinyPlanetSize_setArea_areaSetInProperLocation(self):
        planet = Planet("test", PlanetSize.TINY)
        area = Area(Coord2D(0, 0))
        planet.set_area(area)
        self.assertEqual(area, planet.get_area(Coord2D(0, 0)))

    def test_gargantuanPlanetSize_setAreaTopLeft_areaSetInProperLocation(self):
        planet = Planet("test", PlanetSize.GARGANTUAN)
        area = Area(Coord2D(0, 0))
        planet.set_area(area)
        self.assertEqual(area, planet.get_area(Coord2D(0, 0)))

    def test_gargantuanPlanetSize_setAreaBottomRight_areaSetInProperLocation(self):
        planet = Planet("test", PlanetSize.GARGANTUAN)
        area = Area(Coord2D(PlanetSize.GARGANTUAN.value - 1, PlanetSize.GARGANTUAN.value - 1))
        planet.set_area(area)
        self.assertEqual(area, planet.get_area(Coord2D(PlanetSize.GARGANTUAN.value - 1, PlanetSize.GARGANTUAN.value - 1)))


class TestUniverse(unittest.TestCase):
    def test_systemInUniverse_addSameSystem_modelExceptionRaised(self):
        universe = Universe()
        system = System(Star(""), None)
        universe.add_system(system)
        with self.assertRaises(ModelException):
            universe.add_system(system)

    def test_systemInUniverse_addNewSystem_OK(self):
        universe = Universe()
        universe.add_system(System(Star(""), None))
        universe.add_system(System(Star(""), None))


class TestSystem(unittest.TestCase):
    def test_starInSystem_addSameStar_modelExceptionRaised(self):
        star = Star("test")
        system = System(Star(""), None)
        system.add_star(star)
        with self.assertRaises(ModelException):
            system.add_star(star)

    def test_systemDefinedByStar_addSameStar_modelExceptionRaised(self):
        star = Star("test")
        system = System(star, None)
        with self.assertRaises(ModelException):
            system.add_star(star)

    def test_starInSystem_addNewStar_OK(self):
        system = System(Star("test"), None)
        system.add_star(Star("test"))

    def test_systemHasThreeStars_addNewStar__modelExceptionRaised(self):
        system = System(Star("test"), None)
        system.add_star(Star("test"))
        system.add_star(Star("test"))
        with self.assertRaises(ModelException):
            system.add_star(Star("test"))

    def test_systemDefinedByStar_getName_nameSameAsStar(self):
        star = Star("test")
        system = System(star, None)
        self.assertEqual(star.name, system.name)

    def test_planetInSystem_addSamePlanet_modelExceptionRaised(self):
        system = System(Star(""), None)
        planet = Planet(None, PlanetSize.TINY)
        system.add_planet(planet)
        with self.assertRaises(ModelException):
            system.add_planet(planet)

    def test_planetInSystem_addNewPlanet_OK(self):
        system = System(Star(""), None)
        system.add_planet(Planet(None, PlanetSize.TINY))
        system.add_planet(Planet(None, PlanetSize.TINY))
