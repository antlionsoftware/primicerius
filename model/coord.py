import math


class Coord3D:
    def __init__(self, x: int, y: int, z: int):
        self.x = x
        self.y = y
        self.z = z

    def dist(self, other) -> float:
        assert isinstance(other, Coord3D)
        return math.sqrt(math.pow((self.x - other.x), 2) + math.pow((self.y - other.y), 2) + math.pow((self.z - other.z), 2))


class Coord2D(Coord3D):
    def __init__(self, x: int, y: int):
        super().__init__(x, y, 0)
