from enum import Enum

from model.coord import Coord3D, Coord2D
from model.entity import Entity
from model.exceptions import ModelException
from settings import stage_size


class PlanetSize(Enum):
    TINY = 1
    SMALL = 2
    MEDIUM = 3
    LARGE = 4
    XLARGE = 5
    HUGE = 6
    GARGANTUAN = 7


class Tile:
    def __init__(self, entity_bg: Entity = None):
        self.entity_bg = entity_bg
        self.entity_under = None
        self.entity = None


class Stage:
    def __init__(self, entity_bg: Entity = None):
        self.tiles = [[Tile(entity_bg) for y in range(stage_size)] for x in range(stage_size)]


class Area:
    def __init__(self, coord: Coord2D):
        self.coord = coord


class Planet:
    def __init__(self, name: str, size: PlanetSize):
        self.name = name
        self.size = size
        self.areas = [[None for y in range(size.value)] for x in range(size.value)]

    def set_area(self, area: Area):
        c = area.coord
        self.areas[c.y][c.x] = area

    def get_area(self, coord: Coord2D):
        return self.areas[coord.y][coord.x]


class Star:
    def __init__(self, name: str):
        self.name = name


class System:
    def __init__(self, star: Star, coord: Coord3D):
        self.coord = coord
        self.stars = []
        self.stars.append(star)
        self.main_star = star
        self.name = self.main_star.name
        self.planets = []

    def add_star(self, star: Star):
        if len(self.stars) >= 3:
            raise ModelException('System %s already has three stars.' % self.name)
        if star in self.stars:
            raise ModelException('Star %s already in system %s.' % (star.name, self.name))
        self.stars.append(star)

    def add_planet(self, planet: Planet):
        if planet in self.planets:
            raise ModelException('Planet %s already in system %s.' % (planet.name, self))
        self.planets.append(planet)


class Universe:
    def __init__(self):
        self.systems = []

    def add_system(self, system: System):
        if system in self.systems:
            raise ModelException('System %s already in universe.' % system)
        self.systems.append(system)
