import math
import unittest

from model.coord import Coord3D, Coord2D


class Coord3DTest(unittest.TestCase):
    def test_twoCoords_dist_distAccurate(self):
        c1 = Coord3D(0, 0, 0)
        c2 = Coord3D(1, 1, 1)
        self.assertTrue(math.isclose(c1.dist(c2), 1.732051, abs_tol=0.000001))


class Coord2DTest(unittest.TestCase):
    def test_twoCoords_test_distAccurate(self):
        c1 = Coord2D(0, 3)
        c2 = Coord2D(4, 0)
        self.assertEqual(5, c1.dist(c2))
