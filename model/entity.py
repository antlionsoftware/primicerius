import copy
from enum import Enum

from model.exceptions import EntityUninstantiableException


class Tag(Enum):
    ACTOR = "ACTOR"
    # AMMO = "AMMO"
    # ARMOUR = "ARMOUR"
    # ATTACKABLE = "ATTACKABLE"  # This entity can be attacked.
    # BACKGROUND = "BACKGROUND"
    # BALLISTIC = "BALLISTIC"
    # BED = "BED"  # Actors may sleep on this entity.
    # BEHAVIOUR_FRIENDLY = "BEHAVIOUR: FRIENDLY"  # This entity is typically friendly.
    # BEHAVIOUR_HOSTILE = "BEHAVIOUR: HOSTILE"  # This entity is typically hostile based on certain rules.
    # BEHAVIOUR_PASSIVE = "BEHAVIOUR: PASSIVE"  # This entity tends to ignore other entities.
    # BEHAVIOUR_UNKNOWN = "BEHAVIOUR: UNKNOWN"  # The behaviour of this entity is difficult to predict.
    # BLADE = "BLADE"
    BLOCK = "BLOCK"
    # CONTAINER = "CONTAINER"  # This entity is able to contain item entities.
    # CORPSE = "CORPSE"
    # DOOR = "DOOR"
    # ENERGY = "ENERGY"
    # ENERGY_PACK = "ENERGY_PACK"
    # EXOTIC = "EXOTIC"  # Exotic entities are rare and valuable.
    # BACKGROUND = "BACKGROUND"  # This entity is intended to be used as the background entity of a tile.
    ITEM = "ITEM"
    # JUNK = "JUNK"
    MATERIAL = "MATERIAL"
    # META = "$meta"
    # ON_DEATH_SCRAP = "ON_DEATH_SCRAP"  # On scrapping the entity, get on_death as material.
    PASSABLE = "PASSABLE"  # Other entities may move as though this entity was not placed.
    # RACE_CANINE = "RACE: CANINE"
    # RACE_GOBLINOID = "RACE: GOBLINOID"
    # RACE_HUMAN = "RACE: HUMAN"
    # RAW = "RAW"  # Indicates that a material is raw.
    # RANGED = "RANGED"
    # SILICATE = "SILICATE"
    # STACK = "STACK"  # This entity stacks one or more objects of the same type
    # TRANSPARENT = "TRANSPARENT"  # This entity does not block view.
    UNINSTANTIABLE = "UNINSTANTIABLE"  # This entity will raise an Exception if it is instanced.
    # WEAPON = "WEAPON"


class Entity:
    def __init__(self,
                 uid: str,
                 name: str = "No name!",
                 desc: str = "No description!",
                 tags: [Tag] = None):
        if tags is None:
            tags = []

        self.uid = uid
        self.name = name
        self.desc = desc
        self.tags = tags

    def has_tag(self, tag: Tag):
        return tag in self.tags

    def get_instance(self):
        if self.has_tag(Tag.UNINSTANTIABLE):
            raise EntityUninstantiableException(self)
        return copy.deepcopy(self)


class Actor(Entity):
    pass


class Block(Entity):
    pass


class Item(Entity):
    pass